## README

`C:\_\keyprism3\.assets\webbrain`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `163`
- `thought_uid` - `95ADF5BD-4226-4A3F-0D04-245D1BDF196E`
- `vp_url` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/46rGCNqGAqAAAgnq`
- `FmColorName` - `gray`
- `ColorHex` - `#9999A2`
- `ColorRgb` - `153,153,162`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cwebbrain) _Source Files for Github Push Directory for WebBrain UserScript_

See [Keyprism3_WebBrain](https://openuserjs.org/scripts/MarkRobbins/Keyprism3_WebBrain) UserScript

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%38%26webthought_id%3D%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FyOFsx%31qGAqAAAgq_%2FuDwcx%31qGAqAAAgsh) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cwebbrain%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%34%36rGCNqGAqAAAgnq) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%39%35ADF%35BD-%34%32%32%36-%34A%33F-%30D%30%34-%32%34%35D%31BDF%31%39%36E) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.assets/webbrain/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-163) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cwebbrain%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cwebbrain%5Cwebbrain.user.js) `webbrain.user.js` - HardLink to `assets/.webbrain/webbrain.user.js` - Extends WebBrain to integrate Yodiz, Visual Paradigm for [keyprism3](https://bitbucket.org/keyprism3/keyprism3/src/master/)

