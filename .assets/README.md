## README

`C:\_\keyprism3\.assets`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `8`
- `webthought_id` - `8`
- `thought_uid` - `070DDECA-2C65-450B-E2EA-965240B179A3`
- `vp_url` - `keyprism3.vpp://shape/yOFsx1qGAqAAAgq_/uDwcx1qGAqAAAgsh`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/9xCKvVqGAqAAAg0x`
- `FmColorName` - `white-light`
- `ColorHex` - `#FDFDFD`
- `ColorRgb` - `253,253,253`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets) _Various Assets for tools and site_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FU.GAPVqGAqAAAhLD) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FyOFsx%31qGAqAAAgq_%2FuDwcx%31qGAqAAAgsh) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%30%37%30DDECA-%32C%36%35-%34%35%30B-E%32EA-%39%36%35%32%34%30B%31%37%39A%33) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.assets/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-8) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_yOFsx1qGAqAAAgq_.html) - visual paradigm publish

### Anticipates

- `< more-types>`

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5C.webbrain%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%36%31%26webthought_id%3D%31%36%31%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FEin%36CNqGAqAAAgmt) `.webbrain` - Github Push Directory for WebBrain UserScript
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5C.yodiz%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%36%32%26webthought_id%3D%31%36%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FotKGCNqGAqAAAgnF) `.yodiz` - Github Push Directory for Yodiz UserScript
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-orange-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%30%26webthought_id%3D%32%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FnEwRvVqGAqAAAg%31x) `esdoc` - EsDoc enhancements, css and script
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cfonts%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%31%26webthought_id%3D%32%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fu%31TJvVqGAqAAAg%32%34) `fonts` - Font Assets
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cimages%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%32%26webthought_id%3D%32%31%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FbeCFvVqGAqAAAg%34E) `images` - Image Assets, Originals
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cnode_modules%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%33%26webthought_id%3D%31%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%30MQlvVqGAqAAAg%34g) `node_modules` - see `/_tools/reset-nm.bat` :: Dummy Directory for preserving Icon
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `test_files`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cvisual_paradigm_publish%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%34%26webthought_id%3D%32%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FY%36HlvVqGAqAAAg%34%30) `visual_paradigm_publish` - Visual Paradigm Publish Overlay Assets
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cwebbrain%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%36%33%26webthought_id%3D%31%36%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%34%36rGCNqGAqAAAgnq) `webbrain` - Source Files for Github Push Directory for WebBrain UserScript
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cyodiz%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%36%34%26webthought_id%3D%31%36%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FDJnGCNqGAqAAAgn%30) `yodiz` - Source Files for Github Push Directory for Yodiz UserScript
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5C_crushed%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%39%26webthought_id%3D%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%38FWqvVqGAqAAAg%31b) `_crushed` - temp crush folder

### Files

_none_

