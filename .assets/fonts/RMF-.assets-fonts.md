## RMF-.assets-fonts

`C:\_\keyprism3\.assets\fonts`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `21`
- `webthought_id` - `23`
- `thought_uid` - `FA42B065-7B21-0985-49A0-F8FC64358E1F`
- `vp_url` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/u1TJvVqGAqAAAg24`
- `FmColorName` - `blue-dark`
- `ColorHex` - `#455DAC`
- `ColorRgb` - `69,93,172`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cfonts) _Font Assets_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%38%26webthought_id%3D%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FyOFsx%31qGAqAAAgq_%2FuDwcx%31qGAqAAAgsh) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cfonts%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fu%31TJvVqGAqAAAg%32%34) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/FA%34%32B%30%36%35-%37B%32%31-%30%39%38%35-%34%39A%30-F%38FC%36%34%33%35%38E%31F) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.assets/fonts/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-21) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cfonts%5Cunused%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%38%36%26webthought_id%3D%38%36%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FYjWpvVqGAqAAAg%33J) `unused` - Unused Font Assets

### Files

_none_

