## README

`C:\_\keyprism3\.assets\yodiz`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `164`
- `thought_uid` - `54336DF4-4827-6295-8213-3B09AE2A6914`
- `vp_url` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/DJnGCNqGAqAAAgn0`
- `FmColorName` - `gray`
- `ColorHex` - `#9999A2`
- `ColorRgb` - `153,153,162`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cyodiz) _Source Files for Github Push Directory for Yodiz UserScript_

See [Keyprism3_Yodiz](https://openuserjs.org/scripts/MarkRobbins/Keyprism3_Yodiz) UserScript

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%38%26webthought_id%3D%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FyOFsx%31qGAqAAAgq_%2FuDwcx%31qGAqAAAgsh) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cyodiz%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FDJnGCNqGAqAAAgn%30) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%35%34%33%33%36DF%34-%34%38%32%37-%36%32%39%35-%38%32%31%33-%33B%30%39AE%32A%36%39%31%34) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.assets/yodiz/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-164) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cyodiz%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cyodiz%5Cyodiz.user.js) `yodiz.user.js` - HardLink to `.assets/.yodiz/yodiz.user.js` - Extends Yodiz to integrate WebBrain and Visual Paradigm for [keyprism3](https://bitbucket.org/keyprism3/keyprism3/src/master/)

