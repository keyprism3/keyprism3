## README

`C:\_\keyprism3\.assets\esdoc`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `20`
- `thought_uid` - `7AB1F7ED-86E1-63D0-537B-E891B11FE779`
- `vp_url` - `keyprism3.vpp://shape/NGcYyNqGAqAAAh4O/qJSYyNqGAqAAAh4d`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/nEwRvVqGAqAAAg1x`
- `FmColorName` - `red-dark`
- `ColorHex` - `#F43F2A`
- `ColorRgb` - `244,63,42`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc) _EsDoc enhancements, css and script_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%38%26webthought_id%3D%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FyOFsx%31qGAqAAAgq_%2FuDwcx%31qGAqAAAgsh%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%39xCKvVqGAqAAAg%30x) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FNGcYyNqGAqAAAh%34O%2FqJSYyNqGAqAAAh%34d) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FNGcYyNqGAqAAAh%34O%2FqJSYyNqGAqAAAh%34d) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%37AB%31F%37ED-%38%36E%31-%36%33D%30-%35%33%37B-E%38%39%31B%31%31FE%37%37%39) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.assets/esdoc/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-20) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_NGcYyNqGAqAAAh4O.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Ccss%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%35%26webthought_id%3D%31%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FifLYyNqGAqAAAh%35N%2FoKHYyNqGAqAAAh%35c%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fc%32PRvVqGAqAAAg%32C) `css` - Source EsDoc Style
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%36%26webthought_id%3D%31%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FwLE%34yNqGAqAAAh%35%35%2FVt%30%34yNqGAqAAAh%36G%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FIA%35xvVqGAqAAAg%32W) `script` - Source EsDoc built-in scripts, and additional scripts plus `manual.js` custom script

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5CREADME.html) `README.html`

