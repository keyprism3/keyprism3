## README

`C:\_\keyprism3\.assets\esdoc\script\prettify`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `27`
- `webthought_id` - `11`
- `thought_uid` - `DB2581F8-8501-2DC2-2EFC-4F9812D163BC`
- `vp_url` - `keyprism3.vpp://shape/lHe4yNqGAqAAAh6t/r7R4yNqGAqAAAh68`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/FEgJvVqGAqAAAg2p`
- `FmColorName` - `black`
- `ColorHex` - `#525257`
- `ColorRgb` - `82,82,87`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Cprettify) _Source Prettify Code_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%36%26webthought_id%3D%31%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FwLE%34yNqGAqAAAh%35%35%2FVt%30%34yNqGAqAAAh%36G%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FIA%35xvVqGAqAAAg%32W) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Cprettify%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FlHe%34yNqGAqAAAh%36t%2Fr%37R%34yNqGAqAAAh%36%38) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FlHe%34yNqGAqAAAh%36t%2Fr%37R%34yNqGAqAAAh%36%38) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/DB%32%35%38%31F%38-%38%35%30%31-%32DC%32-%32EFC-%34F%39%38%31%32D%31%36%33BC) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.assets/esdoc/script/prettify/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-27) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/.assets/esdoc/script/prettify/prettify.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/esdoc_script_prettify_prettify_js/index.html) - plato
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_lHe4yNqGAqAAAh6t.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Cprettify%5CApache-License-%32.%30.txt) `Apache-License-2.0.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Cprettify%5Cprettify.js) `prettify.js` - Library, has css dependency
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Cprettify%5CREADME.html) `README.html`

