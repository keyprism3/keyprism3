## README

`C:\_\keyprism3\.assets\esdoc\script`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `26`
- `webthought_id` - `13`
- `thought_uid` - `8DB9D7F1-3E64-D45B-1D07-033962F8ECEF`
- `vp_url` - `keyprism3.vpp://shape/wLE4yNqGAqAAAh55/Vt04yNqGAqAAAh6G`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/IA5xvVqGAqAAAg2W`
- `FmColorName` - `black`
- `ColorHex` - `#525257`
- `ColorRgb` - `82,82,87`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript) _Source EsDoc built-in scripts, and additional scripts plus `manual.js` custom script_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%30%26webthought_id%3D%32%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FNGcYyNqGAqAAAh%34O%2FqJSYyNqGAqAAAh%34d%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FnEwRvVqGAqAAAg%31x) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FwLE%34yNqGAqAAAh%35%35%2FVt%30%34yNqGAqAAAh%36G) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FwLE%34yNqGAqAAAh%35%35%2FVt%30%34yNqGAqAAAh%36G) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%38DB%39D%37F%31-%33E%36%34-D%34%35B-%31D%30%37-%30%33%33%39%36%32F%38ECEF) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.assets/esdoc/script/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-26) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_wLE4yNqGAqAAAh55.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Cprettify%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%37%26webthought_id%3D%31%31%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FlHe%34yNqGAqAAAh%36t%2Fr%37R%34yNqGAqAAAh%36%38%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FFEgJvVqGAqAAAg%32p) `prettify` - Source Prettify Code

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Cclipboard.min.js) `clipboard.min.js` - Clipboard Copy
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5CcontextMenu.min.js) `contextMenu.min.js` - Context Menu
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Cjquery-%32.%31.%34.min.js) `jquery-2.1.4.min.js` - EsDocs wants
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Cjquery.qtip.min.js) `jquery.qtip.min.js` - QTip2 for jQuery
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5CloadCSS.js) `loadCSS.js` - Load CSS file asynch with window.loadCSS(href, before, media, idd)`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Clscache.min.js) `lscache.min.js` - localStorage extension [lscashe Github](https://github.com/pamelafox/lscache)
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Cmanual.js) `manual.js` - Mark Robbins EsDoc Enhancements `window._`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5Cnotify.min.js) `notify.min.js` - [notifyjs.com](https://notifyjs.com/)
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5ConloadCSS.js) `onloadCSS.js` - adds onload support for asynchronous stylesheets loaded with `loadCSS.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Cscript%5CREADME.html) `README.html`

