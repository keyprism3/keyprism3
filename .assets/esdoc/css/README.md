## README

`C:\_\keyprism3\.assets\esdoc\css`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `25`
- `webthought_id` - `15`
- `thought_uid` - `F42F6C6D-FB21-A08F-17E0-7941DFCAB212`
- `vp_url` - `keyprism3.vpp://shape/ifLYyNqGAqAAAh5N/oKHYyNqGAqAAAh5c`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/c2PRvVqGAqAAAg2C`
- `FmColorName` - `black`
- `ColorHex` - `#525257`
- `ColorRgb` - `82,82,87`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Ccss) _Source EsDoc Style_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%30%26webthought_id%3D%32%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FNGcYyNqGAqAAAh%34O%2FqJSYyNqGAqAAAh%34d%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FnEwRvVqGAqAAAg%31x) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Ccss%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FifLYyNqGAqAAAh%35N%2FoKHYyNqGAqAAAh%35c) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FifLYyNqGAqAAAh%35N%2FoKHYyNqGAqAAAh%35c) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/F%34%32F%36C%36D-FB%32%31-A%30%38F-%31%37E%30-%37%39%34%31DFCAB%32%31%32) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.assets/esdoc/css/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-25) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_ifLYyNqGAqAAAh5N.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Ccss%5CcontextMenu.css) `contextMenu.css` - for `contextMenu.min.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Ccss%5Cjquery.qtip.min.css) `jquery.qtip.min.css` - for `jquery.qtip.min.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Ccss%5Cprettify-tomorrow.css) `prettify-tomorrow.css` - for `prettify.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Ccss%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Ccss%5Cstyle-qtip.css) `style-qtip.css` - for `jquery.qtip.min.js` - extending
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cesdoc%5Ccss%5Cstyle.css) `style.css` - main EsDoc styles - in transition

