## RMF-.assets-images

`C:\_\keyprism3\.assets\images`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cimages) _Image Assets, Originals_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%38%26webthought_id%3D%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FyOFsx%31qGAqAAAgq_%2FuDwcx%31qGAqAAAgsh) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cimages%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FbeCFvVqGAqAAAg%34E) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/BAEFE%35%30E-%39EB%38-BD%39%30-%39C%31%31-%39BDAF%32%36%37%38%32%35A) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.assets/images/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-22) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cimages%5CBlack-Background-Collapsar.jpg) `Black-Background-Collapsar.jpg` - Find where used, obsolete?
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cimages%5Cbrain-%31%38%30x%31%32%30.png) `brain-180x120.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cimages%5Cfavicomatic.zip) `favicomatic.zip` - Icon Images
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cimages%5Cicon-%32%35%36.png) `icon-256.png` - Letter K, rainbow
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cimages%5Cicon-%39%36.png) `icon-96.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cimages%5Clogo-work.png) `logo-work.png` - Word "Keyprism" with rainbow
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.assets%5Cimages%5Cwebbrain-%31%38%30x%31%32%30.png) `webbrain-180x120.png`

### Folder Built-ins

_none_

### Anticipates

_none_
