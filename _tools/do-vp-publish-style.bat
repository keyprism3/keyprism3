cd %~dp0

rem C:\_\socialnitro-app\._vphtml\index.html
copy ..\.assets\visual_paradigm_publish\index.html ..\._vphtml\publish\index.html /y

copy ..\.assets\visual_paradigm_publish\content\blank_menu.html ..\._vphtml\publish\content\blank_menu.html /y
rem copy ..\.assets\visual_paradigm_publish\content\diagram_navigator.html ..\._vphtml\publish\content\diagram_navigator.html /y
copy ..\.assets\visual_paradigm_publish\content\project_content.html ..\._vphtml\publish\content\project_content.html /y

copy ..\.assets\visual_paradigm_publish\content\report.css ..\._vphtml\publish\content\report.css /y

copy ..\.assets\visual_paradigm_publish\content\link_popup.js ..\._vphtml\publish\content\link_popup.js /y
copy ..\.assets\visual_paradigm_publish\content\report.js ..\._vphtml\publish\content\report.js /y
copy ..\.assets\visual_paradigm_publish\content\tooltip.js ..\._vphtml\publish\content\tooltip.js /y

copy ..\.assets\visual_paradigm_publish\images\icons\*.* ..\._vphtml\publish\images\icons /y
rem _tools\do-vp-report-style.bat
rem pause
