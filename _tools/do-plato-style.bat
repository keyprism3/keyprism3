cd %~dp0
cd ..

COPY .assets\plato\assets\css\*.css ._plato\assets\css /y
COPY .assets\plato\assets\css\vendor\*.css ._plato\assets\css\vendor /y
COPY .assets\plato\assets\font\*.eot ._plato\assets\font /y
COPY .assets\plato\assets\font\*.svg ._plato\assets\font /y
COPY .assets\plato\assets\font\*.ttf ._plato\assets\font /y
COPY .assets\plato\assets\font\*.woff ._plato\assets\font /y
COPY .assets\plato\assets\scripts\*.js ._plato\assets\scripts /y
COPY .assets\plato\assets\scripts\bundles\*.js ._plato\assets\scripts\bundles /y
COPY .assets\plato\assets\scripts\vendor\*.js ._plato\assets\scripts\vendor /y
COPY .assets\plato\assets\scripts\vendor\codemirror\*.js ._plato\assets\scripts\vendor\codemirror /y
COPY .assets\plato\assets\scripts\vendor\codemirror\util\*.js ._plato\assets\scripts\vendor\codemirror\util /y
COPY .assets\plato\assets\scripts\vendor\codemirror\util\*.css ._plato\assets\scripts\vendor\codemirror\util /y

pause

