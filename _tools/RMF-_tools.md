## RMF-_tools

`C:\_\keyprism3\_tools`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5C_tools) _Various Batch Tools_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/_tools/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-18) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdeploy.bat) `deploy.bat` - Deploy to Galaxy - obsolete
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdo-esdoc-pt%31.bat) `do-esdoc-pt1.bat` - Helper
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdo-esdoc.bat) `do-esdoc.bat` - Create EsDoc Report
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdo-plato-pt%31.bat) `do-plato-pt1.bat` - Helper
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdo-plato.bat) `do-plato.bat` - Create Plato Report
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdo-vp-publish-rev-style.bat) `do-vp-publish-rev-style.bat` - Copy VP styles from published to assets
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdo-vp-publish-style.bat) `do-vp-publish-style.bat` - Helper to `do-vp-publish.bat`
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdo-vp-publish-upload.bat) `do-vp-publish-upload.bat` - Helper to `do-vp-publish.bat`
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdo-vp-publish.bat) `do-vp-publish.bat` - Create Visual Paradigm Publish, after generated from VP
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdragonlister-keys.txt) `dragonlister-keys.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdragonlister-out.txt) `dragonlister-out.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdragonlister.bat) `dragonlister.bat` - Make file lists to `dragonlister.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cdragonlister.txt) `dragonlister.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cesdoc.json) `esdoc.json` - EsDoc Configuration
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Clists.txt) `lists.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cstyleindexer.bat) `styleindexer.bat` - Pull style imports into `app\client\main2.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C_tools%5Cuml.puml) `uml.puml`

### Folder Built-ins

_none_

### Anticipates

_none_
