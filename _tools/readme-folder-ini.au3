; #INDEX# =======================================================================================================================
 ; Title .........: ReadMe Folder Ini v1.0.0.0
 ; AutoIt Version : 3.3
 ; Language ......: English (language independent)
 ; Description ...:
 ; Author(s) .....: MarkRobbins
 ; Copyright .....: Copyright (C) Mark C Robbins. All rights reserved.
 ; License .......: Artistic License 2.0, see Artistic.txt
 ;
 ; ReadMe Folder Ini is free software; you can redistribute it and/or modify
 ; it under the terms of the Artistic License as published by Larry Wall,
 ; either version 2.0, or (at your option) any later version.
 ;
 ; This program is distributed in the hope that it will be useful,
 ; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 ; See the Artistic License for more details.
 ;
 ; You should have received a copy of the Artistic License with this Kit,
 ; in the file named "Artistic.txt".  If not, you can get a copy from
 ; <http://www.perlfoundation.org/artistic_license_2_0> OR
 ; <http://www.opensource.org/licenses/artistic-license-2.0.php>
 ;
 ; ===============================================================================================================================

; #OVERVIEW# ===========================================================================================================
 ; SOURCE_______________________________________________________________________________________________________________
  ; Organization ..: Mark Robbins and Associates
  ; Author ........: Mark Robbins
 ; LOG__________________________________________________________________________________________________________________
  ; Created .......: 2017.03.15.14.34.42
  ; Modified ......: 2017.03.15.14.34.42
  ; Entries........: yyyy.mm.dd.hh.mm.ss Comments
 ; HEADER_______________________________________________________________________________________________________________
  ; Type ..........: Class
  ; Subtype .......:
  ; Name ..........: ReadMe Folder Ini
  ; Summary .......:
  ; Description ...:
  ;
  ; Remarks .......:
 ; DEVELOPMENT__________________________________________________________________________________________________________
  ; Issues ........:
  ; Status ........: [X] New
  ;                  [ ] Open
  ;                  [ ] InProgress
  ;                  [ ] Resolved
  ;                  [ ] Closed
 ; OTHER________________________________________________________________________________________________________________
  ; Related .......:
  ; Related Links .:
  ; Resources......:
 ; =====================================================================================================================
;#NoTrayIcon
#include <file.au3>
#include <array.au3>


Global $thisfile="C:\batch\readme-folder-ini.au3"
Global $logg="C:\$data\logs\readme-folder-ini-log.txt";
Global $timestamp=@YEAR&"."&@MON&"."&@MDAY&"."&@HOUR&"."&@MIN&"."&@SEC&"."&@MSEC

Global $testing=True

If $CmdLine[0]<>0 Then
  $testing=False
EndIf

Global $p1='C:\_\keyprism3\.assets\Desktop.ini'

If Not $testing Then
  If $CmdLine[0]<>1 Then
    RunWait(@AutoItExe&' C:\batch\splash.au3 "Need 1 params - exiting" "'&$thisfile&'" "10"',""); ,@SW_NORMAL)
    Exit
  EndIf
  $p1=$CmdLine[1]
EndIf

Global $bsl='\'
Global $pfx='RMF'
Global $projects='http://mrobbinsassoc.com/projects/'

; INI FORMAT

;[>]
;? = subhead
;p1..= paragraphs
;-<filename>=descriptor
;+1..=anticipates
;-.=no list files with extensions list

;[_.<filename>]
;_= (if file)

;Local $tt=getSubtitle($p1)
;MsgBox(0,$thisfile,projectName())
;MsgBox(0,$thisfile,esdocFolderUrl($p1&$bsl&'README.md'))
;Exit

MakeReadMe($p1) ;;;;HERE
Exit

Func MakeReadMe($p)
  Local $n=CreateFn($p)
  If $n=='' Then
    MsgBox(0,$thisfile,'could not find git directory above '&$p&' ... exiting');
    Exit
  EndIf
  Local $fn=$p1&'\'&$n&'.md';
  Local $fn2=$p1&'\README.md';
  MakeMd_($fn)
  MakeMd_($fn2)
  EndFunc

Func MakeMd_($fn)
  If FileExists($fn) Then
    FileDelete($fn)
  EndIf
  ;MsgBox(0,$thisfile,$fn)
  ;FileWriteLine($fn,'## '&$n);
  MakeMd($fn)
EndFunc


Func getSubtitle($fn)
  Local $ini=IniOf($fn)
  Return IniRead($ini,'>','?','')
EndFunc

Func IniOf($fn)
  If IsDir($fn) Then
    Return $fn&'\Desktop.ini'
  EndIf
  Local $p=FilePart($fn,'p_')
  Return $p&'Desktop.ini';
EndFunc

Func IsDir($fn)
  Local $a=FileGetAttrib($fn)
  Return StringInStr($a,'D')<>0
EndFunc


Func makeReadsFrom()
  Local $fn='C:\_\pinterest\_extra\_i\!i-mfd-list.txt';
  Local $file = FileOpen($fn, 0)

  ; Check if file opened for reading OK
  If $file = -1 Then
      MsgBox(0, "Error", "Unable to open file.")
      Exit
  EndIf

  ; Read in lines of text until the EOF is reached
  While 1
      Local $line = FileReadLine($file)
      If @error = -1 Then ExitLoop
      makeReadMeUsing($line)
      ;ExitLoop
      ;MsgBox(0, "Line read:", $line)
  WEnd
  FileClose($file)
EndFunc

Func makeReadMeUsing($p)
  Local $fp=FilePart($p,'p');
  Local $rm=$fp&$bsl&'README.md';
  If FileExists($rm) Then
    MsgBox(0, "file exists, skipping", $rm);
    Return
  EndIf
  FileCopy($p,$rm);
  readmeFixup($rm);
EndFunc

Func readmeFixup($p)
  Local $out=$p&'.tmp';
  Local $file = FileOpen($p, 0);

  ; Check if file opened for reading OK
  If $file = -1 Then
      MsgBox(0, "Error", "Unable to open file."&$p)
      Exit
  EndIf

  ; Read in lines of text until the EOF is reached
  While 1
      Local $line = FileReadLine($file)
      If @error = -1 Then ExitLoop
      $line=readmeFixupLine($line)
      FileWriteLine($out,$line)
      ;MsgBox(0, "Line read:", $line)
  WEnd
  FileClose($file)
  FileMove($out,$p,1)
EndFunc


Func readmeFixupLine($l)
  Local $t='## RMF';
  Local $r='## README';
  If Not StringInStr($l,$t,1) Then Return $l
  Return StringReplace($l,$t,$r,1);
EndFunc


Func writeParagraphs($p)
  Local $x=1;
  Local $none='NONE-NONE'
  Local $ini=IniOf($p)
  Local $prevnorm=True
  While True
    Local $par=IniRead($ini,'>','?'&$x,$none);
    If $par=$none Then
      ExitLoop
    EndIf
    Local $nl=True
    If StringLeft($par,5)='!    ' Then
      $par=StringRight($par,StringLen($par)-1)
      $nl=False
    EndIf
    If $prevnorm Then
      If Not $nl Then
        FileWriteLine($p,'');
      Else
        FileWriteLine($p,'');
      EndIf
    Else
      If Not $nl Then
      Else
        FileWriteLine($p,'');
      EndIf
    EndIf
    If $nl Then
      $prevnorm=True
    Else
      $prevnorm=False
    EndIf
    FileWriteLine($p,$par);
    $x=$x+1
  WEnd
EndFunc

Func writeAnticipates($p)
  Local $x=1;
  Local $none='NONE-NONE'
  Local $ini=IniOf($p)
  Local $did=False
  While True
    Local $par=IniRead($ini,'>','+'&$x,$none);
    If $par=$none Then
      ExitLoop
    EndIf
    FileWriteLine($p,'- '&$par);
    $x=$x+1
    $did=True
  WEnd
  If Not $did Then
    FileWriteLine($p,'_none_');
  EndIf
EndFunc



Func fileLineItemDescriptor($p,$fn)
  Local $ini=IniOf($p)
  Local $des=IniRead($ini,'>',$fn,'');
  Return $des
EndFunc

Func fileLineItem($p,$lff,$fn)
  Local $s='- '&$lff&' `'&$fn&'`';
  Local $des=fileLineItemDescriptor($p,$fn)
  If $des<>'' Then
    $s=$s&' - '&$des
  EndIf
  Return $s
EndFunc


Func folderLineItemDescriptor($p,$fn)
  Local $fini=IniOf($p&$bsl&$fn)
  Local $fdes=IniRead($fini,'>','?','');
  ;
  Local $ini=IniOf($p)
  Local $des=IniRead($ini,'>',$fn,'');
  Local $d='';
  If $des<>'' Then
    $d=$des
  EndIf
  If $fdes<>'' Then
    If $d<>'' Then
      $d=$d&' :: '&$fdes
    Else
      $d=$fdes
    EndIf
  EndIf
  Return $d
EndFunc

Func folderLineItem($p,$lff,$fn)
  Local $s='- '&$lff&' `'&$fn&'`';
  Local $path=FilePart($p,'p')
  Local $des=folderLineItemDescriptor($path,$fn)
  If $des<>'' Then
    $s=$s&' - '&$des
  EndIf
  Return $s
EndFunc


Func getUrlFrom($urlf)
  Local $v=IniRead($urlf,'InternetShortcut','URL','')
  Return $v
EndFunc

Func bitBucketLink($url)
  ;[![](https://static.woopra.com/website/v6/img/icons/javascript.png)](http://google.com)
  ;Local $fne=FilePart($p,'ne');
  ;  Local $http='http://mrobbinsassoc.com/images/icons/md/fm/'
  Local $img='http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png';imageForFolder($p,$sz)
  $img='![]('&$img&')';
  Local $res='['&$img&']('&$url&')';
  Return $res;
EndFunc

Func webbrainFolderUrl($p)
  ;https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09/thought/8
  Local $s='https://webbrain.com/brainpage/brain/';
  Local $ini=IniOf($p)
  Local $brain=IniRead($ini,'_','brain_uid','')
  Local $thought=IniRead($ini,'_','thought_id','')
  If $brain='' Or $thought='' Then
    Return '';
  EndIf
  Return $s&$brain&'#-'&$thought;
EndFunc


Func webbrainFolderLink($p)
  Local $url=webbrainFolderUrl($p)
  If $url='' Then
    Return '';
  EndIf
  Local $img='http://mrobbinsassoc.com/images/icons/md/webbrain-16.png';imageForFolder($p,$sz)
  $img='![]('&$img&')';
  Local $res='['&$img&']('&$url&')';
  Return $res;
EndFunc

Func projectName()
  Local $a=StringSplit($p1,$bsl,3)
  ;_ArrayDisplay($a)
  Return $a[2]
EndFunc

Func brainAipUrl($p)
  Local $aip='aip://thoughtuid/';
  Local $ini=IniOf($p)
  Local $link=IniRead($ini,'_','thought_uid','')
  If $link='' Then
    Return ''
  EndIf
  Return $aip&_URIEncode($link)
EndFunc

Func brainAipFolderLink($p)
  Local $url=brainAipUrl($p)
  If $url='' Then
    Return ''
  EndIf
  Local $img='http://mrobbinsassoc.com/images/icons/md/webbrain-16.png';imageForFolder($p,$sz)
  $img='![]('&$img&')';
  Local $res='['&$img&']('&$url&')';
  Return $res;
EndFunc

Func vpAipUrl($p)
  Local $aip='aip://vplink/';
  Local $ini=IniOf($p)
  Local $link=IniRead($ini,'_','vp_url','')
  If $link='' Then
    Return ''
  EndIf
  Return $aip&_URIEncode($link)
  ;e C:\batch\aip.au3
EndFunc

Func vpOverviewAipUrl($p)
  Local $aip='aip://vplink/';
  Local $ini=IniOf($p)
  Local $link=IniRead($ini,'_','vp_url_overview','')
  If $link='' Then
    Return ''
  EndIf
  Return $aip&_URIEncode($link)
  ;e C:\batch\aip.au3
EndFunc

Func vpAipFolderLink($p)
  Local $url=vpAipUrl($p)
  If $url='' Then
    Return ''
  EndIf
  Local $img='http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png';imageForFolder($p,$sz)
  $img='![]('&$img&')';
  Local $res='['&$img&']('&$url&')';
  Return $res;
EndFunc

Func vpOverviewAipFolderLink($p)
  Local $url=vpOverviewAipUrl($p)
  If $url='' Then
    Return ''
  EndIf
  Local $img='http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png';imageForFolder($p,$sz)
  $img='![]('&$img&')';
  Local $res='['&$img&']('&$url&')';
  Return $res;
EndFunc

Func iniAipUrl($p)
  Local $aip='aip://slickedit/';
  Local $ini=IniOf($p)
  If $ini='' Then
    Return ''
  EndIf
  Return $aip&_URIEncode($ini)
EndFunc

Func iniAipFolderLink($p)
  Local $url=iniAipUrl($p)
  If $url='' Then
    Return ''
  EndIf
  Local $img='http://mrobbinsassoc.com/images/icons/md/_ini-16.png';imageForFolder($p,$sz)
  $img='![]('&$img&')';
  Local $res='['&$img&']('&$url&')';
  Return $res;
EndFunc


;http://mrobbinsassoc.com/projects/project/plato
;http://mrobbinsassoc.com/projects/project/esdoc
;http://mrobbinsassoc.com/projects/project/vp/report

Func rootLessApp($p)
  ;MsgBox(0,'p',$p)
  Local $path=$p;FilePart($p,'p');
  ;MsgBox(0,'path',$path)
  Local $a=StringSplit($path,$bsl,3)
  ;_ArrayDisplay($a)
  ;0  1  2
  ;C:\_\eopeo\
  Local $x
  Local $z=UBound($a)-1
  Local $s='';
  For $x=4 To $z
    If $s='' Then
      $s=$a[$x];
    Else
      $s=$s&$bsl&$a[$x];
    EndIf
  Next
  Return $s;
EndFunc
Func rootLess($p)
  ;MsgBox(0,'p',$p)
  Local $path=$p;FilePart($p,'p');
  ;MsgBox(0,'path',$path)
  Local $a=StringSplit($path,$bsl,3)
  ;_ArrayDisplay($a)
  ;0  1  2
  ;C:\_\eopeo\
  Local $x
  Local $z=UBound($a)-1
  Local $s='';
  For $x=3 To $z
    If $s='' Then
      $s=$a[$x];
    Else
      $s=$s&$bsl&$a[$x];
    EndIf
  Next
  Return $s;
EndFunc

Func findJs($p,$n)
  Local $path=FilePart($p,'p_');
  Local $index=$path&$n&'.js';
  If FileExists($index) Then
    Return $n;
  EndIf
  Return ''
EndFunc

Func findFolderJs($p)
  Local $path=FilePart($p,'p');
  Local $dirn=FilePart($path,'ne');
  Local $idx=FindJs($p,'index');
  Local $main=FindJs($p,'main');
  Local $pubs=FindJs($p,'publications');
  Local $dir=FindJs($p,$dirn);
  If $idx<>'' Then
    Return $idx
  EndIf
  If $main<>'' Then
    Return $main
  EndIf
  If $dir<>'' Then
    Return $dir
  EndIf
  If $pubs<>'' Then
    Return $pubs
  EndIf
  Return ''
EndFunc


Func esdocFolderUrl($p)
  Local $path=FilePart($p,'p');
  Local $js=findFolderJs($p)
  If $js='' Then
    Return ''
  EndIf
  ;http://mrobbinsassoc.com/projects/project/esdoc/file/app/both/index.js.html
  Local $pn=projectName()
  Local $sub=rootLess($path)
  $sub=StringReplace($sub,$bsl,'/')
  Return $projects&$pn&'/esdoc/file/'&$sub&'/'&$js&'.js.html'
EndFunc

Func esdocFolderLink($p)
  Local $url=esdocFolderUrl($p)
  If $url='' Then
    Return '';
  EndIf
  Local $img='http://mrobbinsassoc.com/images/icons/md/esdoc-16.png';imageForFolder($p,$sz)
  $img='![]('&$img&')';
  Local $res='['&$img&']('&$url&')';
  Return $res;
EndFunc

Func platoFolderUrl($p)
  Local $path=FilePart($p,'p');
  Local $js=findFolderJs($p)
  If $js='' Then
    Return ''
  EndIf
  Local $sub=rootLessApp($path)
  $sub=StringReplace($sub,$bsl,'_')
  ;
  Local $pn=projectName()
  ;http://mrobbinsassoc.com/projects/project/plato/files/imports_api_todos_methods_js/index.html
  Return $projects&$pn&'/plato/files/'&$sub&'_'&$js&'_js/index.html'
EndFunc

Func platoFolderLink($p)
  Local $url=platoFolderUrl($p)
  If $url='' Then
    Return '';
  EndIf
  Local $img='http://mrobbinsassoc.com/images/icons/md/plato-16.png';imageForFolder($p,$sz)
  $img='![]('&$img&')';
  Local $res='['&$img&']('&$url&')';
  Return $res;
EndFunc

Func vppublishFolderUrl($p)
  Local $ini=IniOf($p)
  If Not FileExists($ini) Then
    Return ''
  EndIf
  Local $vpurl=IniRead($ini,'_','vp_url','')
  If $vpurl=='' Then
    Return ''
  EndIf
  Local $a=StringSplit($vpurl,'/',2)
  Local $package_id=$a[3]
  Local $pn=projectName()
  Local $rv0=$projects&$pn&'/vp/publish/index.html?url='
  Local $rv=$projects&$pn&'/vp/publish/content/PackageDiagram_'&$package_id&'.html'
  ;http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_yOFsx1qGAqAAAgq_.html
  Return $rv0&$rv
  ;
  ;Local $path=FilePart($p,'p');
  ;           0          1  2      3
  ;vp_url=keyprism3.vpp://shape/yOFsx1qGAqAAAgq_/uDwcx1qGAqAAAgsh
  ;http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_yOFsx1qGAqAAAgq_.html
  ;
EndFunc
Func vppublishFolderLink($p)
  Local $url=vppublishFolderUrl($p)
  If $url='' Then
    Return '';
  EndIf
  Local $img='http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png';imageForFolder($p,$sz)
  $img='![]('&$img&')';
  Local $res='['&$img&']('&$url&')';
  Return $res;
EndFunc


Func writeLocalLinks($p)
  FileWriteLine($p,'### Local Links');
  FileWriteLine($p,'');
  Local $did=False
  Local $path=FilePart($p,'p')
  Local $inir=LinkForParentFolderReadmeOrIni($path,16)
  If $inir<>'' Then
    FileWriteLine($p,'- '&$inir&' - parent');
    $did=True
  EndIf
  Local $inilink=iniAipFolderLink($p)
  If $inilink<>'' Then
    FileWriteLine($p,'- '&$inilink&' - ini');
    $did=True
  EndIf
  Local $vplink=vpAipFolderLink($p)
  If $vplink<>'' Then
    FileWriteLine($p,'- '&$vplink&' - vplink');
    $did=True
  EndIf
  Local $vpolink=vpAipFolderLink($p)
  If $vpolink<>'' Then
    FileWriteLine($p,'- '&$vpolink&' - vpolink');
    $did=True
  EndIf
  Local $thought=brainAipFolderLink($p)
  If $thought<>'' Then
    FileWriteLine($p,'- '&$thought&' - thought');
    $did=True
  EndIf
  If Not $did Then
    FileWriteLine($p,'_none_');
  EndIf
EndFunc

Func writeExternalLinks($p)
  Local $dir=FilePart($p,'p')
  FileWriteLine($p,'### External Links');
  FileWriteLine($p,'');
  Local $did=False
  Local $fne=FilePart($dir,'ne')
  Local $urlf=$dir&$bsl&$fne&'.url';
  If FileExists($urlf) Then
    Local $url=getUrlFrom($urlf);
    FileWriteLine($p,'- '&bitBucketLink($url)&' - bitbucket');
    $did=True
  EndIf
  Local $webbrain=webBrainFolderLink($p)
  If $webbrain<>'' Then
    FileWriteLine($p,'- '&$webbrain&' - webbrain');
    $did=True
  EndIf
  Local $esdoc=esdocFolderLink($p)
  If $esdoc<>'' Then
    FileWriteLine($p,'- '&$esdoc&' - esdoc');
    $did=True
  EndIf
  Local $plato=platoFolderLink($p)
  If $plato<>'' Then
    FileWriteLine($p,'- '&$plato&' - plato');
    $did=True
  EndIf
  Local $vp=vppublishFolderLink($p)
  If $vp<>'' Then
    FileWriteLine($p,'- '&$vp&' - visual paradigm publish');
    $did=True
  EndIf
  If Not $did Then
    FileWriteLine($p,'_none_');
  EndIf
EndFunc

Func writeFileLineSubItems($p,$fn)
  Local $ini=IniOf($p)
  If Not FileExists($ini) Then
    Return
  EndIf
  Local $x=1;
  Local $did=False
  While True
    Local $line=IniRead($ini,'>',$fn&'?'&$x,'')
    If $line='' Then
      ExitLoop
    EndIf
    $did=True
    FileWriteLine($p,' - '&$line)
    $x=$x+1
  WEnd
  If $did Then
    FileWriteLine($p,'')
    FileWriteLine($p,'<hr>')
    FileWriteLine($p,'')
  EndIf
EndFunc


Func writeDataIniSecKey($p,$sec,$key)
  Local $ini=IniOf($p)
  ;ToolTip($ini)
  If Not FileExists($ini) Then
    ;MsgBox(0,'not exist',$ini)
    Return;
  EndIf
  Local $v=IniRead($ini,$sec,$key,'');
  If $v<>'' Then
    FileWriteLine($p,'- `'&$key&'` - `'&$v&'`');
  EndIf
EndFunc

Func writeData($p)
  FileWriteLine($p,'');
  writeDataIniSecKey($p,'_','brain_uid');
  writeDataIniSecKey($p,'_','thought_id');
  writeDataIniSecKey($p,'_','webthought_id');
  writeDataIniSecKey($p,'_','thought_uid');
  writeDataIniSecKey($p,'_','vp_url');
  writeDataIniSecKey($p,'_','vp_url_overview');
  writeDataIniSecKey($p,'.ShellClassInfo','FmColorName');
  writeDataIniSecKey($p,'.ShellClassInfo','ColorHex');
  writeDataIniSecKey($p,'.ShellClassInfo','ColorRgb');
EndFunc



Func MakeMd($p)
  ;MsgBox(0,$thisfile,$p)
  ;Exit
  Local $dir=FilePart($p,'p')
  Local $fno=FilePart($p,'n')
  FileWriteLine($p,'## '&$fno);
  FileWriteLine($p,'');
  FileWriteLine($p,'`'&$dir&'`');
  writeData($p)
  FileWriteLine($p,'');
  Local $subtitle=getSubtitle($p1)
  If $subtitle='' Then
    $subtitle='summary'
  EndIf
  Local $big=LinkForFolder(FilePart($p,'p'),48);
  FileWriteLine($p,$big&' _'&$subtitle&'_');
  writeParagraphs($p)
  FileWriteLine($p,'');
  ;
  writeLocalLinks($p)
  FileWriteLine($p,'');
  ;
  writeExternalLinks($p)
  FileWriteLine($p,'');
  ;
  FileWriteLine($p,'### Anticipates');
  FileWriteLine($p,'');
  writeAnticipates($p)
  ;
  Local $filelist=FileListFor($dir,True)
  Local $filelist_=FileListFor($dir,False)
  Local $dirlist=FolderListFor($dir,True)
  Local $dirlist_=FolderListFor($dir,False)
  Local $af=StringSplit($filelist,@CRLF,2)
  Local $af_=StringSplit($filelist_,@CRLF,2)
  Local $ad=StringSplit($dirlist,@CRLF,2)
  Local $ad_=StringSplit($dirlist_,@CRLF,2)
  Local $x;
  ;
  FileWriteLine($p,'');
  FileWriteLine($p,'### Folders');
  FileWriteLine($p,'');
  Local $didfolder=False;
  If $dirlist<>'' Then
    For $x=0 To UBound($ad)-1
      Local $dn=$ad[$x]
      Local $lfd=LinkForFolderReadmeOrIni($ad_[$x],16);
      FileWriteLine($p,folderLineItem($p,$lfd,$dn))
      $didfolder=True;
    Next
  EndIf
  If Not $didfolder Then
    FileWriteLine($p,'_none_');
  EndIf
  ;
  FileWriteLine($p,'');
  FileWriteLine($p,'### Files');
  FileWriteLine($p,'');
  Local $didfile=False;
  If $filelist<>'' Then
    For $x=0 To UBound($af)-1
      If Not excludeFile($p,$af_[$x]) Then
        Local $fn=$af[$x]
        Local $lff=LinkForFile($af_[$x]);
        FileWriteLine($p,fileLineItem($p,$lff,$fn));
        writeFileLineSubItems($p,$fn)
        $didfile=True;
      EndIf
    Next
  EndIf
  If Not $didfile Then
    FileWriteLine($p,'_none_');
  EndIf
  ;
  FileWriteLine($p,'');
  ;FileWriteLine($p,'_tbd_');
EndFunc

Func LinkForFile($p)
 ;[![](https://static.woopra.com/website/v6/img/icons/javascript.png)](http://google.com)
  Local $aip='aip://slickedit/';
  ;Local $fne=FilePart($p,'ne');
  Local $img=imageForFile($p)
  If $img='' Then
    $img='edit';
  Else
    $img='![]('&$img&')';
  EndIf
  Local $link=$aip&_URIEncode($p)
  Local $res='['&$img&']('&$link&')';
  Return $res;
EndFunc


Func LinkForParentFolderReadmeOrIni($path,$sz)
  ;MsgBox(0,'path',$path)
  Local $pathp=PathPeel($path)
  ;MsgBox(0,'pathp',$pathp)
  Local $readmep=$pathp&$bsl&'README.md'
  Local $inip=$pathp&$bsl&'Desktop.ini'
  If FileExists($readmep) Then
    Return LinkForFolderReadme($pathp,$sz)
  EndIf
  If FileExists($inip) Then
    Return LinkForFolderIni($pathp,$sz)
  EndIf
  Return '';
EndFunc


Func LinkForFolderIni($path,$sz)
  ;[![](https://static.woopra.com/website/v6/img/icons/javascript.png)](http://google.com)
  Local $aip='aip://open/';
  ;Local $fne=FilePart($p,'ne');
  Local $img='http://mrobbinsassoc.com/images/icons/md/_ini-'&$sz&'.png';;imageForFolder($path,$sz)
  If $img='' Then
    $img='open';
  Else
    $img='![]('&$img&')';
  EndIf
  Local $link=$aip&_URIEncode($path&$bsl&'Desktop.ini')
  Local $res='['&$img&']('&$link&')';
  Return $res;
EndFunc


Func LinkForFolderIni2($path,$sz)
  ;[![](https://static.woopra.com/website/v6/img/icons/javascript.png)](http://google.com)
  Local $aip='aip://open/';
  ;Local $fne=FilePart($p,'ne');
  Local $imgf=imageForFolder($path,$sz)
  If $imgf='' Then
    $imgf='open';
  Else
    $imgf='![]('&$imgf&')';
  EndIf
  Local $parms=getFolderParms($path)
  Local $img='http://mrobbinsassoc.com/images/icons/md/_ini-'&$sz&'.png';;imageForFolder($path,$sz)
  $img='![]('&$img&')';
  Local $link=$aip&_URIEncode($path&$bsl&'Desktop.ini'&$parms)
  Local $res1='['&$imgf&']('&$link&')';
  Local $res2='['&$img&']('&$link&')';
  Return $res1&$res2;
EndFunc


Func LinkForFolderReadmeOrIni($path,$sz)
  Local $readme=$path&$bsl&'README.md'
  If FileExists($readme) Then
    Return LinkForFolderReadme($path,$sz)
  EndIf
  Local $ini=$path&$bsl&'Desktop.ini'
  If FileExists($ini) Then
    Return LinkForFolderIni2($path,$sz)
  EndIf
  Return NonLinkForFolder($path,$sz)
  ;Return ''
EndFunc

Func getFolderParms($path)
  Local $ini=$path&$bsl&'Desktop.ini';
  If Not FileExists($ini) Then
    Return '';
  EndIf
  Local $brain_uid=IniRead($ini,'_','brain_uid','')
  Local $thought_id=IniRead($ini,'_','thought_id','')
  Local $webthought_id=IniRead($ini,'_','webthought_id',$thought_id)
  Local $vp_url=IniRead($ini,'_','vp_url','')
  Local $vp_url_overview=IniRead($ini,'_','vp_url_overview','')
  Local $rv='';
  If $brain_uid<>'' Then
    $rv=$rv&'brain_uid='&$brain_uid;
  EndIf
  If $thought_id<>'' Then
    If $rv<>'' Then $rv=$rv&'&';
    $rv=$rv&'thought_id='&$thought_id;
  EndIf
  If $webthought_id<>'' Then
    If $rv<>'' Then $rv=$rv&'&';
    $rv=$rv&'webthought_id='&$webthought_id;
  EndIf
  If $vp_url<>'' Then
    If $rv<>'' Then $rv=$rv&'&';
    $rv=$rv&'vp_url='&$vp_url;
  EndIf
  If $vp_url_overview<>'' Then
    If $rv<>'' Then $rv=$rv&'&';
    $rv=$rv&'vp_url_overview='&$vp_url_overview;
  EndIf
  ;
  If $rv<>'' Then
    $rv='?'&$rv;
  EndIf
  Return $rv
EndFunc


Func LinkForFolderReadme($path,$sz)
  ;[![](https://static.woopra.com/website/v6/img/icons/javascript.png)](http://google.com)
  Local $aip='aip://open/';
  ;Local $fne=FilePart($p,'ne');
  Local $img=imageForFolder($path,$sz)
  If $img='' Then
    $img='open';
  Else
    $img='![]('&$img&')';
  EndIf
  Local $parms=getFolderParms($path)
  Local $link=$aip&_URIEncode($path&$bsl&'README.md'&$parms)
  Local $res='['&$img&']('&$link&')';
  Return $res;
EndFunc


Func LinkForFolder($path,$sz)
  ;[![](https://static.woopra.com/website/v6/img/icons/javascript.png)](http://google.com)
  Local $aip='aip://open/';
  ;Local $fne=FilePart($p,'ne');
  Local $img=imageForFolder($path,$sz)
  If $img='' Then
    $img='open';
  Else
    $img='![]('&$img&')';
  EndIf
  Local $link=$aip&_URIEncode($path)
  Local $res='['&$img&']('&$link&')';
  Return $res;
EndFunc

Func NonLinkForFolder($path,$sz)
  ;[![](https://static.woopra.com/website/v6/img/icons/javascript.png)](http://google.com)
  Local $aip='aip://open/';
  ;Local $fne=FilePart($p,'ne');
  Local $img=imageForFolder($path,$sz)
  If $img='' Then
    $img='open';
  Else
    $img='![]('&$img&')';
  EndIf
  Local $link=$aip&_URIEncode($path)
  Local $res=$img;'['&$img&']('&$link&')';
  Return $res;
EndFunc

Func excludeExtFor($p,$fn)
  Local $ini=IniOf($p)
  If $ini='' Then
    Return False
  EndIf
  Local $v=IniRead($ini,'>','-.','');
  If $v='' Then
    Return False
  EndIf
  Local $ext=FilePart($fn,'e')
  $v=' '&$v&' ';
  If StringInStr($v,' '&$ext&' ')=0 Then
    Return False
  EndIf
  Return True
EndFunc

Func excludeExtWith($p,$fn)
  Local $ini=IniOf($p)
  If $ini='' Then
    Return False
  EndIf
  Local $v=IniRead($ini,'>','-.with','');
  If $v='' Then
    Return False
  EndIf
  Local $ext=FilePart($fn,'e')
  Local $a=StringSplit($v,' ',2)
  Local $x;
  Local $z=UBound($a)-1
  For $x=0 To $z
    Local $i=$a[$x]
    If StringInStr($ext,$i)<>0 Then
      Return True
    EndIf
  Next
  Return False
EndFunc



Func noFiles($p)
  Local $ini=IniOf($p)
  If $ini='' Then
    Return False
  EndIf
  Local $v=IniRead($ini,'>','*NOFILES','');
  If $v='' Then
    Return False
  EndIf
  Return True
EndFunc


Func excludeFile($p,$fn)
  Return noFiles($p) Or excludeFne($fn) Or excludeLastFilePart($fn) Or excludeExtFor($p,$fn) Or excludeExtWith($p,$fn)
EndFunc


Func excludeFne($p)
  Local $fne=FilePart($p,'ne')
  If $fne='Desktop.ini' Or $fne='FolderMarker.ico' Then
    Return True;
  EndIf
  Return False
EndFunc

Func excludeLastFilePart($p)
  If isLastPart($p,'.idea\workspace.xml') Then Return True
  If isLastPart($p,'.idea\tasks.xml') Then Return True
  If isLastPart($p,'.idea\jsLibraryMappings.xml') Then Return True
  If isLastPart($p,'.idea\dataSources.ids') Then Return True
  If isLastPart($p,'.idea\dataSources.xml') Then Return True
  If isLastPart($p,'.idea\dataSources.local.xml') Then Return True
  If isLastPart($p,'.idea\sqlDataSources.xml') Then Return True
  If isLastPart($p,'.idea\dynamic.xml') Then Return True
  If isLastPart($p,'.idea\uiDesigner.xml') Then Return True
  ;If isLastPart($p,'') Then Return True
  Return False
EndFunc
Func isLastPart($p,$s)
  If StringRight($p,StringLen($s))=$s Then Return True
  Return False
EndFunc



Func imageForFile($p)
  Local $fp=FilePart($p,'p')
  Local $fe=FilePart($p,'e')
  Local $fn=FilePart($p,'n')
  Local $fne=FilePart($p,'ne')
  Local $http='http://mrobbinsassoc.com/images/icons/md/'
  Local $dots=' eslintrc gitignore jshintrc '
  If $fn='' And StringInStr($dots,' '&$fe&' ')<>0 Then
    Return $http&'_'&$fe&'-16.png';
  EndIf
  Local $sole=' packages versions ';
  If $fe='' And StringInStr($sole,' '&$fn&' ')<>0 And StringInStr($fp,'.meteor')<>0 Then
    Return $http&'meteor-'&$fn&'-16.png';
  EndIf
  Local $full=' index.js ';
  If $fne='index.js' Then
    Return $http&'_js-index-16.png';
  EndIf
  Local $part=' .import.less .import.sass .import.scss '
  If $fe='less' Or $fe='sass' Or $fe='scss' Then
    If StringInStr($fne,'.import.')<>0 Then
      Return $http&'_'&$fe&'-import-16.png';
    EndIf
  EndIf
  If StringInStr($fn,'-container')<>0 And $fe='jsx' Then
    Return $http&'_jsx-container-16.png';
  EndIf
  Local $good=' bat css feature html ini js json jsx md less sass scss txt vpj vpw '
  If StringInStr($good,' '&$fe&' ')<>0 Then
    Return $http&'_'&$fe&'-16.png';
  EndIf
  Return $http&'_blank-16.png';
EndFunc

Func imageForFolder($p,$sz)
  ;MsgBox(0,'imageForFolder',$p)
  Local $http='http://mrobbinsassoc.com/images/icons/md/fm/'
  Local $fp=FilePart($p,'p')
  Local $fe=FilePart($p,'e')
  Local $fn=FilePart($p,'n')
  Local $fne=FilePart($p,'ne')
  Local $up=FindGitUp($p);
  ;MsgBox(0,'$up',$up);
  Local $rel=StringReplace($p,$up,''); '' or \...
  ;MsgBox(0,'$rel',$rel);
  ;Local $clr=PathColor($rel)
  Local $clr=PathColor($p)
  ;Local $clr=PathColor($rel)
  If $clr='' Then $clr='black-dark'
  Return $http&'fm-'&$clr&'-'&$sz&'.png';
EndFunc

;gray 9999A2
;gray-dark 7D7D85
;green-light A2E7A8
;blue-light 7A82A1
;blue-dark 455DAC
;blue 6C7FD4
;green 70CF70
;green-dark 40C040
;green-light 9EE6A2
;cyan-dark 4A9EFE
;cyan 78C9FC
;cyan-light BDE5FF
;orange-light FAC083
;red F56C4B
;pink-dark CB60A3
;pink E388C0

Func bail($s)
  MsgBox(0,"BAIL",$s);
  Exit
EndFunc

Func PathColor($p)
  Local $ini=$p&'\Desktop.ini';
  If Not FileExists($ini) Then
    ;bail('Desktop.ini not exists for '&$p)
    Return ''
  EndIf
  Local $rv=IniRead($ini,'.ShellClassInfo','FmColorName','');
  Return $rv;
EndFunc


Func FileListFor($p,$nameonly)
  Local $s='';
  Local $search = FileFindFirstFile($p&"\*.*")

  ; Check if the search was successful
  If $search = -1 Then
      ;MsgBox(0, "Error", "No files/directories matched the search pattern")
      Return $s;
      Exit
  EndIf
  Local $dirn=FilePart($p,'ne')
  Local $isroot=FileExists($p&"\.git") And $dirn<>'._plato' And $dirn<>'._esdoc'

  While 1
      Local $file = FileFindNextFile($search)
      If @error Then ExitLoop
      Local $pfile=$p&$bsl&$file
      Local $use=$pfile
      If $nameonly Then
        $use=$file
      EndIf
      ;MsgBox(4096, "File:", $file&@CRLF&FileGetAttrib($pfile))
      If StringInStr(FileGetAttrib($pfile),'D')== 0 Then
        Local $include=True;
        If HasExt($pfile,'md') Then $include=False;
        If HasExt($pfile,'$explhere') Then $include=False;
        If HasExt($pfile,'url') Then $include=False;
        If HasExt($pfile,'md') And StringInStr($pfile,'\_docs\') Then $include=True;
        If $isroot Then $include=True;
        If HasExt($pfile,'md') And StringInStr($dirn,'.')==1 Then $include=False;
        If HasExt($pfile,'url') And StringInStr($dirn,'.')==1 Then $include=False;
        If HasExt($pfile,'$explhere') Then $include=False;
        If $include Then
          If $s='' Then
            $s=$use
          Else
            $s=$s&@CR&$use
          EndIf
        EndIf
      EndIf
  WEnd
  ; Close the search handle
  FileClose($search)
  Return $s;
EndFunc

Func FolderListFor($p,$nameonly)
  Local $s='';
  Local $search = FileFindFirstFile($p&"\*.*")

  ; Check if the search was successful
  If $search = -1 Then
      ;MsgBox(0, "Error", "No files/directories matched the search pattern")
      Return $s;
      Exit
  EndIf

  While 1
      Local $file = FileFindNextFile($search)
      If @error Then ExitLoop
      Local $pfile=$p&$bsl&$file
      Local $use=$pfile
      If $nameonly Then
        $use=$file
      EndIf
      ;MsgBox(4096, "File:", $file&@CRLF&FileGetAttrib($pfile))
      If StringInStr(FileGetAttrib($pfile),'D')<> 0 Then
        If $s='' Then
          $s=$use
        Else
          $s=$s&@CR&$use
        EndIf
      EndIf
  WEnd
  ; Close the search handle
  FileClose($search)
  Return $s;
EndFunc

Func CreateFn($p)
  Local $gu=FindGitUp($p)
  If $gu=='' Then
    Return ''
  EndIf
  Local $s=StringReplace($p,$gu,'')
  ;$s=StringMid($s,2)
  $s=$pfx&StringReplace($s,'\','-')
  Return $s
EndFunc

Func FindGitUp($p)
  Local $px=$p
  While True
    If FileExists($px&'\.git') Then
      Return $px
    EndIf
    $px=PathPeel($px)
    If $px=='' Then
      Return ''
    EndIf
  WEnd
EndFunc

Func PathPeel($p)
  Local $px=$p
  While StringRight($px,1)<>'\'
    $px=StringLeft($px,StringLen($px)-1);
  WEnd
  If StringLen($px)<3 Then Return ''
  Return StringLeft($px,StringLen($px)-1);
EndFunc


Func HasExt($p,$ext)
  Local $ex=FilePart($p,'e')
  Return $ex=$ext
EndFunc


Func FilePart($f,$n)
   Local $drive, $dir, $fn, $ext, $path
   _PathSplit($f, $drive, $dir, $fn, $ext)
   $path=StringLeft($dir,StringLen($dir)-1)
   If $n=='dl' Then Return StringLeft($drive,1)
   If $n=='d' Then Return $drive
   If $n=='drive' Then Return $drive
   If $n=='dir' Then Return $dir
   If $n=='n' Then Return $fn
   If $n=='name' Then Return $fn
   If $n=='e' Then Return StringRight($ext,StringLen($ext)-1)
   If $n=='ext' Then Return $ext
   If $n=='p' Then Return $drive&$path
   If $n=='path' Then Return $drive&$path
   If $n=='dp' Then Return $drive&$path&"\"
   If $n=='p_' Then Return $drive&$path&"\"
   If $n=='path_' Then Return $drive&$path&"\"
   If $n=='ne' Then Return $fn&$ext
   If $n=='nameext' Then Return $fn&$ext
   If $n=='pn' Then Return $path&"\"&$fn
   If $n=='dpn' Then Return $drive&$path&"\"&$fn
   Return SetError(-1,0,"")
EndFunc


Func _URIEncode($sData)
    ; Prog@ndy
    Local $aData = StringSplit(BinaryToString(StringToBinary($sData,4),1),"")
    Local $nChar
    $sData=""
    For $i = 1 To $aData[0]
        ConsoleWrite($aData[$i] & @CRLF)
        $nChar = Asc($aData[$i])
        Switch $nChar
            Case 45, 46, 48-57, 65 To 90, 95, 97 To 122, 126
                $sData &= $aData[$i]
            Case 32
                $sData &= "+"
            Case Else
                $sData &= "%" & Hex($nChar,2)
        EndSwitch
    Next
    Return $sData
EndFunc




Func _URIDecode($sData)
    ; Prog@ndy
    ;;MsgBox(0,'$sData',$sData);
    ;;MsgBox(0,'$sData',StringReplace($sData,"+"," ",0,1));
    Local $aData = StringSplit(StringReplace($sData,"+"," ",0,1),"%")
    ;;_ArrayDisplay($aData);
    $sData = ""
    For $i = 2 To $aData[0]
        $aData[1] &= Chr(Dec(StringLeft($aData[$i],2))) & StringTrimLeft($aData[$i],2)
    Next
    Return BinaryToString(StringToBinary($aData[1],1),4)
EndFunc



;;;;;;functions
Func ts()
  Return @YEAR&"."&@MON&"."&@MDAY&"."&@HOUR&"."&@MIN&"."&@SEC&"."&@MSEC
EndFunc
Func logline($line)
  Local $fh1=FileOpen($logg,1);
  If $fh1<>-1 Then
    FileWriteLine($fh1,$line)
    FileClose($fh1)
  EndIf
EndFunc
Func snarl($i,$t,$s)
  $snarl="C:\batch\Snarl_CMD.exe";
  $s1=StringReplace($s,'"',"'")
  $t1=StringReplace($t,'"',"'")
  $cmd=$snarl&' snShowMessage '&$i&' "'&$t1&'" "'&$s1&'"';
  Run($cmd)
EndFunc

