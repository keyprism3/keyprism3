cd %~dp0

rem C:\_\socialnitro-app\._vphtml\index.html
copy ..\._vphtml\publish\index.html ..\.assets\visual_paradigm_publish\index.html /y

copy ..\._vphtml\publish\content\blank_menu.html ..\.assets\visual_paradigm_publish\content\blank_menu.html /y
rem copy ..\._vphtml\publish\content\diagram_navigator.html ..\.assets\visual_paradigm_publish\content\diagram_navigator.html /y
copy ..\._vphtml\publish\content\project_content.html ..\.assets\visual_paradigm_publish\content\project_content.html /y

copy ..\._vphtml\publish\content\report.css ..\.assets\visual_paradigm_publish\content\report.css /y

copy ..\._vphtml\publish\content\link_popup.js ..\.assets\visual_paradigm_publish\content\link_popup.js /y
copy ..\._vphtml\publish\content\report.js ..\.assets\visual_paradigm_publish\content\report.js /y
copy ..\._vphtml\publish\content\tooltip.js ..\.assets\visual_paradigm_publish\content\tooltip.js /y
rem _tools\do-vp-report-style.bat
pause
