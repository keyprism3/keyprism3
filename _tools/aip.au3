; #INDEX# =======================================================================================================================
 ; Title .........: AutoIt Protocol v1.0.0.0
 ; AutoIt Version : 3.3
 ; Language ......: English (language independent)
 ; Description ...:
 ; Author(s) .....: MarkRobbins
 ; Copyright .....: Copyright (C) Mark C Robbins. All rights reserved.
 ; License .......: Artistic License 2.0, see Artistic.txt
 ;
 ; AutoIt Protocol is free software; you can redistribute it and/or modify
 ; it under the terms of the Artistic License as published by Larry Wall,
 ; either version 2.0, or (at your option) any later version.
 ;
 ; This program is distributed in the hope that it will be useful,
 ; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 ; See the Artistic License for more details.
 ;
 ; You should have received a copy of the Artistic License with this Kit,
 ; in the file named "Artistic.txt".  If not, you can get a copy from
 ; <http://www.perlfoundation.org/artistic_license_2_0> OR
 ; <http://www.opensource.org/licenses/artistic-license-2.0.php>
 ;
 ; ===============================================================================================================================

; #OVERVIEW# ===========================================================================================================
 ; SOURCE_______________________________________________________________________________________________________________
  ; Organization ..: Mark Robbins and Associates
  ; Author ........: Mark Robbins
 ; LOG__________________________________________________________________________________________________________________
  ; Created .......: 2015.03.22.05.08.37
  ; Modified ......: 2015.03.22.05.08.37
  ; Entries........: yyyy.mm.dd.hh.mm.ss Comments
 ; HEADER_______________________________________________________________________________________________________________
  ; Type ..........: Protocol
  ; Subtype .......:
  ; Name ..........: AutoIt Protocol
  ; Summary .......:
  ; Description ...:
  ;
  ; Remarks .......:
 ; DEVELOPMENT__________________________________________________________________________________________________________
  ; Issues ........:
  ; Status ........: [X] New
  ;                  [ ] Open
  ;                  [ ] InProgress
  ;                  [ ] Resolved
  ;                  [ ] Closed
 ; OTHER________________________________________________________________________________________________________________
  ; Related .......:
  ; Related Links .:
  ; Resources......:
 ; =====================================================================================================================
#NoTrayIcon
#include <Array.au3>
#include <File.au3>


#include <GuiConstantsEx.au3>
#include <AVIConstants.au3>
#include <TreeViewConstants.au3>
#include <GUIListBox.au3>
#include <Constants.au3>
#include <ListBoxConstants.au3>
#include <WindowsConstants.au3>

#include <GuiConstants.au3>
#include <Misc.au3>



Global $thisfile="C:\batch\aip.au3"
Global $logg="C:\$data\logs\aip-log.txt";
Global $timestamp=@YEAR&"."&@MON&"."&@MDAY&"."&@HOUR&"."&@MIN&"."&@SEC&"."&@MSEC

Global $testing=True

  ;Alters the method that is used to match window titles during search operations.
  ;1 = Match the title from the start (default)
  ;Opt("WinTitleMatchMode",1)
  ;2 = Match any substring in the title
  ;3 = Exact title match
  ;Opt("WinTitleMatchMode",3)
  ;4 = Advanced mode, see Window Titles & Text (Advanced)
  ;Opt("WinTitleMatchMode",4)
  ;-1 to -4 = force lower case match according to other type of match.

  ;Advanced Window Descriptions
  ;Identify a window by the following properties:
  ;
  ; TITLE - Window title
  ; CLASS - The internal window classname
  ; REGEXPTITLE - Window title using a regular expression (if the regular expression is wrong @error will be set to 2)
  ; REGEXPCLASS - Window classname using a regular expression (if the regular expression is wrong @error will be set to 2)
  ; LAST - Last window used in a previous AutoIt command
  ; ACTIVE - Currently active window
  ; X \ Y \ W \ H - The position and size of a window
  ; INSTANCE - The 1-based instance when all given properties match
  ; One or more properties are used in the title parameter of a window command in the format:

  ; [PROPERTY1:Value1; PROPERTY2:Value2]
  ; Note : if a Value must contain a ";" it must be doubled.
  ; e.g. Wait a window of classname "Notepad"
  ; WinWaitActive("[CLASS:Notepad]", "")
  ;
  ; e.g. Close the currently active window
  ; WinClose("[ACTIVE]", "")
  ;
  ; e.g. Wait for the 2nd instance of a window with title "My Window" and classname "My Class"
  ; WinWaitActive("[TITLE:My Window; CLASS:My Class; INSTANCE:2]", "")


If $CmdLine[0]<>0 Then
  $testing=False
EndIf

Local $p1="aip://edit/C%3A%5Cbatch%5Caip.au%33"

;E%3A%5C%24res%5Cicon%5Cextensions%5Capache_server_avatar-%39%36.png
$p1="aip://edit/E%3A%5C%24res%5Cicon%5Cextensions%5Capache_server_avatar-%39%36.png"
$p1="aip://pick/Atlassian"
$p1="aip://clip/Atlassian"
$p1="aip://clip/body.space.apps.section-app%20%3E%20div.content-container%20%3E%20section%23wrapper.apps--new.app-view.space.apps.section-app.is-revealed%20%3E%20nav.app-view__sidebar"

If Not $testing Then
  If $CmdLine[0]<>1 Then
    RunWait('"C:\Program Files (x86)\AutoIt3\AutoIt3.exe"'&' C:\batch\splash.au3 "Need 1 params - exiting" "'&$thisfile&'" "10"',""); ,@SW_NORMAL)
    Exit
  EndIf
  $p1=$CmdLine[1]
EndIf


;ClipPut(_URIEncode(ClipGet()));C%3A%5Cbatch%5Caip.au%33
;Exit

;aip://clip/body.space.apps.section-app%20%3E%20div.content-container%20%3E%20section%23wrapper.apps--new.app-view.space.apps.section-app.is-revealed%20%3E%20nav.app-view__sidebar


;Local $ss="body.space.apps.section-app%20%3E%20div.content-container%20%3E%20section%23wrapper.apps--new.app-view.space.apps.section-app.is-revealed%20%3E%20nav.app-view__sidebar";
;Local $jj=StringReplace($ss,"+"," ",0,1)
;$jj=_URIDecode1($ss);
;MsgBox(0,$thisfile,$jj)
;Exit





;MsgBox(0,$thisfile,$p1)
;ClipPut($p1)

Global $fa[1]

Local $cm=StringMid($p1,7,10000)
Local $cm0=$cm
Local $pp=StringInStr($cm,'/')
$cm=StringMid($cm,1,$pp-1)
Global $u=StringMid($cm0,$pp+1,10000)
Global $parms=''


;MsgBox(0,$thisfile,$u)
;Exit

;;_ArrayDisplay($a)

;$cm=$a[0]
;Global $u=$a[1]
;MsgBox(0,$thisfile,$u)
$u=_URIDecode($u)

If StringInStr($u,'?')<>0 Then
  $parms=StringMid($u,StringInStr($u,'?')+1,10000)
  $u=StringLeft($u,StringInStr($u,'?')-1)
EndIf

;If $parms<>'' Then
;  MsgBox(0,'parms',$parms)
;  MsgBox(0,'u',$u)
;EndIf


;aip://edit/C%3A%5Cbatch%5Caip.au%33
;aip://enc/xx
;"C:\Program Files (x86)\AutoIt3\Aut2Exe\Aut2Exe.exe" /in "C:\batch\aip.au3"
;body.space.apps.section-app > div.content-container > section#wrapper.apps--new.a
;MsgBox(0,$thisfile,$u)
;Exit
;aip://au3run/C%3A%5Cbatch%5Cact-launch-resophnotes.au%33/untuck
;aip://c9

;"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" "C:\batch\act-launch-resophnotes.au3" "untuck"


If $cm=='hello' Then
  MsgBox(0,$thisfile,'hello')
  Exit
EndIf

If $cm=='c9' Then
  Opt("WinTitleMatchMode",3)
  WinActivate('Developer Tools - https://pss2-lionps.c9users.io/')
  Opt("WinTitleMatchMode",2)
  WinActivate('- Google Chrome')
  Send('{esc}')
  Send('^6')
  Send('^r');
  Exit
EndIf

If $cm=='au3run' Then
  ;RunWait(@AutoItExe
  Local $a=StringSplit($u,'/')
  Local $s='';
  For $x=1 To $a[0]
    $s=$s&' "'&$a[$x]&'"';
  Next
  ;MsgBox(0,'',$s);
  ;ClipPut('"C:\Program Files (x86)\AutoIt3\AutoIt3.exe"'&$s);
  Run('"C:\Program Files (x86)\AutoIt3\AutoIt3.exe"'&$s,'');
  Exit
EndIf
If $cm=='enc' Then
  ClipPut(_URIEncode(ClipGet()));
  Exit;
EndIf

If $cm=='edit' Then
  If FileExists($u) Then
    ShellExecute ( $u, '', '' , "edit")
    Exit
  EndIf
  Bail('File not Found '&$u)
EndIf

If $cm=='open' Then
  If FileExists($u) Then
    ShellExecute ( $u, '', '' , "open")
    Exit
  EndIf
  Bail('File not Found '&$u)
EndIf

If $cm=='vplink' Then
  ;u= keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/PcSWS1qGAqAAAgyQ
  tryVpLink();
EndIf

If $cm=='thoughtuid' Then
  ;u= keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/PcSWS1qGAqAAAgyQ
  tryBrainUid()
EndIf



If $cm=='clip' Then
  ClipPut($u)
  ;snarl(10,$thisfile,$u);
  Exit
EndIf


If $cm=='pick' Then
  If pick($u) Then
    ;ShellExecute ( $u, '', '' , "edit")
    ;_ArrayDisplay($fa)
    pick2()
    Exit
  EndIf
  Bail('File not Found '&$u)
EndIf

If $cm=='webstorm' Then
  Local $a=StringSplit($u,'/',2);
  webstorm($a[0],$a[1]);
EndIf

If $cm=='x2' Then
  browsex2($u);
EndIf

If $cm=='slickedit' Then
  Local $a=StringSplit($u&'//','/',2);
  Slickedit($a[0],$a[1],$a[2]);
  Exit
  ; "C:\Program Files (x86)\AutoIt3\Aut2Exe\Aut2Exe_x64.exe" /in "C:\batch\aip.au3"
EndIf

Func browsex2($d)
  LaunchActEx($d)
  Exit
EndFunc


Func tryBrainUid()
  If Not isBrainOpen() Then
    bail('brain not open')
  EndIf
  goBrainLink()
EndFunc
Func isBrainOpen()
  Opt("WinTitleMatchMode",2)
  Return WinExists('- TheBrain 8','')
EndFunc
Func activateBrain()
  ;RunWait('"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" "C:\batch\act-launch-brain.au3"','');
  Opt("WinTitleMatchMode",2)
  WinActivate(' - TheBrain 8')
EndFunc
Func goBrainLink()
  activateBrain()
  Send('{esc}{esc}{esc}');
  Local $uid=$u;IniRead($ini,$sec,'thought_uid','')
  Send('{end}+{home}{del}');
  Sleep(100)
  ClipPut($uid)
  Sleep(100)
  Send('^v{enter}')
  Sleep(100)
  Send('{end}+{home}{del}');
EndFunc


Func tryVpLink()
  If Not isVpOpen() Then
    Bail('Visula Paradigm is not open')
  EndIf
  goVpLink()
EndFunc
Func isVpOpen()
  Opt("WinTitleMatchMode",2)
  Return WinExists('Visual Paradigm for UML Enterprise Edition','') Or WinExists('VP-UML EE','')
EndFunc
Func goVpLink()
  activateVp()
  Send('{esc}{esc}{esc}');
  Sleep(100)
  Send('^l');
  Sleep(100)
  til('Open Link');
  Local $url=$u
  ClipPut($url)
  Sleep(100)
  Send('^v')
  Sleep(100)
  Send('!o')
EndFunc
Func activateVp()
  RunWait('"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" "C:\batch\act-launch-visualparadigm.au3"','');
EndFunc

Func til($t,$secs=2)
  Local $ww=WinWaitActive($t,'',$secs)
  If $ww=0 Then
    MsgBox(0,'Could not find window, exiting',$t);
    Exit
  EndIf
EndFunc


Func Slickedit($f,$l,$c)
  Local $cmd='"C:\se17\win\vs.exe" "-#cmdline_slickedit_goto/'&$f
  If $l<>'' Then
    $cmd=$cmd&'/'&$l;
    If $c<>'' Then
      $cmd=$cmd&'/'&$c;
    EndIf
  EndIf
  Run($cmd,"")
EndFunc

Func LaunchActEx($p)
  Opt("WinTitleMatchMode", 4);adv
  Local $t=StringReplace($p,"\","\\");
  Local $ex="[REGEXPTITLE:xplorer� .*? - "&$t&"$]"
  If WinExists($ex) then
    WinActivate ($ex)
    Exit
  Else
    LaunchEx($p)
    ;Exit
    ;$act='r'
  EndIf
EndFunc



Func LaunchEx($p)
  Local $cmd='"C:\Program Files\zabkat\xplorer2\xplorer2_64.exe" /1 /T "'&$p&'"';
  Run($cmd,"")
EndFunc


Func webstorm($f,$l)
;"C:\Program Files (x86)\JetBrains\WebStorm 11.0.3\bin\WebStorm.exe" --line 6 "C:\_\meteor-react\app\tests\jasmine\specs\HelloWorld-spec.js"
  Local $cmd = "C:\Program Files (x86)\JetBrains\WebStorm 2016.2.4\bin\WebStorm.exe";
  If $l<>'' Then
    $cmd=$cmd&' --line '&$l;
  EndIf
  $cmd=$cmd&' "'&$f&'"';
  Run($cmd);
  Opt("WinTitleMatchMode",2)
  WinActivate("- WebStorm 11");
EndFunc

Func pick($u)
  Local $d='E:\$res\icon\extensions\'
  Local $search = FileFindFirstFile($d&"*.*")
  If $search = -1 Then
      MsgBox(0, "Error", "No files/directories matched the search pattern")
      Exit
  EndIf
  While 1
      Local $file = FileFindNextFile($search)
      If @error Then ExitLoop
      If StringInStr(StringLower($file),StringLower($u)) Then
        _ArrayAdd($fa,$file)
      EndIf
      ;_ArrayAdd($fa,$file)
      ;If StringInStr(FileGetAttrib($file),"N") Then
      ;    ;_ArrayAdd($fa,$file)
      ;  Local $drive, $dir, $fn, $ext
      ;  _PathSplit($file, $drive, $dir, $fn, $ext)
      ;EndIf
      ;MsgBox(4096, "File:", $file)
  WEnd
  ; Close the search handle
  FileClose($search)
  Return True
EndFunc


Func pick2()
  ;Return
  Local $d='E:\$res\icon\extensions\'
  $hGUI = GUICreate($u, 350, 400)
  $hButton = GUICtrlCreateButton("GO!", 10, 330, 100, 30)
  ; Create LISTBOX
  Global $hListBox = _GUICtrlListBox_Create($hGUI, "String upon creation", 10, 30, 300, 290, BitOR($LBS_SORT, $WS_HSCROLL, $WS_VSCROLL, $WS_BORDER, $LBS_HASSTRINGS, $LBS_MULTIPLESEL));BitOR($LBS_SORT, $WS_HSCROLL, $WS_VSCROLL, $WS_BORDER, $LBS_ExtendedSEL, $LBS_HASSTRINGS, $LBS_MULTIPLESEL))
  ;_GUICtrlListBox_Create($hWnd, $sText, $iX, $iY[, $iWidth = 100[, $iHeight = 200[, $iStyle = 0x00B00002[, $iExStyle = 0x00000200]]]])
  ;$hListBox=GuiCtrlCreateList("", 10, 30, 300, 90)
  ; Add files
  _GUICtrlListBox_BeginUpdate($hListBox)
  _GUICtrlListBox_ResetContent($hListBox)
  _GUICtrlListBox_InitStorage($hListBox, 100, 4096)
  For $x=1 To UBound($fa)-1
    _GUICtrlListBox_AddString($hListBox, $d&$fa[$x])
  Next

  ;_GUICtrlListBox_Dir($hListBox, "C:\h264\*.*")
  _GUICtrlListBox_EndUpdate($hListBox)



  HotKeySet("{ENTER}", "DoCopy")
  ; GUI MESSAGE LOOP
  GUISetState()
  ;GUICtrlSetState ( $hListBox, $GUI_FOCUS )
  ControlFocus("","",$hListBox)
  While 1
      $guimsg = GUIGetMsg()
     ;MsgBox(4160, "Information", "Gui Selected: " & $guimsg)
     Switch $guimsg
       Case $hButton
        DoCopy()
        Exit
       Case $GUI_EVENT_CLOSE
        Exit
    EndSwitch
  WEnd
  Return
EndFunc

Func DoCopy()
  $ia=_GUICtrlListBox_GetSelItemsText($hListBox)
  copyFiles($ia)
  Exit
EndFunc

Func copyFiles($ia)
  ;_ArrayDisplay($ia)
  Local $d='U:\inetpub\wwwroot\images\podio\projectlist\'
  For $x=1 To UBound($ia)-1
    FileMove($ia[$x],$d,1)
    ;MsgBox(0,$x,$ia[$x]);
  Next
EndFunc


Func EnterWasPressed()
  MsgBox(0,"enter",'')
EndFunc

Func Bail($s)
  MsgBox(0,$thisfile,$s)
  Exit
EndFunc

;ShellExecute ( "filename" [, "parameters" [, "workingdir" [, "verb" [, showflag]]]] )

;ClipPut($cm)
;edit/C%3A%5Cbatch%5Caip.au%33
;MsgBox(0,$cm,'')

;$p1=_URIDecode($p1)

;MsgBox(0,$p1,'');aip://hello





;;;;;;functions
Func ts()
  Return @YEAR&"."&@MON&"."&@MDAY&"."&@HOUR&"."&@MIN&"."&@SEC&"."&@MSEC
EndFunc
Func logline($line)
  Local $fh1=FileOpen($logg,1);
  If $fh1<>-1 Then
    FileWriteLine($fh1,$line)
    FileClose($fh1)
  EndIf
EndFunc
Func snarl($i,$t,$s)
  $snarl="C:\batch\Snarl_CMD.exe";
  $s1=StringReplace($s,'"',"'")
  $t1=StringReplace($t,'"',"'")
  $cmd=$snarl&' snShowMessage '&$i&' "'&$t1&'" "'&$s1&'"';
  Run($cmd)
EndFunc

Func _URIEncode($sData)
    ; Prog@ndy
    Local $aData = StringSplit(BinaryToString(StringToBinary($sData,4),1),"")
    Local $nChar
    $sData=""
    For $i = 1 To $aData[0]
        ConsoleWrite($aData[$i] & @CRLF)
        $nChar = Asc($aData[$i])
        Switch $nChar
            Case 45, 46, 48-57, 65 To 90, 95, 97 To 122, 126
                $sData &= $aData[$i]
            Case 32
                $sData &= "+"
            Case Else
                $sData &= "%" & Hex($nChar,2)
        EndSwitch
    Next
    Return $sData
EndFunc




Func _URIDecode($sData)
    ; Prog@ndy
    ;;MsgBox(0,'$sData',$sData);
    ;;MsgBox(0,'$sData',StringReplace($sData,"+"," ",0,1));
    Local $aData = StringSplit(StringReplace($sData,"+"," ",0,1),"%")
    ;;_ArrayDisplay($aData);
    $sData = ""
    For $i = 2 To $aData[0]
        $aData[1] &= Chr(Dec(StringLeft($aData[$i],2))) & StringTrimLeft($aData[$i],2)
    Next
    Return BinaryToString(StringToBinary($aData[1],1),4)
EndFunc
