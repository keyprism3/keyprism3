REM _tools\do-esdoc.bat
cd %~dp0

mkdir .\..\._esdoc\_

SET D=ast
CALL :KILLDIR
SET D=class
CALL :KILLDIR
SET D=css
CALL :KILLDIR
SET D=file
CALL :KILLDIR
SET D=function
CALL :KILLDIR
SET D=image
CALL :KILLDIR
SET D=script
CALL :KILLDIR
SET D=variable
CALL :KILLDIR
SET D=test-file
CALL :KILLDIR
SET D=variable
CALL :KILLDIR

del .\..\._esdoc\*.html
del .\..\._esdoc\*.json
REM del .\..\._esdoc\*.md
del .\..\._esdoc\*.svg

REM EXIT

rem ren README.md README.md0
rem copy ..\README.md README.md
nvm use 7.4.0
sleep 4
call do-esdoc-pt1.bat
nvm use 4.6.2
sleep 4

rem del README.md
rem ren README.md0 README.md
rem call do-esdoc-pt2.bat
cd ..
xcopy .assets\esdoc\script ._esdoc\script /S /Y /I /F
xcopy .assets\esdoc\css ._esdoc\css /S /Y /I /F


REM cd C:\_\socialnitro-aux-repos\._esdoc
cd ._esdoc
rem start powershell -c (New-Object Media.SoundPlayer "C:\Windows\Media\notify.wav").PlaySync();

SET D=ast
CALL :EXPLDIR
SET D=class
CALL :EXPLDIR
SET D=css
CALL :EXPLDIR
SET D=file
CALL :EXPLDIR
SET D=function
CALL :EXPLDIR
SET D=image
CALL :EXPLDIR
SET D=script
CALL :EXPLDIR
SET D=script\prettify
CALL :EXPLDIR
cd..
SET D=variable
CALL :EXPLDIR
SET D=test-file
CALL :EXPLDIR
SET D=variable
CALL :EXPLDIR

del /f /s /q _ 1>nul
rmdir /s /q _
rmdir /s /q _


REM dir
REM GOTO :EOF
git add -u
git add .
git commit -am auto
git push


GOTO :EOF


:KILLDIR
IF NOT EXIST .\..\._esdoc\%D% EXIT /B
MKDIR .\..\._esdoc\_\%D%

ATTRIB -s -h .\..\._esdoc\%D%\Desktop.ini
COPY .\..\._esdoc\%D%\Desktop.ini .\..\._esdoc\_\%D%
ATTRIB +s +h .\..\._esdoc\%D%\Desktop.ini


ATTRIB -s -h .\..\._esdoc\%D%\FolderMarker.ico
COPY .\..\._esdoc\%D%\*.ico .\..\._esdoc\_\%D%
ATTRIB +s +h .\..\._esdoc\%D%\FolderMarker.ico

COPY .\..\._esdoc\%D%\*.$explhere .\..\._esdoc\_\%D%
COPY .\..\._esdoc\%D%\README.html .\..\._esdoc\_\%D%
COPY .\..\._esdoc\%D%\*.md .\..\._esdoc\_\%D%
COPY .\..\._esdoc\%D%\*.url .\..\._esdoc\_\%D%


del /f /s /q .\..\._esdoc\%D% 1>nul
rmdir /s /q .\..\._esdoc\%D%
rmdir /s /q .\..\._esdoc\%D%
EXIT /B

:EXPLDIR
IF NOT EXIST %D% EXIT /B

COPY .\_\%D%\Desktop.ini .\%D% /y
ATTRIB +s +h .\%D%\Desktop.ini

COPY .\_\%D%\FolderMarker.ico .\%D% /y
ATTRIB +s +h .\%D%\FolderMarker.ico

COPY .\_\%D%\*.$explhere .\%D% /y
COPY .\_\%D%\*.md .\%D% /y
COPY .\_\%D%\*.url .\%D% /y

ATTRIB +r %D%

EXIT /B



:EOF


