## RMF-_docs

`C:\_\keyprism3\_docs`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_docs) _Project Readmes_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FU.GAPVqGAqAAAhLD) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_docs%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FWfXHvVqGAqAAAhDU) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%38D%32D%32%30%32C-%38%39%31F-%38A%32%30-CF%39F-D%34%39%30%39%33B%32%31%31%37%34) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/_docs/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-16) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_docs%5Cconventions.md) `conventions.md` - Tree Categorized Convention Lists
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_docs%5Cfacts.md) `facts.md` - List in order of Discovery
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_docs%5Clog.md) `log.md` - Zdot List Description
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_docs%5Cobserve.md) `observe.md` - List - not quite `seems`, which are more important witnessings, observe is more general and less critical
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_docs%5CREADME.md) `README.md`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_docs%5CRMF-_docs.md) `RMF-_docs.md`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_docs%5Cseems.md) `seems.md` - Tree Categorized Lists - Not Quite Facts Yet

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_docs%5Cprocedures%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%33%39%26webthought_id%3D%34%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FQ%34HnvVqGAqAAAhD%37) `procedures` - Markdowns For Procedure Instructions
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_docs%5Csnapshots%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%30%26webthought_id%3D%34%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FBUaXvVqGAqAAAhER) `snapshots` - Screenshot `png`s
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_docs%5Cstages%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%31%26webthought_id%3D%33%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FMXDXvVqGAqAAAhEo) `stages` - Like `proceedures` but at project startup and config

### Anticipates

- `<platform>.md`
- `<aspect>.md`
