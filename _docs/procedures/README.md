## README

`C:\_\keyprism3\_docs\procedures`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_docs%5Cprocedures) _Markdowns For Procedure Instructions_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_docs%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%36%26webthought_id%3D%31%36%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FWfXHvVqGAqAAAhDU) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_docs%5Cprocedures%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FQ%34HnvVqGAqAAAhD%37) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/FD%33%34%36%31%36%31-%37%34F%34-%37%34%34%31-%36E%37%33-%33%33%35D%35C%30%31%36EAA) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/_docs/procedures/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-39) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_docs%5Cprocedures%5CREADME.md) `README.md`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_docs%5Cprocedures%5CRMF-_docs-procedures.md) `RMF-_docs-procedures.md`

### Folder Built-ins

_none_

### Anticipates

- `<procedure>.md`
