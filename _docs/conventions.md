# Conventions


## Routing

- path lower case
- Name Upper Single Word
- ComponentName Upper Single Word
- PageName Upper Single Word


## Locations

Components go packaged in sister directory `_components` if possible, else in `ui/components`


## Naming

### Blaze

- __Name:__ `one two three`
- __Path:__ `one_two_three/one_two_three.*`
- __Template:__ `oneTwoThree`


