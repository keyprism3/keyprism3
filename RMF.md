## RMF

`C:\_\keyprism3`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `2`
- `webthought_id` - `2`
- `thought_uid` - `8DD9562E-A35E-F4FA-06B4-8324540C5545`
- `vp_url` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/U.GAPVqGAqAAAhLD`
- `FmColorName` - `purple`
- `ColorHex` - `#BE93E3`
- `ColorRgb` - `190,147,227`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33) _KeyPrism3 Project_

KeyPrism is a dynamic cheatsheet for keyboard shortcuts. Keyboard information comes in various layouts and the transition between the layouts is animated so the user can better trace what the organization really is, thus enabling an information push from software into the human brain.

There is also a small [demo](http://mrobbinsassoc.com/demo/keyprism_vpw/) of the concept.

### Documentation and README

This project also contains an outer project (the `Tie-in` - a work in progress) which is a documentation integration of [TheBrain](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09), [EsDocs](http://mrobbinsassoc.com/projects/keyprism3/esdocs/), [Plato](http://mrobbinsassoc.com/projects/keyprism3/plato/), [Yodiz Issue Management](https://app.yodiz.com/plan/pages/scrum-board.vz?cid=30887), and [Visual Paradigm](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/) (aka the `Tie-in`).

The README's are generated automatically using an Autoit3 script [`_tools\readme-folder-ini.au3`](https://bitbucket.org/keyprism3/keyprism3/src/master/_tools/readme-folder-ini.au3) which takes information in `Desktop.ini` files and creates the README for each folder.

Under windows, links on Local Item icons currently use `aip` protocol which you will not have, but such opens files locally. See [`_tools/aip.au3`](https://bitbucket.org/keyprism3/keyprism3/src/master/_tools/aip.au3) and [`_tools/aip.reg`](https://bitbucket.org/keyprism3/keyprism3/src/master/_tools/aip.reg). `_tools/aip.au3` will have to be compiled to `aip.exe` via AutoIt3 (aka Right-Click `_tools/aip.au3`, select Compile), then change the `aip.exe` path in `aip.reg` before registering it by double clicking `aip.reg`.

Please use Chrome for the `Tie-in`. On Chrome, in order to get the [`Tie-in`](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/) to work you will need the [Tampermonkey Extension](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) installed.

[Firefox Greasmonkey Add-On](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) may also work, but is untested.

After you have a UserScripts extension installed (Tampermonkey or Greasemonkey), install the [Keyprism3 WebBrain UserScript](https://openuserjs.org/scripts/MarkRobbins/Keyprism3_WebBrain) and the the [Keyprism3 Yodiz UserScript](https://openuserjs.org/scripts/MarkRobbins/Keyprism3_Yodiz)

## Reporters

- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09) - WebBrain

- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/) - EsDocs

- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/) - Plato Complexity Report

- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/) - Visual Paradigm - Tie-in with WebBrain

## Follow the Project

Issue Management is being done on Yodiz (also becoming part of the `Tie-in`) and a public viewer login is available with the username `mindprism@gmail.com` and a password of `public#1` -- login with these credentials at [Yodiz App](https://app.yodiz.com/). Please do not alter that account profile.

You may also be able to [get an invitation](https://mrobbinsassocslack2keyprism3.herokuapp.com/) to the Slack room `#keyprism3` on [mrobbinsassoc.slack.com](https://mrobbinsassoc.slack.com/) or [a general invitation](https://mrobbinsassocslack.herokuapp.com/)

If neither of those methods work, send me an email at [mark.robbins@mrobbinsassoc.com](mailto:mark.robbins@mrobbinsassoc.com) to request an invitation.

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FU.GAPVqGAqAAAhLD) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FU.GAPVqGAqAAAhLD) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%38DD%39%35%36%32E-A%33%35E-F%34FA-%30%36B%34-%38%33%32%34%35%34%30C%35%35%34%35) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-2) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.assets%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%38%26webthought_id%3D%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FyOFsx%31qGAqAAAgq_%2FuDwcx%31qGAqAAAgsh%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%39xCKvVqGAqAAAg%30x) `.assets` - Various Assets for tools and site
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.git%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%30%26webthought_id%3D%31%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FB%32eNvVqGAqAAAg%37V)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.git%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%30%26webthought_id%3D%31%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FB%32eNvVqGAqAAAg%37V) `.git` - Wrapper Repository
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.idea%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%31%26webthought_id%3D%31%31%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%37GXtvVqGAqAAAg%37.) `.idea` - Webstorm Configuration
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.scrap%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%32%26webthought_id%3D%31%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FbhzjvVqGAqAAAg.P) `.scrap` - Developer Scrap Area
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.temp%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%33%26webthought_id%3D%31%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%38dSbvVqGAqAAAhA_) `.temp` - Developer Temp Area
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.types%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%34%26webthought_id%3D%31%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FbTY%37vVqGAqAAAhBs) `.types` - Types Local to project for Webstorm
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.webstorm%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%35%26webthought_id%3D%31%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FoHD%37vVqGAqAAAhCT) `.webstorm` - Global Types for Webstorm
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._esdoc%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%33%26webthought_id%3D%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%34cdvCNqGAqAAAgtu%2Fhi%30fCNqGAqAAAguy%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FxNZ%36PVqGAqAAAhy%30) `._esdoc` - EsDoc Output for KeyPrism3
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%26webthought_id%3D%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2Fk.%35%38SNqGAqAAAhXC%2F%38MN%38SNqGAqAAAhXO%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fo%34jOPVqGAqAAAh%30C) `._plato` - Plato Report Output
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%26webthought_id%3D%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FggwpPVqGAqAAAh%30%34) `._vphtml` - Visual Paradigm Output Home
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%39%26webthought_id%3D%31%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Ft%34sSi%31qGAqAAAgeS) `app` - Main Application
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_docs%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%36%26webthought_id%3D%31%36%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FWfXHvVqGAqAAAhDU) `_docs` - Project Readmes
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%26webthought_id%3D%31%37%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FNLB%33vVqGAqAAAhFG) `_extra`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_tools%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%26webthought_id%3D%31%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FMGNsi%31qGAqAAAgdl) `_tools` - Various Batch Tools

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C%21i.txt) `!i.txt` - Information Pad, links to `_extra/_i/` and `.scrap/_extra/_i/`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C%21t.txt) `!t.txt` - Taskpad
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C%21w.txt) `!w.txt` - Scratchpad
- [![](http://mrobbinsassoc.com/images/icons/md/_gitignore-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.gitignore) `.gitignore`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Ckeyprism%33.url) `keyprism3.url`
- [![](http://mrobbinsassoc.com/images/icons/md/_vpj-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Ckeyprism%33_vpw.vpj) `keyprism3_vpw.vpj` - SlickEdit Project
- [![](http://mrobbinsassoc.com/images/icons/md/_vpw-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Ckeyprism%33_vpw.vpw) `keyprism3_vpw.vpw` - SlickEdit Workspace
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5CREADME.md) `README.md`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5CRMF.md) `RMF.md`

