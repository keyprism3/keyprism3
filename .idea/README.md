## README

`C:\_\keyprism3\.idea`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.idea) _Webstorm Configuration_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FU.GAPVqGAqAAAhLD) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.idea%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%37GXtvVqGAqAAAg%37.) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%39%36B%37C%31A%34-%34D%37%32-%37C%36E-%38%34BA-%34A%36DADB%36D%32%30E) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.idea/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-11) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.idea%5Cencodings.xml) `encodings.xml` - `file:charset`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.idea%5Ckeyprism%33_vpw.iml) `keyprism3_vpw.iml` - folder types, exludes,libraries
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.idea%5Cmodules.xml) `modules.xml` - ProjectModuleManager references `*.iml`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.idea%5Cvcs.xml) `vcs.xml` - VcsDirectoryMappings Git
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C.idea%5CwatcherTasks.xml) `watcherTasks.xml` - File Watchers: `Scss Indexer` - `_tools/styleindexer.bat`

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.idea%5Cdictionaries%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%38%26webthought_id%3D%33%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%33.FdvVqGAqAAAg%38a) `dictionaries` - `<username>` dictionaries
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.idea%5CinspectionProfiles%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%39%26webthought_id%3D%33%37%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FG%34q%39vVqGAqAAAg%38%30) `inspectionProfiles` - Various Inspection Profile Configurations
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.idea%5CjsLinters%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%33%30%26webthought_id%3D%33%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FDt%39%39vVqGAqAAAg%39C) `jsLinters` - Various Linter Configurations
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.idea%5Clibraries%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%33%31%26webthought_id%3D%33%36%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FdG%30DvVqGAqAAAg%39S) `libraries` - Webstorm Auto Libraries
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.idea%5CrunConfigurations%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%33%32%26webthought_id%3D%33%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FiMFDvVqGAqAAAg%39i) `runConfigurations` - Webstorm Runners
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C.idea%5Cscopes%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%33%33%26webthought_id%3D%33%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FQSojvVqGAqAAAg%39%36) `scopes` - Webstorm Scopes

### Anticipates

_none_
