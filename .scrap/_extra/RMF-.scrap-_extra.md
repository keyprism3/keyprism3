## RMF-.scrap-_extra

`C:\_\keyprism3\.scrap\_extra`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5C.scrap%5C_extra) _Alternative Area for `_extra`_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.scrap/_extra/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-34) - webbrain

### File Built-ins

_none_

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-light-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5C.scrap%5C_extra%5C_i) `_i` - Deeper Stash for `_extra/_i`

### Anticipates

_none_
