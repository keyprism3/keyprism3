## README

`C:\_\keyprism3\.scrap\eslint\full`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5C.scrap%5Ceslint%5Cfull) _EsLint RC File_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.scrap/eslint/full/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-36) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_eslintrc-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C.scrap%5Ceslint%5Cfull%5C.eslintrc) `.eslintrc` - Settings

### Folder Built-ins

_none_

### Anticipates

_none_
