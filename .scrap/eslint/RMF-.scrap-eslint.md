## RMF-.scrap-eslint

`C:\_\keyprism3\.scrap\eslint`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5C.scrap%5Ceslint) _Stash for EsLint Confiigs_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/.scrap/eslint/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-35) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5C.scrap%5Ceslint%5Ceslintrc.json) `eslintrc.json`

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5C.scrap%5Ceslint%5Cfull) `full` - EsLint RC File
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5C.scrap%5Ceslint%5Cother) `other` - EsLint RC file

### Anticipates

_none_
