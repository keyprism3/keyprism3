## RMF-_extra

`C:\_\keyprism3\_extra`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra) _summary_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FU.GAPVqGAqAAAhLD) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FNLB%33vVqGAqAAAhFG) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%35%36%39A%31%37%38A-ADF%33-%35CA%36-ED%33%36-%30B%35A%34D%34C%30%35%32E) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/_extra/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-17) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

_none_

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%39%31%26webthought_id%3D%39%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FlWAvvVqGAqAAAhF%32) `vp` - Visual Paradigm Workspace Folders
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%39%30%26webthought_id%3D%39%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FnHiPvVqGAqAAAhFo) `_i`

### Anticipates

_none_
