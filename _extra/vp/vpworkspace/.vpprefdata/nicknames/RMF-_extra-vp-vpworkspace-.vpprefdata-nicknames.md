## RMF-_extra-vp-vpworkspace-.vpprefdata-nicknames

`C:\_\keyprism3\_extra\vp\vpworkspace\.vpprefdata\nicknames`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cnicknames) _Visual Paradigm Nicknames_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%39%36%26webthought_id%3D%39%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fj.cAfVqGAqAAAhHI) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cnicknames%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%38%31%34EfVqGAqAAAhIY) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/F%35%37%36%37B%38%31-%36%35%33B-%36%31EA-%33AC%30-%36%32%36B%30A%37%38%31%35BC) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/_extra/vp/vpworkspace/.vpprefdata/nicknames/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-100) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cnicknames%5CLwdrPVqGAqAAAh%32l) `LwdrPVqGAqAAAh2l` - Xml Nickname
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cnicknames%5CNickNameIndexing.xml) `NickNameIndexing.xml` - Maps Files here to paths

### Folder Built-ins

_none_

### Anticipates

- `<random>` - Xml Nicknames
