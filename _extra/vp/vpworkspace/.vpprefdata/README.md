## README

`C:\_\keyprism3\_extra\vp\vpworkspace\.vpprefdata`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata) _Visual Paradigm Project Preferences_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%39%32%26webthought_id%3D%39%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FIIjvvVqGAqAAAhGG) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fj.cAfVqGAqAAAhHI) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/CECC%33%34F%32-%35%34%33%34-%31%30B%31-BCBB-%35%30%34%37%36%32%33%32%38%35F%30) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/_extra/vp/vpworkspace/.vpprefdata/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-96) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5C.systemproject.vpp) `.systemproject.vpp` - Binary, SQLite
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5C.systemproject.vpp~%39%38) `.systemproject.vpp~98`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5C.systemproject.vpp~%39%39) `.systemproject.vpp~99`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5C.vp.preference) `.vp.preference` - Project Preferences Xml

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cdictionary%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%39%37%26webthought_id%3D%39%37%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fn.cgfVqGAqAAAhHf) `dictionary` - Visual Paradigm Dictionary Files
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CLayoutPerspectiveData%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%39%38%26webthought_id%3D%39%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FBQ%37IfVqGAqAAAhHx) `LayoutPerspectiveData` - Visual Paradigm Layout Data
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%39%39%26webthought_id%3D%39%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FibwYfVqGAqAAAhIG) `modelTypePreferData` - Visual Paradigm Model Type Defaults
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cnicknames%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%30%30%26webthought_id%3D%31%30%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%38%31%34EfVqGAqAAAhIY) `nicknames` - Visual Paradigm Nicknames
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CprojectsViewInfo%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%30%31%26webthought_id%3D%31%30%31%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FRyqkfVqGAqAAAhIn) `projectsViewInfo` - Visual Paradigm DiagramOpenInfos
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Ctreestates%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%30%32%26webthought_id%3D%31%30%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FTf%32%30fVqGAqAAAhI_)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Ctreestates%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%30%32%26webthought_id%3D%31%30%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FTf%32%30fVqGAqAAAhI_) `treestates` - Visual Paradigm Treestate Data

### Anticipates

- Unknown
