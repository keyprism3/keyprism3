## RMF-_extra-vp-vpworkspace-.vpprefdata-modelTypePreferData

`C:\_\keyprism3\_extra\vp\vpworkspace\.vpprefdata\modelTypePreferData`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData) _Visual Paradigm Model Type Defaults_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%39%36%26webthought_id%3D%39%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fj.cAfVqGAqAAAhHI) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FibwYfVqGAqAAAhIG) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%35D%32%38%33%35%39%34-FCB%31-B%38%33%35-%30%33%37%31-%35%30%34B%38%31%34CED%33B) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/_extra/vp/vpworkspace/.vpprefdata/modelTypePreferData/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-99) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CActor) `Actor` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CArtifact) `Artifact` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CBMMStrategy) `BMMStrategy` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CBMMTactic) `BMMTactic` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CBusinessRule) `BusinessRule` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CBusinessRuleGroup) `BusinessRuleGroup` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CClass) `Class` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CComponent) `Component` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CDiagramOverview) `DiagramOverview` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CFactType) `FactType` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CGlossary) `Glossary` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CInstanceSpecification) `InstanceSpecification` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CModel) `Model` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CNode) `Node` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CPackage) `Package` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CRectangle) `Rectangle` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CSubsystem) `Subsystem` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CUILabel) `UILabel` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CUITabbedHeader) `UITabbedHeader` - Text, unstudied format

### Folder Built-ins

_none_

### Anticipates

- Unknown
