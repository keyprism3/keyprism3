## README

`C:\_\keyprism3\_extra\vp\vpworkspace\.vpprefdata\projectsViewInfo`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CprojectsViewInfo) _Visual Paradigm DiagramOpenInfos_

Git Ignore extensionless here

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%39%36%26webthought_id%3D%39%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fj.cAfVqGAqAAAhHI) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CprojectsViewInfo%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FRyqkfVqGAqAAAhIn) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/E%35%36BE%38%38F-%32%34%37B-%39%37%32%32-E%33%36%38-EB%36A%38B%39B%39%33B%31) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/_extra/vp/vpworkspace/.vpprefdata/projectsViewInfo/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-101) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

_none_

### Folder Built-ins

_none_

### Anticipates

_none_
