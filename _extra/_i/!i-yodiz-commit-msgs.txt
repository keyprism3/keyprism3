In your commit message, add following notations to link commits to Tasks and Issues at Yodiz.

 For Tasks: use notation @T25
The commit message will be included in Task with ID 25.
 To mark task done, use notation @T25:R
"R" will indicate that this task is completed and Yodiz system will update the task status automatically.
 For Issues: use notation @B12
The commit message will be included in Issue with ID 12.
 To mark Issue as "resolved", use notation @B12:R
"R" will indicate that this issue is resolved and Yodiz system will update the issue status automatically

Can only use :R
