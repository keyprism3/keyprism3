## RMF-_extra-_i

`C:\_\keyprism3\_extra\_i`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-light-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i) _summary_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C_extra%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%26webthought_id%3D%31%37%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FNLB%33vVqGAqAAAhFG) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FnHiPvVqGAqAAAhFo) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/A%30FEFC%36%37-%36%36%30%34-%39%35CB-%35%37%39%35-E%35%39%34%37%34%39FA%35%33%37) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/_extra/_i/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-90) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-chrome-meter-element-css-map.txt) `!i-chrome-meter-element-css-map.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-code-header-nav-li.txt) `!i-code-header-nav-li.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-css-filters.txt) `!i-css-filters.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-eslint-base.txt) `!i-eslint-base.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-eslint.txt) `!i-eslint.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-galaxy-error.txt) `!i-galaxy-error.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-gravatar-attribs.txt) `!i-gravatar-attribs.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-gravatar-defaults.txt) `!i-gravatar-defaults.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-header.txt) `!i-header.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-import-export-syntax.txt) `!i-import-export-syntax.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-libs-to-add.txt) `!i-libs-to-add.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-materialize-button-classes.txt) `!i-materialize-button-classes.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-materialize-colors.txt) `!i-materialize-colors.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-materialize-variables.txt) `!i-materialize-variables.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-meteor-structure-examples.txt) `!i-meteor-structure-examples.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-new-package-ui.txt) `!i-new-package-ui.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-plantuml-skinparam.txt) `!i-plantuml-skinparam.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-prj-dir-list.txt) `!i-prj-dir-list.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-react-inception-pattern.txt) `!i-react-inception-pattern.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-React-propTypes.txt) `!i-React-propTypes.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-RegisterForm-tmp.txt) `!i-RegisterForm-tmp.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-ripple-code.txt) `!i-ripple-code.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-temps.txt) `!i-temps.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-terms-html.txt) `!i-terms-html.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-urls.txt) `!i-urls.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-yodiz-commit-msgs.txt) `!i-yodiz-commit-msgs.txt`
- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C_extra%5C_i%5C%21i-zxcvbn-retval.txt) `!i-zxcvbn-retval.txt`

### Folder Built-ins

_none_

### Anticipates

_none_
