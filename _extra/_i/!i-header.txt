import React from 'react';

export default class Header extends React.Component{
  constructor(){
    super();
    this.displayName="Header";
    this.mixins=[ReactMeteorData];
    this.propTypes={};
  }
  getDefaultProps(){
    return {};
  }
  getMeteorData(){
    return {
      currentUser: Meteor.user()
    };
  }
  handleLogout() {
    Meteor.logout();
    var strings=Header.constStrings();
    FlowRouter.go(strings.routeLogoutSuccess);
  }
  getInitialState(){
    return {};
  }
  componentWillMount(){
  }
  componentDidMount(){
  }
  componentWillReceiveProps(nextProps){
  }
  shouldComponentUpdate(nextProps, nextState){
    return true;
  }
  componentWillUpdate(nextProps, nextState){
  }
  componentDidUpdate(prevProps, prevState){
  }
  componentWillUnmount(){
  }
  render(){
    return (
      <div id="header">Header</div>
    );
  }
};

Header.returnFalse=()=>{
    return false;
};
Header.constStrings=()=>{
    return {
      routeLogoutSuccess:'Home'
    }
  }
};

