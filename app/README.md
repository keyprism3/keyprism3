## README

`C:\_\keyprism3\app`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `19`
- `webthought_id` - `19`
- `thought_uid` - `FCC6AC1B-79B5-4908-234C-496BA9D94B2A`
- `vp_url` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/t4sSi1qGAqAAAgeS`
- `FmColorName` - `purple-dark`
- `ColorHex` - `#BE67D5`
- `ColorRgb` - `190,103,213`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp) _Main Application_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FU.GAPVqGAqAAAhLD) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Ft%34sSi%31qGAqAAAgeS) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/FCC%36AC%31B-%37%39B%35-%34%39%30%38-%32%33%34C-%34%39%36BA%39D%39%34B%32A) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-19) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5C.config%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%32%26webthought_id%3D%34%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FAJhyi%31qGAqAAAge%35) `.config` - Configuration Sets
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5C.deploy%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%33%26webthought_id%3D%34%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%32yiai%31qGAqAAAgfy) `.deploy` - Deployment Tools
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5C.meteor%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%34%26webthought_id%3D%34%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FFIB%36i%31qGAqAAAggC) `.meteor` - Meteor Config and Build
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cboth%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%35%26webthought_id%3D%34%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FVx%31Gi%31qGAqAAAgga) `both` - Client and Server
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%36%26webthought_id%3D%34%36%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FKoV%32i%31qGAqAAAghW) `client` - Client Main
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5CDesktop.ini)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5CDesktop.ini) `imports`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Ckeys%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%38%26webthought_id%3D%34%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fw%39F%39S%31qGAqAAAg.n)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Ckeys%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%38%26webthought_id%3D%34%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fw%39F%39S%31qGAqAAAg.n) `keys`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Clib%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%39%26webthought_id%3D%34%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FD.uDS%31qGAqAAAg.%36)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Clib%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%39%26webthought_id%3D%34%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FD.uDS%31qGAqAAAg.%36) `lib`
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `node_modules`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-brown-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cpackages%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%30%26webthought_id%3D%35%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FT%39_TS%31qGAqAAAhAE)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cpackages%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%30%26webthought_id%3D%35%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FT%39_TS%31qGAqAAAhAE) `packages`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-brown-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cprivate%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%31%26webthought_id%3D%35%31%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FhoRrS%31qGAqAAAhBP)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cprivate%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%31%26webthought_id%3D%35%31%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FhoRrS%31qGAqAAAhBP) `private`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cpublic%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%32%26webthought_id%3D%35%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FyP%34bS%31qGAqAAAhBl)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cpublic%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%32%26webthought_id%3D%35%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FyP%34bS%31qGAqAAAhBl) `public`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-pink-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cserver%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%33%26webthought_id%3D%35%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FspOnS%31qGAqAAAhCf)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cserver%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%33%26webthought_id%3D%35%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FspOnS%31qGAqAAAhCf) `server`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-orange-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Ctests%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%34%26webthought_id%3D%35%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FmIxXS%31qGAqAAAhDC)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Ctests%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%34%26webthought_id%3D%35%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FmIxXS%31qGAqAAAhDC) `tests`

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5C.babelrc) `.babelrc`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5C.editorconfig) `.editorconfig`
- [![](http://mrobbinsassoc.com/images/icons/md/_eslintrc-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5C.eslintrc) `.eslintrc`
- [![](http://mrobbinsassoc.com/images/icons/md/_gitignore-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5C.gitignore) `.gitignore`
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cpackage.json) `package.json`
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Csettings.json) `settings.json`

