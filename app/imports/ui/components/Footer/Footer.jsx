import FontAwesome from 'react-fontawesome';

const Footer = React.createClass({
  displayName: "Footer",
  propTypes: {},
  mixins: [],
  statics: {
    returnFalse(){
      return false;
    }
  },
  getDefaultProps(){
    return {};
  },
  getInitialState(){
    return {};
  },
  componentWillMount(){
  },
  componentDidMount(){
  },
  componentWillReceiveProps(nextProps){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  },
  shouldComponentUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
    return true;
  },
  componentWillUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
  },
  componentDidUpdate(prevProps, prevState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevState); // !unup - lint hack for webstorm
  },
  componentWillUnmount(){
  },
  render(){
    let ds=new Date().toString();
    return (
      <div id="footer">Footer Last rendered:{ds} <FontAwesome name="rocket" /></div>
    );
  }
});
export default Footer;
