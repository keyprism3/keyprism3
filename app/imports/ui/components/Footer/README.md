## README

`C:\_\keyprism3\app\imports\ui\components\Footer`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-light-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Ccomponents%5CFooter) _Footer Component_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Ccomponents%5CFooter%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Frs%30mS%31qGAqAAAgxu) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%34%36%39%31F%39%30D-%38DC%35-%33FF%39-%33%39DD-A%32EF%35A%36%34A%38%37B) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/ui/components/Footer/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-75) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Ccomponents%5CFooter%5CFooter.jsx) `Footer.jsx` - `#footer`
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Ccomponents%5CFooter%5CFooter.scss) `Footer.scss` - `#footer`

### Folder Built-ins

_none_

### Anticipates

_none_
