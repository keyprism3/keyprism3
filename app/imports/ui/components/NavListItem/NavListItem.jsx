import FlowHelpers from '../../../../client/FlowHelpers';
import FontAwesome from 'react-fontawesome';
import classnames from 'classnames';
import TooltipHelper from '../../../utils/TooltipHelper/TooltipHelper';

const NavListItem = React.createClass({
  displayName: "NavListItem",
  propTypes: {
    fontAwesome: React.PropTypes.string
  },
  mixins: [],
  statics: {
    returnFalse(){
      return false;
    }
  },
  getDefaultProps(){
    return {};
  },
  getInitialState(){
    return {};
  },
  componentWillMount(){
  },
  componentDidMount(){
  },
  componentWillReceiveProps(nextProps){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  },
  shouldComponentUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
    return true;
  },
  componentWillUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
  },
  componentDidUpdate(prevProps, prevState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevState); // !unup - lint hack for webstorm
  },
  componentWillUnmount(){
  },
  getMeteorData(){
  },
  render(){
    // label Home
    // route Home
    // href /
    // fontAwesome home
    const props = this.props;
    //
    const route = props.route?props.route:'';
    const a_href =props.href?props.href:'#';
    const active=FlowHelpers.currentRoute(route);
    //
    let li_class=active;
    li_class=classnames(props.className,li_class,'navListItem');
    const el_fa=props.fontAwesome
      ?<FontAwesome name={props.fontAwesome} className="fontAwesome"/>
      :<FontAwesome name="circle" className="fontAwesome" style={{visibility: 'hidden'}}/>;
    //noinspection CheckTagEmptyBody
    const el_text=props.label?<span className="text">{props.label}</span>:<span className="text"></span>;
    //
    const el_dfa=<div key="icon" className="icon">{el_fa}</div>;
    const el_dtx=<div key="text" className="text">{el_text}</div>;
    const inner1=[el_dfa,el_dtx];
    const el_inner=active==='active'?<a>{inner1}</a>:<a href={a_href}>{inner1}</a>;
    //
    //var key=this.props.key?this.props.key:Random.id();
    const tth=new TooltipHelper(props);// {...tth.itemProps}
    //console.log(tth.itemProps);//
    return (
      <li className={li_class} {...tth.itemProps}>
        {el_inner}
      </li>
    );
  }
});
export default NavListItem;
