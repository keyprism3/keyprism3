/* global ReactDOM */
import FontAwesome from 'react-fontawesome';

const Terms = React.createClass({
  displayName: "Terms",
  propTypes: {
    exclude: React.PropTypes.string // null
    ,iconUrl: React.PropTypes.string
  },
  mixins: [],
  statics: {
    returnFalse(){
      return false;
    }
  },
  getDefaultProps(){
    return {
      exclude: null
      ,iconUrl: null
    };
  },
  getInitialState(){
    return {};
  },
  componentWillMount(){
  },
  componentDidMount(){
    const tabs=ReactDOM.findDOMNode(this.refs.tabs);
    $(tabs).tabs();
  },
  componentWillReceiveProps(nextProps){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  },
  shouldComponentUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
    return true;
  },
  componentWillUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
  },
  componentDidUpdate(prevProps, prevState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevState); // !unup - lint hack for webstorm
  },
  componentWillUnmount(){
  },
  getMeteorData(){
    return {
      //currentUser: Meteor.user()
    };
  },
  render(){
    // onClick="__gaTracker('send', 'event', 'outbound-article', 'http://mrobbinsassoc.com'
    // , 'Mark Robbins and Associates');"
    const company="Mark Robbins and Associates";
    const appname="KeyPrism3";
    const sitename=company+" "+appname;
    const companyurl="http://mrobbinsassoc.com";
    const email="mailto:mark.robbins@mrobbinsassoc.com";
    const appurl="http://mrobbinsassoc.com";
    const address_line1="3831 Trellis View Avenue";
    const address_line2="Las Vegas, Nevada 89115";
    const address_line3="United States";

    const excludes=this.props.exclude?this.props.exclude.split(' '):[];
    //
    const sections={
      preface: {t: "Preface", fa: "legal"}
      ,cookies: {t: "Cookies", fa: "info-circle"}
      ,license: {t: "License", fa: "file-text"}
      ,comments: {t: "Comments", fa: "comments"}
      ,linking: {t: "Linking", fa: "chain"}
      ,iframes: {t: "Iframes", fa: "crop"}
      ,liability: {t: "Liability", fa: "balance-scale"} //
      ,reservation: {t: "Rights", fa: "copyright"}
      ,removal: {t: "Removal", fa: "chain-broken"}
      ,disclaimer: {t: "Disclaimer", fa: "warning"}
      ,credits: {t: "Credits", fa: "globe"}
    };
    const tabitems=[];
    for(let n in sections) {
      //console.log(n);
      if(excludes.indexOf(n)!==-1) {
        continue;
      }
      const sec=sections[n];
      const el_fa=sec.fa
        ?<FontAwesome name={""+sec.fa} className="fontAwesome"/>
        :<FontAwesome name={"circle"} className="fontAwesome" style={{visibility: 'hidden'}}/>;
      //var el_fa=<FontAwesome name={""+sec.fa} className="fontAwesome"/>;
      const el_ico=<div key="icon" className="icon">{el_fa}</div>;
      const el_txt=<div key="text" className="text">{sec.t}</div>;
      //style={{maxWidth:'60px',minWidth:'60px'}}
      tabitems.push(
        <li key={n} className="tab">
          <a href={"#sec-"+n}>
            {el_ico}
            {el_txt}
          </a>
        </li>
      );
      //          <li className="tab"><a href="#sec-cookies">Cookies</a></li>
    }
    //
    const icon=this.props.iconUrl?<img className="headerIcon" src={this.props.iconUrl}/>:null;
    //
    const sec_tabs=(
      <div className="sec-tabs">
        <ul ref="tabs" className="tabs">
          {tabitems}
        </ul>
      </div>
    );
    //
    const sec_address=(
      <div className="sec-address">
        <div className="center-align">
          <h5 className="location">{company} location</h5>
          <address>
            {address_line1}<br />
            {address_line2}<br />
            {address_line3}<br />
          </address>
        </div>
      </div>
    );
    //
    sections.preface.o=(
      <div className="sec sec-preface" id="sec-preface">
        <div className="sec-terms">
          These terms and conditions outline the rules and regulations for the use of {sitename} Website. By accessing
          this website we assume you accept these terms and conditions in full. Do not continue to use {sitename}
          website if you do not accept all of the terms and conditions stated on this page.
        </div>
        {sec_address}
        <p className="fineprint">The following terminology applies to these Terms and Conditions, Privacy Statement and
          Disclaimer Notice and any or all Agreements: "Client", "You" and "Your" refers to you, the person accessing
          this website and accepting the Company&#39;s terms and conditions. "The Company", "Ourselves", "We", "Our" and
          "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves, or either
          the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to
          undertake the process of our assistance to the Client in the most appropriate manner, whether by formal
          meetings of a fixed duration, or any other means, for the express purpose of meeting the Client&#39;s needs in
          respect of provision of the Company&#39;s stated services/products, in accordance with and subject to,
          prevailing law of United States. Any use of the above terminology or other words in the singular, plural,
          capitalisation and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>
      </div>
    );
    //
    sections.cookies.o=(
      <div className="sec sec-cookies" id="sec-cookies">
        <h5>Cookies</h5>
        <p>We employ the use of cookies. By using <a href={companyurl} title={company}>{company}</a> website you consent
          to the use of cookies in accordance with {company} privacy policy.</p>
        <p className="fineprint">Most of the modern day interactive web sites use cookies to enable us to retrieve user
          details for each visit. Cookies are used in some areas of our site to enable the functionality of this area
          and ease of use for those people visiting. Some of our affiliate / advertising partners may also use
          cookies.</p>
      </div>
    );
    //
    sections.license.o=(
      <div className="sec sec-license" id="sec-license">
        <h5>License</h5>
        <p>Unless otherwise stated, {company} and/or it&#39;s licensors own the intellectual property rights for all
          material on {appname}. All intellectual property rights are reserved. You may view and/or print pages
          from {appname} {appurl} for your own personal use subject to restrictions set in these terms and
          conditions.</p>
        <p>You must not:</p>
        <ul>
          <li>Republish material from {appname} {appurl}</li>
          <li>Sell, rent or sub-license material from {appname} {appurl}</li>
          <li>Reproduce, duplicate or copy material from {appname} {appurl}</li>
        </ul>
        <p>Redistribute content from {sitename} (unless content is specifically made for redistribution).</p>
      </div>
    );
    //
    sections.comments.o=(
      <div className="sec sec-comments" id="sec-comments">
        <h5>User Comments</h5>
        <ol>
          <li>This Agreement shall begin on the date hereof.</li>
          <li>Certain parts of this website offer the opportunity for users to post and exchange opinions, information,
            material and data ('Comments') in areas of the website. {company} does not screen, edit, publish or review
            Comments prior to their appearance on the website and Comments do not reflect the views or opinions
            of {company}, its agents or affiliates. Comments reflect the view and opinion of the person who posts such
            view or opinion. To the extent permitted by applicable laws {company} shall not be responsible or liable for
            the Comments or for any loss cost, liability, damages or expenses caused and or suffered as a result of any
            use of and/or posting of and/or appearance of the Comments on this website.
          </li>
          <li>{company} reserves the right to monitor all Comments and to remove any Comments which it considers in its
            absolute discretion to be inappropriate, offensive or otherwise in breach of these Terms and Conditions.
          </li>
          <li>You warrant and represent that:
            <ol>
              <li>You are entitled to post the Comments on our website and have all necessary licenses and consents to
                do so;
              </li>
              <li>The Comments do not infringe any intellectual property right, including without limitation copyright,
                patent or trademark, or other proprietary right of any third party;
              </li>
              <li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful
                material or material which is an invasion of privacy
              </li>
              <li>The Comments will not be used to solicit or promote business or custom or present commercial
                activities or unlawful activity.
              </li>
            </ol>
          </li>
          <li>You hereby grant to <strong>{company}</strong> a non-exclusive royalty-free license to use, reproduce,
            edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or
            media.
          </li>
        </ol>
      </div>
    );
    //
    sections.linking.o=(
      <div className="sec sec-linking" id="sec-linking">
        <h5>Hyperlinking to our Content</h5>
        <ol>
          <li>The following organizations may link to our Web site without prior written approval:
            <ul>
              <li>Government agencies;</li>
              <li>Search engines;</li>
              <li>News organizations;</li>
              <li>Online directory distributors when they list us in the directory may link to our Web site in the same
                manner as they hyperlink to the Web sites of other listed businesses; and
              </li>
              <li>Systemwide Accredited Businesses except soliciting non-profit organizations, charity shopping malls,
                and charity fundraising groups which may not hyperlink to our Web site.
              </li>
            </ul>
          </li>
        </ol>
        <ol start="2">
          <li>These organizations may link to our home page, to publications or to other Web site information so long as
            the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval
            of the linking party and its products or services; and (c) fits within the context of the linking party&#39;
            s site.
          </li>
          <li>We may consider and approve in our sole discretion other link requests from the following types of
            organizations:
            <ul>
              <li>commonly-known consumer and/or business information sources such as Chambers of Commerce, American
                Automobile Association, AARP and Consumers Union;
              </li>
              <li>dot.com community sites;</li>
              <li>associations or other groups representing charities, including charity giving sites,</li>
              <li>online directory distributors;</li>
              <li>internet portals;</li>
              <li>accounting, law and consulting firms whose primary clients are businesses; and</li>
              <li>educational institutions and trade associations.</li>
            </ul>
          </li>
        </ol>
        <p>We will approve link requests from these organizations if we determine that: (a) the link would not reflect
          unfavorably on us or our accredited businesses (for example, trade associations or other organizations
          representing inherently suspect types of business, such as work-at-home opportunities, shall not be allowed to
          link); (b)the organization does not have an unsatisfactory record with us; (c) the benefit to us from the
          visibility associated with the hyperlink outweighs the absence of {company}; and (d) where the link is in the
          context of general resource information or is otherwise consistent with editorial content in a newsletter or
          similar product furthering the mission of the organization.</p>
        <p>These organizations may link to our home page, to publications or to other Web site information so long as
          the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of
          the linking party and it products or services; and (c) fits within the context of the linking party&#39;s
          site.</p>
        <p>If you are among the organizations listed in paragraph 2 above and are interested in linking to our website,
          you must notify us by sending an e-mail to <a href={email} title={"send an email to " + email}>{email}</a>.
          Please include your name, your organization name, contact information (such as a phone number and/or e-mail
          address) as well as the URL of your site, a list of any URLs from which you intend to link to our Web site,
          and a list of the URL(s) on our site to which you would like to link. Allow 2-3 weeks for a response.</p>
        <p>Approved organizations may hyperlink to our Web site as follows:</p>
        <ul>
          <li>By use of our corporate name; or</li>
          <li>By use of the uniform resource locator (Web address) being linked to; or</li>
          <li>By use of any other description of our Web site or material being linked to that makes sense within the
            context and format of content on the linking party&#39;s site.
          </li>
        </ul>
        <p>No use of {company} logo or other artwork will be allowed for linking absent a trademark license
          agreement.</p>
      </div>
    );
    //
    sections.iframes.o=(
      <div className="sec sec-iframes" id="sec-iframes">
        <h5>Iframes</h5>
        <p>Without prior approval and express written permission, you may not create frames around our Web pages or use
          other techniques that alter in any way the visual presentation or appearance of our Web site.</p>
      </div>
    );
    //
    sections.liability.o=(
      <div className="sec sec-liability" id="sec-liability">
        <h5>Content Liability</h5>
        <p>We shall have no responsibility or liability for any content appearing on your Web site. You agree to
          indemnify and defend us against all claims arising out of or based upon your Website. No link(s) may appear on
          any page on your Web site or within any context containing content or materials that may be interpreted as
          libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other
          violation of, any third party rights.</p>
      </div>
    );
    //
    sections.reservation.o=(
      <div className="sec sec-reservation" id="sec-reservation">
        <h5>Reservation of Rights</h5>
        <p>We reserve the right at any time and in its sole discretion to request that you remove all links or any
          particular link to our Web site. You agree to immediately remove all links to our Web site upon such request.
          We also reserve the right to amend these terms and conditions and its linking policy at any time. By
          continuing to link to our Web site, you agree to be bound to and abide by these linking terms and
          conditions.</p>
      </div>
    );
    //
    sections.removal.o=(
      <div className="sec sec-removal" id="sec-removal">
        <h5>Removal of links from our website</h5>
        <p>If you find any link on our Web site or any linked web site objectionable for any reason, you may contact us
          about this. We will consider requests to remove links but will have no obligation to do so or to respond
          directly to you.</p>
        <p>Whilst we endeavour to ensure that the information on this website is correct, we do not warrant its
          completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material
          on the website is kept up to date.</p>
      </div>
    );
    //
    sections.disclaimer.o=(
      <div className="sec sec-disclaimer" id="sec-disclaimer">
        <h5>Disclaimer</h5>
        <p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions
          relating to our website and the use of this website (including, without limitation, any warranties implied by
          law in respect of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill).
          Nothing in this disclaimer will:</p>
        <ol>
          <li>limit or exclude our or your liability for death or personal injury resulting from negligence;</li>
          <li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>
          <li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
          <li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>
        </ol>
        <p>The limitations and exclusions of liability set out in this Section and elsewhere in this disclaimer: (a) are
          subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer or in relation
          to the subject matter of this disclaimer, including liabilities arising in contract, in tort (including
          negligence) and for breach of statutory duty.</p>
        <p>To the extent that the website and the information and services on the website are provided free of charge,
          we will not be liable for any loss or damage of any nature.</p>
      </div>
    );
    //
    const credits_style={color: 'inherit', textDecoration: 'none', cursor: 'text'};
    sections.credits.o=(
      <div className="sec sec-credits" id="sec-credits">
        <h5>Credit & Contact Information</h5>
        <p>This Terms and conditions page was created at
          <a style={credits_style} href="http://termsandconditionstemplate.com">
            termsandconditionstemplate.com
          </a> generator. If you have any queries regarding any of our terms, please contact us.</p>
      </div>
    );
    //
    excludes.forEach(v=>{
      delete sections[v].o;
    });
    return (
      <div className="Terms">

        <h4>{icon} Welcome to {appname} !</h4>
        <p className="byline"><i>by {company}</i></p>

        {sec_tabs}

        {sections.preface.o}

        {sections.cookies.o}

        {sections.license.o}

        {sections.comments.o}

        {sections.linking.o}

        {sections.iframes.o}

        {sections.liability.o}

        {sections.reservation.o}

        {sections.removal.o}

        {sections.disclaimer.o}

        {sections.credits.o}

      </div>
    );
  }
});
export default Terms;
