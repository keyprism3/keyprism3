const LogoutForm = React.createClass({
  displayName: "LogoutForm",
  propTypes: {},
  mixins: [],
  statics: {
    returnFalse(){
      return false;
    }
  },
  getDefaultProps(){
    return {};
  },
  getInitialState(){
    return {};
  },
  componentWillMount(){
  },
  componentDidMount(){
  },
  componentWillReceiveProps(nextProps){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  },
  shouldComponentUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
    return true;
  },
  componentWillUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
  },
  componentDidUpdate(prevProps, prevState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevState); // !unup - lint hack for webstorm
  },
  componentWillUnmount(){
  },
  getMeteorData(){
    return {
      //currentUser: Meteor.user()
    };
  },
  render(){
    return (
      <div className="row LogoutForm">
        <h3>Logout</h3>
        <form className="col l12 offset-l2">
          <div className="row">
            <div className="col l4">
              <button ref="cancel" className="right btn waves-effect waves-light" name="cancel">Cancel</button>
            </div>
            <div className="col l4">
              <button ref="submit" className="right btn waves-effect waves-light" type="submit" name="action">Logout
                <i className="material-icons right">power_settings_new</i>
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
});
export default LogoutForm;
