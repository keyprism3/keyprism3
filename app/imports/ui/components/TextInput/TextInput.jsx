/* global ReactDOM, Random */
import classnames from 'classnames';
import InputError from '../InputError/InputError';
import zxcvbn from 'zxcvbn';
import PropsHelper from '../../../utils/PropsHelper/PropsHelper';
import __ from '../../../utils/__/__';

const TextInput = React.createClass({
  displayName: "TextInput",
  // <type>
  // <mzIconPrefix> string
  // <mzIconSuffix> string
  // uniqueName string
  // <errorMessage> string
  // <emptyMessage> string
  //   <required>
  // <minCharacters>
  // validate function
  // onChange function
  // label string
  propTypes: {
    className: React.PropTypes.string
    ,type: React.PropTypes.string              // "text"
    ,uniqueName: React.PropTypes.string.isRequired
    ,validate: React.PropTypes.func.isRequired
    ,onChange: React.PropTypes.func.isRequired
    //
    ,label: React.PropTypes.string
    ,mzIconPrefix: React.PropTypes.string
    ,mzIconSuffix: React.PropTypes.string
    //
    ,minCharacters: React.PropTypes.number // undefined
    ,errorMessage: React.PropTypes.string  // "Input is invalid"
    ,emptyMessage: React.PropTypes.string  // "Input is invalid, required field"
    //
    ,matchString: React.PropTypes.string
  },
  mixins: [],
  statics: {
    returnFalse(){
      return false;
    }
    ,eventList(){
       return 'Std';
     }
  },
  getDefaultProps(){
    return {
      type: "text"
      ,errorMessage: "Input is invalid"
      ,emptyMessage: "Input is invalid, required field"
      ,matchString: ""
    };
  },
  getInitialState(){
   //most of these variables have to do with handling errors
    return {
      isEmpty: true,
      value: '',
      valid: true,
      errorMessage: this.props.errorMessage,
      emptyMessage: this.props.emptyMessage,
      errorVisible: false,
      hitError: false,
      didBlur: false,
      meterValue: 0
    };
  },
  componentWillMount(){
  },
  componentDidMount(){
  },
  componentWillReceiveProps(nextProps){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  },
  shouldComponentUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
    return true;
  },
  componentWillUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
  },
  componentDidUpdate(prevProps, prevState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevState); // !unup - lint hack for webstorm
  },
  componentWillUnmount(){
  },
  getMeteorData(){
    return {
      //currentUser: Meteor.user()
    };
  },
  updateMeter(value){
    const meter = ReactDOM.findDOMNode(this.refs.meter);
    let result = zxcvbn(value);
    console.log('result',result);
    //noinspection JSUnresolvedVariable
    meter.value=result.score+1;
    //noinspection JSUnresolvedVariable
    console.log('result.score',result.score);
    __.callIfFn(this.props,"onPasswordStrength",result);
    //if (typeof this.props.onPasswordStrength==='function') {
    //  this.props.onPasswordStrength(result);
    // }
  },
  handleChange(event){
    const value=event.target.value;
    //validate the field locally
    if (this.state.didBlur||this.props.matchString!=="") {
      this.validation(event.target.value);
    }
    if (this.props.type==='password') {
      this.updateMeter(value);
    }
    //Call onChange method on the parent component for updating it's state
    //If saving this field for final form submission, it gets passed
    // up to the top component for sending to the server
    if(false&&this.props.onChange) {
      this.props.onChange(event);
    }
  },
  validation(value, valid=true) {
    const props=this.props;
    if (typeof valid === 'undefined') {
      valid = true;
    }
    let hitError=this.state.hitError;
    let message = "";
    let errorVisible = false;
    let any=false;
    //
    const compute=(doHitErr)=>{
      if (!valid) {
        any=true;
        message = props.errorMessage;
      } else if (props.required && jQuery.isEmptyObject(value)) {
        any=true;
        message = props.emptyMessage;
      } else if (value.length < this.props.minCharacters) {
        any=true;
        message = props.errorMessage+", must be at least "+this.props.minCharacters+" characters";
      }
      if (props.matchString!=="") {
        if (props.matchString!==value) {
          any=true;
          message = "Passwords do not match";
        }
      }
      if (any) {
        hitError=doHitErr?true:hitError;
        valid = false;
        errorVisible = true;
      }
    };
    if (!hitError) {
      compute(true);
    }else{
      compute(false);
    }
    //console.log('validation,value:',value,'valid:', valid);
    const update={
      value: value,
      isEmpty: jQuery.isEmptyObject(value),
      valid: valid,
      errorMessage: message,
      errorVisible: errorVisible,
      hitError: hitError
    };
    //console.log('validation,state update:',update);
    this.setState(update);
  },
  handleBlur(event) {
    //console.log('handleBlur',event);
    this.setState({didBlur: true});
    const mz1=ReactDOM.findDOMNode(this.refs.mzIconPrefix);
    if (mz1) {
      $(mz1).removeClass('active');
    }
    //Complete final validation from parent element when complete
    const valid = this.props.validate(event.target.value);
    //pass the result to the local validation element for displaying the error
    this.validation(event.target.value, valid);
  },
  focus(){
    ReactDOM.findDOMNode(this.refs.input).focus();
  },
  reset(){
    // isEmpty: true,
    // value: '',
    // valid: true,
    // errorMessage: this.props.errorMessage,
    // emptyMessage: this.props.emptyMessage,
    // errorVisible: false,
    // hitError:false,
    // didBlur:false
  },
  clear(){
    this.setState({isEmpty: true, value: '', valid: true, errorVisible: false, hitError: false, didBlur: false});
    ReactDOM.findDOMNode(this.refs.input).value='';
    //noinspection JSUnresolvedVariable
    this.refs.input_error.reset();
  },
  hasError(){
    return !!((this.state.errorVisible && this.state.hitError) || !this.state.valid);
  },
  doLog(){
    console.log('doLog');
  },
  render(){
    //
    //console.log('render,props:',this.props,'state',this.state);
    // <type>
    // <mzIconPrefix> string
    // <mzIconSuffix> string
    // uniqueName string
    // <errorMessage> string
    // <emptyMessage> string
    // <required>
    // <minCharacters>
    // validate function
    // onChange function
    // label string
    const props=this.props;
    //console.log(TextInput.eventHooksFromProps(this));
    //console.log('_',_);
    ////
    const mz_icon_pfx=props.mzIconPrefix
      ?<i ref="mzIconPrefix" className="mzIconPrefix material-icons prefix">{props.mzIconPrefix}</i>
      :null;
    //
    const mz_icon_sfx=props.mzIconSuffix
      ?<i ref="mzIconSuffix" className="mzIconSuffix material-icons suffix">{props.mzIconSuffix}</i>
      :null;
    ////
    const input_classes=classnames( //for input
      "input"
      //,"validate"
      ,'input-' + this.props.uniqueName
      ,{invalid: !this.state.valid, valid: (this.state.valid&&this.state.didBlur)}
      ,{invalid: (this.state.errorVisible&&this.state.hitError)}
    );
    const wrapper_classes=classnames(
      "TextInput"
      ,"input-field"
      ,this.props.uniqueName
      ,this.props.className
    );
    //
    const input_type=props.type
      ?props.type
      :"text";
    //
    const rand_id=Random.id();
    //
    const label_for=props.label
      ?<label htmlFor={rand_id}>{props.label}</label>
      :null;
    //
    const is_password=props.type==='password'&&props.uniqueName==='password';
    let meter;
    const meter_style={
      height: '0.4em'
      ,display: 'block'
      ,marginTop: '-1.52em'
      ,position: 'relative'
      ,marginBottom: '1.1em'
    };
    if (props.mzIconPrefix) {
      meter_style.marginLeft='3em';
      meter_style.minWidth='calc(100% - 3em)';
    }else{
      meter_style.minWidth='calc(100% - 0em)';
    }
    if (is_password) {
      // min   low   high   max
      // min={0}
      // max={4}
      // low={2}
      // high={3}
      meter=<meter ref="meter" className="password-strength-meter" style={meter_style} max={5}/>;
    }
    const input_event_hooks=PropsHelper.eventHooksFor(this);
    return (
      <div className={wrapper_classes}>
        {mz_icon_pfx}
        {mz_icon_sfx}
        <input
          ref="input"
          id={rand_id}
          data-id={props.uniqueName}
          tabIndex={props.tabIndex}
          type={input_type}
          placeholder={props.placeholder}
          className={input_classes}
          defaultValue={props.value||''}
          {...input_event_hooks}
        />
        {label_for}
        {meter}
        <InputError
          ref="input_error"
          tag={props.uniqueName}
          hasPrefixIcon={!!mz_icon_pfx}
          visible={this.state.errorVisible&&this.state.hitError}
          errorMessage={this.state.errorMessage}
        />
      </div>
    );
  }
});
export default TextInput;
