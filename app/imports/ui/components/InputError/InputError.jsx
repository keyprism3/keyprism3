import classnames from 'classnames';

const InputError = React.createClass({
  displayName: "InputError",
  propTypes: {
    //tag
    //hasPrefixIcon
    //visible
    //errorMessage
    className: React.PropTypes.string
    ,tag: React.PropTypes.string          // undefined
    ,hasPrefixIcon: React.PropTypes.bool // false
    ,visible: React.PropTypes.bool.isRequired
    ,errorMessage: React.PropTypes.string // "Input is invalid"
  },
  mixins: [],
  statics: {
    returnFalse(){
      return false;
    }
  },
  getDefaultProps(){
    return {
      hasPrefixIcon: false
      ,errorMessage: "Input is invalid"
    };
  },
  getInitialState(){
    return {
      message: 'Input is invalid'
    };
  },
  componentWillMount(){
  },
  componentDidMount(){
  },
  componentWillReceiveProps(nextProps){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  },
  shouldComponentUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
    return true;
  },
  componentWillUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
  },
  componentDidUpdate(prevProps, prevState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevState); // !unup - lint hack for webstorm
  },
  componentWillUnmount(){
  },
  getMeteorData(){
    return {
      //currentUser: Meteor.user()
    };
  },
  prevMsg: null,
  prevVisible: null,
  reset(){
    this.prevVisible=null;
    this.prevMsg=null;
  },
  render(){
    const props=this.props;
    let msg=props.errorMessage;
    const visible=props.visible;
    //
    const bug=false;   // bail Libsymbols green
    //var classnames_visibility={visible:visible, invisible:!visible};
    const classnames_visibility={visible: visible, 'invisible-for-fade-in': !visible};
    //invisible-for-fade-in
    const classnames_fade={'visible-fade-out': !visible,'invisible-fade-in': visible};
    let classnames_use=classnames_visibility;
    //
    let path='';
    //debugger;
    if(this.prevVisible===null) {
      this.prevVisible=visible;
      this.prevMsg=msg;
      path='0';
      // visibility
    }else if(this.prevVisible!==visible) {
      classnames_use=classnames_fade;
      this.prevVisible=visible;
      if(visible) {
        this.prevMsg=msg;
        path='1a';
      }else{
        msg=this.prevMsg;
        path='1b';
      }
    }else if(this.prevVisible===visible) {
      if(visible) {
        msg=props.errorMessage;
        this.prevMsg=msg;
        path='2a';
      }else{
        //nada
        msg=props.errorMessage;
        this.prevMsg=msg;
        path='2b';
        //msg=this.prevMgs;
      }
    }
    const errorClass = classnames(this.props.className, "error_container", classnames_use);
    if(bug) {
      console.log('==========================');
      console.log('path',path);
      console.log('visible',visible);
      console.log(errorClass);
      console.log('msg',msg,'prevMsg',this.prevMsg,'errorMessage',props.errorMessage);
    }
    //
    const div_sty={};
    if(this.props.hasPrefixIcon) {
      div_sty.marginLeft='2.9em';
      div_sty.minWidth='calc(100% - 4.3em)';
    }else{
      div_sty.minWidth='calc(100% - 1.4em)';
    }
    const span_sty={fontSize: 'smaller'};
    //
    return (
      <div className={errorClass} style={div_sty}>
        <span style={span_sty}>{msg}</span>
      </div>
    );
  }
});
export default InputError;
