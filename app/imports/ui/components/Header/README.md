## README

`C:\_\keyprism3\app\imports\ui\components\Header`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-light-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Ccomponents%5CHeader) _Header Component_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/ui/components/Header/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-76) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Ccomponents%5CHeader%5CHeader.jsx) `Header.jsx` - `#header-nav`, `#shooter`
 - `pageTitle`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Ccomponents%5CHeader%5CHeader.scss) `Header.scss` - items:
 - `#header-nav`
 - `#nav-mobile`
 - `#header-nav-logo-link`
 - `header-nav-logo-link-saturater`
 - `#shooter`
 - `shooteranim`

<hr>


### Folder Built-ins

_none_

### Anticipates

_none_
