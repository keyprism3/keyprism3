/* global Meteor */
import NavListItem from '../NavListItem/NavListItem';
import FlowHelpers from '../../../../client/FlowHelpers';

const Header = React.createClass({
  displayName: "Header",
  propTypes: {
    pageTitle: React.PropTypes.string
  },
  mixins: [],
  statics: {
    returnFalse(){
      return false;
    },
    constStrings(){
      return {
        routeLogoutSuccess: 'Home'
      };
    }
  },
  getDefaultProps(){
    return {
      pageTitle: ""
    };
  },
  getInitialState(){
    return {};
  },
  componentWillMount(){
  },
  componentDidMount(){
  },
  componentWillReceiveProps(nextProps){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  },
  shouldComponentUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
    return true;
  },
  componentWillUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
  },
  componentDidUpdate(prevProps, prevState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevState); // !unup - lint hack for webstorm
  },
  componentWillUnmount(){
  },
  getMeteorData(){
    return {
      currentUser: Meteor.user()
    };
  },
  handleLogout() {
    //Meteor.logout();
    //var strings=Header.constStrings();
    //FlowRouter.go(strings.routeLogoutSuccess);
  },
  render(){
    //console.log('NavListItem',NavListItem);
    let navListItems=[];
    navListItems.push(
      <NavListItem key="home"
        label="Home"
        route="Home"
        href="/"
        fontAwesome="home"
        data-tip="Home Page"
        data-place="bottom"
        data-delay-show={200}
        data-effect="solid"
      />
    );
    navListItems.push(
      <NavListItem key="about"
        label="About"
        route="About"
        href="/about"
        fontAwesome="info-circle"
      />
    );
    if (!Meteor.user()) {
      navListItems.push(
        <NavListItem key="register"
          label="Register"
          route="Register"
          href="/register"
          fontAwesome="user-plus"
        />
      );
      navListItems.push(
        <NavListItem key="login"
          label="Login"
          route="Login"
          href="/login"
          fontAwesome="user"
        />
      );
      if (FlowHelpers.currentRoute('Login')==='active'
        ||FlowHelpers.currentRoute('Register')==='active') {
        navListItems.push(
          <NavListItem key="recovery"
            label="Recovery"
            route="Recovery"
            href="/recovery"
            fontAwesome="user-md"
          />
        );
      }
    }else{
      navListItems.push(
        <NavListItem key="logout"
          label="Logout"
          route="Logout"
          href="/logout"
          fontAwesome="user-times"
        />
      );
    }
    return (
      <nav id="header-nav">
        <div className="nav-wrapper">
          <div className="nav-flex">
            <div className="flex-item-left">
              <a href="/" id="header-nav-logo-link" className="brand-logo">&nbsp;</a>
            </div>
            <div className="flex-item-center">
              <h3 className="page-title center-align">{this.props.pageTitle}</h3>
            </div>
            <div className="flex-item-right">
              <ul id="nav-mobile" className="right">
                {navListItems}
              </ul>
            </div>
          </div>
        </div>
        <div id="shooter"></div>
      </nav>
      );
  }
});

export default Header;
