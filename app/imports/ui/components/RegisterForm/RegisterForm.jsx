/* global ReactDOM, Materialize, Accounts, FlowRouter, grecaptcha */
import FontAwesome from 'react-fontawesome';
import TextInput from '../TextInput/TextInput';
import ZxcvbnReport from '../ZxcvbnReport/ZxcvbnReport';
import classnames from 'classnames';
import ReCAPTCHA from 'react-google-recaptcha';
import Gravatar from 'react-gravatar';
import __ from '../../../utils/__/__';
//
const RegisterForm = React.createClass({
  displayName: "RegisterForm",
  propTypes: {},
  mixins: [],
  statics: {
    returnFalse(){
      return false;
    }
  },
  getDefaultProps(){
    return {};
  },
  getInitialState(){
    return {
      first_name: ''
      , last_name: ''
      , email: ''
      , password: ''
      , confirm_password: ''
      , passwordStrength: null
      , email_valid: false
      , with_focus: null
    };
  },
  componentWillMount(){
  },
  componentDidMount(){
    // Get the components DOM node
    const elem = ReactDOM.findDOMNode(this); //this.getDOMNode(); ReactDOM.findDOMNode(this)
    // Set the opacity of the element to 0
    elem.style.opacity = 0;
    setTimeout(()=> {
      window.requestAnimationFrame(()=> {
        // Now set a transition on the opacity
        elem.style.transition = "opacity 2000ms";
        // and set the opacity to 1
        elem.style.opacity = 1;
      });
      this.initialFocus();
    }, 2500);
  },
  componentWillReceiveProps(nextProps){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  },
  shouldComponentUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
    return true;
  },
  componentWillUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
  },
  componentDidUpdate(prevProps, prevState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevState); // !unup - lint hack for webstorm
  },
  componentWillUnmount(){
  },
  getMeteorData(){
    return {
      //currentUser: Meteor.user()
    };
  },
  capchaResult: null,
  initialFocus(){
    this.refs.first_name.focus();
  },
  anyErrors(){
    for (let prop in this.refs) {
      if (!Object.prototype.hasOwnProperty.call(this.refs, prop)) {
        continue;
      }
      const ref = this.refs[prop];
      if (typeof ref.hasError === 'function') {
        const res = ref.hasError();
        if (res) {
          return true;
        }
      }
    }
    return false;
  },
  handleSubmit(event){
    event.preventDefault();
    if (this.anyErrors()) {
      Materialize.toast("Form has errors", 6000);
      return;
    }
    if (!this.capchaResult) {
      Materialize.toast("Click the capcha", 6000);
      return;
    }
    // meteor mongo
    // db.users.remove({})
    //
    const password = this.state.password;
    const confirm_password = this.state.confirm_password;
    if (password === confirm_password && password !== '') {
      const accountInfo = {
        email: this.state.email
        , password: password
        , profile: {
          first_name: this.state.first_name
          , last_name: this.state.last_name
        }
      };
      const doit = false;
      if (doit) {
        Accounts.createUser(accountInfo, (err)=> {
          if (err) {
            Materialize.toast(err.message, 6000);
          } else {
            //TODO: goto dashboard
            console.log('success, goto dashboard');
            FlowRouter.go('/');
          }
        });
      }
      console.log('submit,accountInfo:', accountInfo);
    } else {
      Materialize.toast("Passwords do not match!", 6000);
    }
  },
  emailValidate(value) {
    // regex from http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
    //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))
    // @((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //noinspection JSLint
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // eslint-disable-line max-len
    const rv = re.test(value);
    this.setState({email_valid: rv});
    return rv;
  },
  confirmPasswordValidate(value){
    return value === this.state.password;
  },
  passwordValidate(value) {
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](value); // !unup - lint hack for webstorm
    return true;
  },
  commonValidate(event){
    // do validation of min len etc?
    console.log('commonValidate', event);
    return true;
  },
  handleInputChangeSetState(event){
    const o = {};
    const t = jQuery(event.target);
    //console.log('handleInputChangeSetState,event.target',event.target);
    o[t.attr('data-id')] = event.target.value;
    //console.log('handleInputChangeSetState',o);
    this.setState(o);
    //console.log('handleInputChangeSetState,this.state',this.state);
  },
  doForgot(){
    this.clearForm();
    FlowRouter.go('/recovery');
  },
  doCancel(){
    this.clearForm();
    FlowRouter.go('/');
  },
  clearForm(){
    // generic
    for (let prop in this.refs) {
      if (Object.prototype.hasOwnProperty.call(this.refs, prop)) {
        const ref = this.refs[prop];
        if (typeof ref.clear === 'function') {
          ref.clear();
        }
      }
    }
    //noinspection JSUnresolvedVariable
    grecaptcha.reset();
    this.initialFocus();
  },
  handleButtonClick(event){
    event.preventDefault();
    const n = event.target.name;
    if (n === 'clear') {
      this.clearForm();
    } else if (n === 'cancel') {
      this.doCancel();
    } else if (n === 'recover') {
      this.doForgot();
    }
  },
  handleCaptchaOnChange(value){
    console.log('handleCaptchaOnChange, value:', value);
    this.capchaResult = value;
    // TODO: is this proper for captcha
  },
  handlePasswordStrengthChange(obj){
    this.setState({passwordStrength: obj});
  },
  handleFocus(event){
    const id = __.getId(event.target);
    this.setState({with_focus: id});
  },
  handleBlur(event){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](event); // !unup - lint hack for webstorm
    //const id = __.getId(event.target);
    this.setState({with_focus: null});
  },
  render(){
    const btn_classes = "btn waves-effect waves-light";
    let gravatar;
    if (this.state.email && this.state.email_valid) {
      //mathews.kyle@gmail.com
      gravatar = <Gravatar email={this.state.email} default="monsterid" rating="pg" className="gravatar" size={77}/>;
    }
    const zxcvbn_result = this.state.passwordStrength ? this.state.passwordStrength : {};
    const with_focus = this.state.with_focus;
    const report_show = with_focus === "password" || with_focus === "password"
      ? "report"
      : "terms";
    return (
      <div ref="self" className="row RegisterForm">
        <div className="col l8 offset-l2 m10 offset-m1">
          <div className="row icon-background">
            <FontAwesome name="user-plus" className="fontAwesome background"/>
          </div>
          <div className="row form-row">
            <form className="col l12" onSubmit={this.handleSubmit}>
              <div className="row">
                <TextInput
                  className="col l6"
                  type="text"
                  mzIconPrefix="account_circle"
                  ref="first_name"
                  uniqueName="first_name"
                  label="First Name"
                  minCharacters={2}
                  tabIndex={1}
                  validate={this.commonValidate}
                  onChange={this.handleInputChangeSetState}
                  onFocus={this.handleFocus}
                  onBlur={this.handleBlur}
                  //value="Mark"
                />
                <TextInput
                  className="col l6"
                  type="text"
                  ref="last_name"
                  uniqueName="last_name"
                  label="Last Name"
                  minCharacters={2}
                  tabIndex={2}
                  validate={this.commonValidate}
                  onChange={this.handleInputChangeSetState}
                  onFocus={this.handleFocus}
                  onBlur={this.handleBlur}
                />
              </div>
              <div className="row">
                <TextInput
                  className="col l6"
                  type="email"
                  mzIconPrefix="email"
                  ref="email"
                  uniqueName="email"
                  label="Email"
                  minCharacters={4}
                  tabIndex={3}
                  validate={this.emailValidate}
                  onChange={this.handleInputChangeSetState}
                  onFocus={this.handleFocus}
                  onBlur={this.handleBlur}
                />
                <div className="col l6">
                  {gravatar}
                  <ReCAPTCHA
                    className="capcha"
                    ref="recaptcha"
                    sitekey="6LeHzAoUAAAAAPKDAb756LedBohb0M-ArS_Zb0eG"
                    theme="dark"
                    onChange={this.handleCaptchaOnChange}
                  />
                </div>
              </div>
              <div className="row">
                <TextInput
                  className="col l6"
                  type="password"
                  mzIconPrefix="vpn_key"
                  ref="password"
                  uniqueName="password"
                  label="Password"
                  minCharacters={6}
                  tabIndex={4}
                  validate={this.passwordValidate}
                  onChange={this.handleInputChangeSetState}
                  onPasswordStrength={this.handlePasswordStrengthChange}
                  onFocus={this.handleFocus}
                  onBlur={this.handleBlur}
                />
                <TextInput
                  className="col l6"
                  type="password"
                  ref="confirm_password"
                  uniqueName="confirm_password"
                  label="Confirm Password"
                  tabIndex={5}
                  validate={this.confirmPasswordValidate}
                  onChange={this.handleInputChangeSetState}
                  matchString={this.state.password}
                  onFocus={this.handleFocus}
                  onBlur={this.handleBlur}
                />
              </div>
              <div className="row button-row">
                <div className="col l3">
                  <button ref="clear" name="clear" tabIndex={9}
                          className={btn_classes}
                          onClick={this.handleButtonClick}
                          onFocus={this.handleFocus}
                          onBlur={this.handleBlur}
                  >Clear
                  </button>
                </div>
                <div className="col l3">
                  <button ref="cancel" name="cancel" tabIndex={8}
                          className={classnames("right", btn_classes)}
                          onClick={this.handleButtonClick}
                          onFocus={this.handleFocus}
                          onBlur={this.handleBlur}
                  >Cancel
                  </button>
                </div>
                <div className="col l3">
                  <button ref="recover" name="recover" tabIndex={7}
                          className={btn_classes}
                          onClick={this.handleButtonClick}
                          onFocus={this.handleFocus}
                          onBlur={this.handleBlur}
                  >Forgot
                  </button>
                </div>
                <div className="col l3">
                  <button ref="submit" name="submit" type="submit" tabIndex={6}
                          className={classnames("right", btn_classes)}
                          onFocus={this.handleFocus}
                          onBlur={this.handleBlur}
                  >Register
                    <i className="material-icons right">send</i>
                  </button>
                </div>
              </div>
              <div className="row zxcvbn-row">
                <ZxcvbnReport className="col l12" zxcvbnResult={zxcvbn_result} show={report_show}/>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
});
export default RegisterForm;
