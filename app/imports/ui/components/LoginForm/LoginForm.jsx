const LoginForm = React.createClass({
  displayName: "LoginForm",
  propTypes: {},
  mixins: [],
  statics: {
    returnFalse(){
      return false;
    }
  },
  getDefaultProps(){
    return {};
  },
  getInitialState(){
    return {};
  },
  componentWillMount(){
  },
  componentDidMount(){
  },
  componentWillReceiveProps(nextProps){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  },
  shouldComponentUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
    return true;
  },
  componentWillUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
  },
  componentDidUpdate(prevProps, prevState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevState); // !unup - lint hack for webstorm
  },
  componentWillUnmount(){
  },
  getMeteorData(){
    return {
      //currentUser: Meteor.user()
    };
  },
  render(){
    return (
      <div className="row LoginForm">
        <h3>Login</h3>
        <form className="col l12 offset-l2">
          <div className="row">
            <div className="input-field col l4">
              <i className="material-icons prefix">email</i>
              <input id="email" type="email" className="validate"/>
              <label htmlFor="email">Email</label>
            </div>
            <div className="input-field col l4">
              <i className="material-icons prefix">vpn_key</i>
              <input id="password" type="password" className="validate"/>
              <label htmlFor="password">Password</label>
            </div>
          </div>
          <div className="row">
            <div className="col l2">
              <button ref="clear" className="btn waves-effect waves-light" name="clear">Clear</button>
            </div>
            <div className="col l2">
              <button ref="cancel" className="right btn waves-effect waves-light" name="cancel">Cancel</button>
            </div>
            <div className="col l2">
              <button ref="recover" className="btn waves-effect waves-light" name="recover">Forgot</button>
            </div>
            <div className="col l2">
              <button ref="submit" className="right btn waves-effect waves-light" type="submit" name="action">Login
                <i className="material-icons right">send</i>
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
});
export default LoginForm;
