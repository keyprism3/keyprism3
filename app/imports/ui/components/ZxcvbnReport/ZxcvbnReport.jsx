import classnames from 'classnames';
import Terms from '../Terms/Terms';
//
const ZxcvbnReport = React.createClass({
  displayName: "ZxcvbnReport",
  propTypes: {
    className: React.PropTypes.string
    , zxcvbnResult: React.PropTypes.object.isRequired
    , show: React.PropTypes.string // report or terms
  },
  mixins: [],
  statics: {
    returnFalse(){
      return false;
    }
  },
  getDefaultProps(){
    return {};
  },
  getInitialState(){
    return {};
  },
  componentWillMount(){
  },
  componentDidMount(){
  },
  componentWillReceiveProps(nextProps){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  },
  shouldComponentUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
    return true;
  },
  componentWillUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
  },
  componentDidUpdate(prevProps, prevState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevState); // !unup - lint hack for webstorm
  },
  componentWillUnmount(){
  },
  getMeteorData(){
    return {
      //currentUser: Meteor.user()
    };
  },
  render(){
    const props = this.props;
    const data = this.props.zxcvbnResult;
    const classes = classnames("ZxcvbnReport", this.props.className);
    if (Object.keys(data).length === 0 || props.show === "terms") {
      return (
        <div className={classes}>
          <Terms exclude="iframes" iconUrl="images/icon/favicon-32x32.png"/>
        </div>
      );
    }
    const h5 = data.feedback.warning || "Try to reach a Score of 4";
    const suggestions = data.feedback.suggestions.map(i=> {
      return <li key={i} className="suggestion">{i}</li>;
    });
    const ratelimit="Rate limiting is used to control the rate of traffic"
      +" sent or received by a network interface controller";
    return (
      <div className={classes}>
        <div className="score-guesses">
          <h4 className="header">Password Analysis</h4>
          <div className="div-calculated">
            <div className="calculated">
              <small>Calculated in {data.calc_time}ms</small>
            </div>
          </div>
          <h5 className="warning">{h5}</h5>
          <div className="score">
            <span className="score-title">Score: </span>
            <span className={"score-score score-"+data.score}>{data.score}</span> of <span className="of-score">4</span>
          </div>
          <div className="guesses">
            It would take <span className="number">{data.guesses.toFixed(4)}</span> guesses
            to crack your password at <span className="number">{data.guesses_log10.toFixed(4)}</span> magnitude
          </div>
        </div>
        <div className="div-cracks">
          <h5 className="online-attack">Online Attack</h5>
          <div className="div-online-attack">
            <div className="cracked-in">
              Your password would be cracked in ...
            </div>
            <div className="throttling">
              <span className="number">{data.crack_times_display.online_throttling_100_per_hour} </span>
                on a service that
                <a href="#" className="ratelimit"
                   data-tip={ratelimit} data-effect="solid" data-type="info" data-place="bottom"
                > rate limits </a>
                password authorization attempts
            </div>
            <div className="nothrottling">
              <span className="number">{data.crack_times_display.online_no_throttling_10_per_second} </span>
                on a service that does not
                <a href="#" className="ratelimit"
                   data-tip={ratelimit} data-effect="solid" data-type="info" data-place="bottom"
                > rate limit</a>
                , or where an attacker has outsmarted ratelimiting
            </div>
          </div>
        </div>
        <div className="div-suggestions">
          <h5 className="header">Suggestions</h5>
          <div className="div-suggestions-suggestions">
            <ul className="suggestions">
              {suggestions}
            </ul>
          </div>
        </div>
      </div>
    );
  }
});
export default ZxcvbnReport;
