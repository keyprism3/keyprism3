import ReactTooltip from 'react-tooltip';

const MainLayout = React.createClass({
  displayName: "MainLayout",
  propTypes: {
    page: React.PropTypes.string
    ,header: React.PropTypes.element
    ,footer: React.PropTypes.element
    ,content: React.PropTypes.element

  },
  mixins: [],
  statics: {
    returnFalse(){
      return false;
    }
  },
  getDefaultProps(){
    return {};
  },
  getInitialState(){
    return {};
  },
  componentWillMount(){
  },
  componentDidMount(){
  },
  componentWillReceiveProps(nextProps){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  },
  shouldComponentUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
    return true;
  },
  componentWillUpdate(nextProps, nextState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextState); // !unup - lint hack for webstorm
  },
  componentDidUpdate(prevProps, prevState){
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevState); // !unup - lint hack for webstorm
  },
  componentWillUnmount(){
  },
  render(){
    let cls=["app-root"];
    if(this.props.page) {
      cls.push(this.props.page+'-Page');
    }
    cls=cls.join(" ");
    return (
      <div id="mainlayout" className={cls}>
        <ReactTooltip/>
        <header id="mainheader">
          {this.props.header}
        </header>
        <div id="content" className="x-container">
          {this.props.content}
        </div>
        <footer id="mainfooter">
          {this.props.footer}
        </footer>
      </div>
    );
  }
});
export default MainLayout;
