## README

`C:\_\keyprism3\app\imports\ui\aastyleimports`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-pink-light-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Caastyleimports) _Sass Style Imports_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/ui/aastyleimports/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-68) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Caastyleimports%5Cfontface.scss) `fontface.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Caastyleimports%5Chelpers.scss) `helpers.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Caastyleimports%5Ctext.scss) `text.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Caastyleimports%5Ctypography.scss) `typography.scss`
- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cui%5Caastyleimports%5Cvariables.scss) `variables.scss`

### Folder Built-ins

_none_

### Anticipates

_none_
