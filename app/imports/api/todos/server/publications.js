/* eslint-disable prefer-arrow-callback */
/**
 * <h4>`Todos` Publications</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Ctodos%5Cserver%5Cpublications%2Ejs">C:\_\socialnitro-app\imports\api\todos\server\publications.js</a><br>
 * - `todos.inList` - {@link Lists}, {@link Todos}, {@link Todos_publish_inList_validator}
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor}        - `meteor/meteor`
 * - {@link SimpleSchema}  - `meteor/aldeed:simple-schema`
 * <p>
 * <h5>NPM Dependencies</h5>
 * none
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}                        - `'/both/ctrl/ctrl'`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Lists}                       - `'../lists/lists.js'`
 * - {@link Todos}                       - `'../todos.js'`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/todos/server/publications.js
 */
const todos_server_publications_js = '/imports/api/todos/server/publications.js';

import { Meteor } from 'meteor/meteor';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

import Ctrl from '/both/ctrl/ctrl';

import { Todos } from '../todos.js';
import { Lists } from '../../lists/lists.js';

Ctrl.logFileLoaded();
Ctrl.unused(todos_server_publications_js);

/**
 * <h4>`inList` schema</h4>
 * - listId - `{ type: String }`
 * <p>
 * @protected
 */
const Todos_publish_inList_schema = {
  listId: { type: String }
};
/**
 * <h4>`inList` validator</h4>
 * `new SimpleSchema(Todos_publish_inList_schema)`
 * <p>
 * {@link Todos_publish_inList_schema} <br>
 * @protected
 */
const Todos_publish_inList_validator =  new SimpleSchema(Todos_publish_inList_schema);

/**
 * <h4>`todos.inList` Composite Publication</h4>
 * <p>
 * {@link Lists} <br>
 * {@link Todos} <br>
 * {@link Todos_publish_inList_validator} <br>
 * @protected
 * @param {{listId: String}} params
 * @return {Object}
 */
const Todos_inList_publication = function todosInList(params) {
  Todos_publish_inList_validator.validate(params);
  const { listId } = params;
  const userId = this.userId;
  return {
    find() {
      const query = {
        _id: listId,
        $or: [{ userId: { $exists: false }}, { userId }],
      };
      // We only need the _id field in this query, since it's only
      // used to drive the child queries to get the todos
      const options = {
        fields: { _id: 1 },
      };
      return Lists.find(query, options);
    },
    children: [{
      find(list) {
        return Todos.find({ listId: list._id }, { fields: Todos.publicFields });
      },
    }],
  };
};
Meteor.publishComposite('todos.inList', Todos_inList_publication);
