## RMF-app-imports-api-todos

`C:\_\keyprism3\app\imports\api\todos`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Ctodos) _Todos api_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/api/todos/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-112) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/api/todos/todos.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_api_todos_todos_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Ctodos%5CincompleteCountDenormalizer.js) `incompleteCountDenormalizer.js` - export static singleton, Update `Lists`, methods:
 - `afterInsertTodo(todo)`
 - `afterUpdateTodo(selector, modifier)`
 - `afterRemoveTodos(todos)`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Ctodos%5Cmethods.js) `methods.js` - `Todos` methods
 - `todos.insert({listId, text})`
 - `todos.setCheckedStatus({todoId, newCheckedStatus})`
 - `todos.updateText({todoId, newText})`
 - `todos.remove({todoId})`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Ctodos%5Ctodos.js) `todos.js` - `Todos` extended Mongo Collection `todos`
 - `Todos.insert(doc, callback)`
 - `Todos.update(selector, modifier)`
 - `Todos.remove(selector)`
 - `Todos.schema`
 - `Todos.publicFields`
 - Factory: `todo`
 - Helpers: `list()`, `editableBy(iserId)`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Ctodos%5Ctodos.tests.js) `todos.tests.js` - method tests
 - `todos : mutators : builds correctly from factory`
 - `todos : leaves createdAt on update`
 - `todos : publications : todos.inList : sends all todos for a public list`
 - `todos : publications : todos.inList : sends all todos for a public list when logged in`
 - `todos : publications : todos.inList : sends all todos for a private list when logged in as owner`
 - `todos : publications : todos.inList : sends no todos for a private list when not logged in`
 - `todos : publications : todos.inList : sends no todos for a private list when logged in as another user`

<hr>


### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-light-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Ctodos%5Cserver) `server` - Todos Publications

### Anticipates

_none_
