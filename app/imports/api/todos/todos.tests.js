/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */
/**
 * <h4>`Todos` Server Tests</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Ctodos%5Ctodos%2Etests%2Ejs">C:\_\socialnitro-app\imports\api\todos\todos.tests.js</a><br>
 * - `todos : mutators : builds correctly from factory`
 * - `todos : leaves createdAt on update`
 * - `todos : publications : todos.inList : sends all todos for a public list`
 * - `todos : publications : todos.inList : sends all todos for a public list when logged in`
 * - `todos : publications : todos.inList : sends all todos for a private list when logged in as owner`
 * - `todos : publications : todos.inList : sends no todos for a private list when not logged in`
 * - `todos : publications : todos.inList : sends no todos for a private list when logged in as another user`
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor}               - `meteor/meteor`
 * - {@link Random}               - `meteor/random`
 * - {@link _}                    - `meteor/underscore`
 * - {@link Factory}              - `meteor/dburles:factory`
 * - {@link PublicationCollector} - `meteor/johanbrook:publication-collector`
 * - {@link chai}                 - `meteor/practicalmeteor:chai`
 * - {@link assert}               - `meteor/practicalmeteor:chai`
 * <h5>NPM Dependencies</h5>
 * none
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}                        - `'/both/ctrl/ctrl'`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Todos}                       - `'../todos/todos.js'`
 * - {@link socialnitro-app/imports/api/todos/server/publications.js~todos_server_publications_js}      - `'./server/publications.js'`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/todos/todos.tests.js
 */
const todos_tests_js = '/imports/api/todos/todos.tests.js';

import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { _ } from 'meteor/underscore';
import { Factory } from 'meteor/dburles:factory';
import { PublicationCollector } from 'meteor/johanbrook:publication-collector';

import { chai, assert } from 'meteor/practicalmeteor:chai';

import Ctrl from '/both/ctrl/ctrl';

import { Todos } from './todos.js';

Ctrl.logFileLoaded();
Ctrl.unused(todos_tests_js);

/**
 * <h4>`Todos` Server Tests</h4>
 * - `todos : mutators : builds correctly from factory`
 * - `todos : leaves createdAt on update`
 * - `todos : publications : todos.inList : sends all todos for a public list`
 * - `todos : publications : todos.inList : sends all todos for a public list when logged in`
 * - `todos : publications : todos.inList : sends all todos for a private list when logged in as owner`
 * - `todos : publications : todos.inList : sends no todos for a private list when not logged in`
 * - `todos : publications : todos.inList : sends no todos for a private list when logged in as another user`
 * <p>
 * @protected
 */
const Todos_server_tests = Meteor.isServer;

if (Todos_server_tests) {
  require('./server/publications.js');
  /**
   * @test {Todos}
   * @test {Todos_factory}
   * @test {TodosCollection#update}
   */
  describe('todos', function () {
    /**
     * @test {Todos}
     * @test {Todos_factory}
     * @test {TodosCollection#update}
     */
    describe('mutators', function () {
      /**
       * @test {Todos_factory}
       */
      it('builds correctly from factory', function (done) {
        const todo = Factory.create('todo');
        assert.typeOf(todo, 'object');
        assert.typeOf(todo.createdAt, 'date');
        done();
      });
    });
    /**
     * @test {Todos}
     * @test {Todos_factory}
     * @test {TodosCollection#update}
     */
    it('leaves createdAt on update', function (done) {
      //noinspection MagicNumberJS
      const createdAt = new Date(new Date() - 1000);
      let todo = Factory.create('todo', { createdAt });
      //
      const text = 'some new text';
      Todos.update(todo, { $set: { text }});
      //
      todo = Todos.findOne(todo._id);
      assert.equal(todo.text, text);
      assert.equal(todo.createdAt.getTime(), createdAt.getTime());
      done();
    });
    /**
     * @test {Todos_inList_publication}
     */
    describe('publications', function () {
      let publicList;
      let privateList;
      let userId;
      //
      before(function (done) {
        userId = Random.id();
        publicList = Factory.create('list');
        privateList = Factory.create('list', { userId });
        //
        _.times(3, () => {
          Factory.create('todo', { listId: publicList._id });
          // TODO get rid of userId, https://github.com/meteor/todos/pull/49
          Factory.create('todo', { listId: privateList._id, userId });
        });
        done();
      });
      /**
       * @test {Todos_inList_publication}
       */
      describe('todos.inList', function () {
        /**
         * @test {Todos_inList_publication}
         */
        it('sends all todos for a public list', function (done) {
          const collector = new PublicationCollector();
          collector.collect(
            'todos.inList',
            { listId: publicList._id },
            (collections) => {
              chai.assert.equal(collections.todos.length, 3);
            }
          );
          done();
        });
        /**
         * @test {Todos_inList_publication}
         */
        it('sends all todos for a public list when logged in', function (done) {
          const collector = new PublicationCollector({ userId });
          collector.collect(
            'todos.inList',
            { listId: publicList._id },
            (collections) => {
              chai.assert.equal(collections.todos.length, 3);
            }
          );
          done();
        });
        /**
         * @test {Todos_inList_publication}
         */
        it('sends all todos for a private list when logged in as owner', function (done) {
          const collector = new PublicationCollector({ userId });
          collector.collect(
            'todos.inList',
            { listId: privateList._id },
            (collections) => {
              chai.assert.equal(collections.todos.length, 3);
            }
          );
          done();
        });
        /**
         * @test {Todos_inList_publication}
         */
        it('sends no todos for a private list when not logged in', function (done) {
          const collector = new PublicationCollector();
          collector.collect(
            'todos.inList',
            { listId: privateList._id },
            (collections) => {
              chai.assert.isUndefined(collections.todos);
            }
          );
          done();
        });
        /**
         * @test {Todos_inList_publication}
         */
        it('sends no todos for a private list when logged in as another user', function (done) {
          const collector = new PublicationCollector({ userId: Random.id() });
          collector.collect(
            'todos.inList',
            { listId: privateList._id },
            (collections) => {
              chai.assert.isUndefined(collections.todos);
            }
          );
          done();
        });
      });
    });
  });
}
