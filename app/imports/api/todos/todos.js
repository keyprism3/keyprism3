/**
 * <h4>`Todos` Collection</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Ctodos%5Ctodos%2Ejs">C:\_\socialnitro-app\imports\api\todos\todos.js</a><br>
 * - `TodosCollection`    - Mongo Collection
 * - `Todos` export       - `new TodosCollection('todos')`
 * - `Todos_deny`         - `Todos` Access
 * - `Todos_schema`       - `Todos` Schema
 * - `Todos_publicFields` - `Todos` Public Fields
 * - `Todos_factory`      - `Todos` Factory
 * - `Todos_helpers`      - `Todos` Helpers
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Mongo}        - `meteor/mongo`
 * - {@link Factory}      - `meteor/dburles:factory`
 * - {@link SimpleSchema} - `meteor/aldeed:simple-schema`
 * <p>
 * <h5>NPM Dependencies</h5>
 * - {@link faker}        - `^3.0.1`
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}                        - `'/both/ctrl/ctrl'`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Lists}                       - `'../lists/lists.js'`
 * - {@link incompleteCountDenormalizer} - `'./incompleteCountDenormalizer.js'`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/todos/todos.js
 */
const todos_js = '/imports/api/todos/todos.js';

import { Mongo } from 'meteor/mongo';
import { Factory } from 'meteor/dburles:factory';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

import faker from 'faker';

import Ctrl from '/both/ctrl/ctrl';

import { Lists } from '../lists/lists.js';
import incompleteCountDenormalizer from './incompleteCountDenormalizer.js';

Ctrl.logFileLoaded();
Ctrl.unused(todos_js);

/**
 * <h4>Mongo Collection</h4>
 * <p>
 * - `insert`
 * - `update`
 * - `remove`
 * @protected
 */
class TodosCollection extends Mongo.Collection {
  /**
   * <h4>Insert todo item</h4>
   * and update incomplete counts
   * <p>
   * {@link incompleteCountDenormalizer} <br>
   * @override
   * @public
   * @param {Object} doc - todo
   * @param {function} callback
   * @return {WriteResult}
   */
  insert(doc, callback) {
    const ourDoc = doc;
    ourDoc.createdAt = ourDoc.createdAt || new Date();
    const result = super.insert(ourDoc, callback);
    incompleteCountDenormalizer.afterInsertTodo(ourDoc);
    return result;
  }
  /**
   * <h4>Update todo item</h4>
   * and update incomplete counts
   * <p>
   * {@link incompleteCountDenormalizer} <br>
   * @override
   * @public
   * @param {Object} selector
   * @param {Object} modifier
   * @return {WriteResult}
   */
  update(selector, modifier) {
    const result = super.update(selector, modifier);
    incompleteCountDenormalizer.afterUpdateTodo(selector, modifier);
    return result;
  }
  /**
   * <h4>Remove todo item</h4>
   * and update incomplete counts
   * <p>
   * {@link incompleteCountDenormalizer} <br>
   * @override
   * @public
   * @param {Object} selector
   * @return {WriteResult}
   */
  remove(selector) {
    const todos = this.find(selector).fetch();
    const result = super.remove(selector);
    incompleteCountDenormalizer.afterRemoveTodos(todos);
    return result;
  }
}
/**
 * <h4>`Todos` __export__</h4>
 * new {@link TodosCollection} `'todos'`
 * <p>
 * - `insert`
 * - `update`
 * - `remove`
 * @public
 */
export const Todos = new TodosCollection('todos');

//////

/**
 * <h4>`Todos` Access</h4>
 * Deny all client-side updates since we will be using methods to manage this collection
 * <p>
 * - `insert` true
 * - `update` true
 * - `true` true
 * <p>
 * @protected
 */
class Todos_deny {
  /**
   * `true`
   * @protected
   */
  static insert() { return true; }
  /**
   * `true`
   * @protected
   */
  static update() { return true; }
  /**
   * `true`
   * @protected
   */
  static remove() { return true; }
}
Todos.deny(Todos_deny);

/**
 * <h4>`Todos` Schema</h4>
 * @protected
 */
class Todos_schema {
  /**
   * `type: String, regEx: SimpleSchema.RegEx.Id`
   * @protected
   */
  _id = { type: String, regEx: SimpleSchema.RegEx.Id };
  /**
   * `type: String, regEx: SimpleSchema.RegEx.Id, denyUpdate: true`
   * @protected
   */
  listId = { type: String, regEx: SimpleSchema.RegEx.Id, denyUpdate: true };
  //noinspection MagicNumberJS
  /**
   * `type: String, max: 100, optional: true`
   * @protected
   */
  text = { type: String, max: 100, optional: true };
  /**
   * `type: Date, denyUpdate: true`
   * @protected
   */
  createdAt = { type: Date, denyUpdate: true };
  /**
   * `type: Boolean, defaultValue: false`
   * @protected
   */
  checked = { type: Boolean, defaultValue: false };
}
//noinspection JSCheckFunctionSignatures
Todos_schema.attachSchemaToCollection(Todos);

/**
 * <h4>`Todos` Public Fields</h4>
 * This represents the keys from Lists objects that should be published
 * to the client. If we add secret properties to List objects, don't list
 * them here to keep them private to the server.
 * <p>
 * - `listId`
 * - `text`
 * - `createdAt`
 * - `checked`
 * @protected
 */
const Todos_publicFields = {
  listId: 1,
  text: 1,
  createdAt: 1,
  checked: 1,
};
Todos.publicFields = Todos_publicFields;

/**
 * <h4>`Todos` Factory</h4>
 * `'todo'`
 * <p>
 * - `listId`
 * - `text`
 * - `createdAt`
 * <p>
 * @protected
 */
class Todos_factory {
  /**
   * <h4>List Id</h4>
   * from 'list' factory
   * <p>
   * @public
   */
  static listId() {
    return Factory.get('list');
  }
  /**
   * <h4>Lorem text</h4>
   * from `faker.lorem.sentence`
   * <p>
   * @public
   */
  static text() {
    return faker.lorem.sentence();
  }
  /**
   * <h4>Creation Date</h4>
   * from `new Date`
   * <p>
   * @public
   */
  static createdAt() {
    return new Date();
  }
}
Todos_factory.definesFactoryForCollection('todo', Todos);

/**
 * <h4>`Todos` Helpers</h4>
 * - `list` - parent list
 * - `editableBy`
 * @protected
 */
class Todos_helpers {
  /**
   * <h4>Parent List Item</h4>
   * <p>
   * @protected
   * @return {document} `Lists` item
   */
  static list() {
    return Lists.findOne(this.listId);
  }
  /**
   * <h4>Can Edit Todo Item</h4>
   * Check List to see if `editableBy`
   * <p>
   * @protected
   * @param {string} userId
   * @return {boolean}
   */
  static editableBy(userId) {
    return this.list().editableBy(userId);
  }
}
//noinspection JSCheckFunctionSignatures
Todos_helpers.attachHelpersToCollection(Todos);
