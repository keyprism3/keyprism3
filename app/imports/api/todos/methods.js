/**
 * <h4>`Todos` Methods</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Ctodos%5Cmethods%2Ejs">C:\_\socialnitro-app\imports\api\todos\methods.js</a><br>
 * - `insert` -
 * - `setCheckedStatus` -
 * - `updateText` -
 * - `remove` -
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor}          - `meteor/meteor`
 * - {@link DDPRateLimiter}  - `meteor/ddp-rate-limiter`
 * - {@link _}               - `meteor/underscore`
 * <p>
 * <h5>NPM Dependencies</h5>
 * none
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}                      - `'/both/ctrl/ctrl'`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Lists}                       - `'./lists/lists.js'`
 * - {@link Todos}                       - `'./todos.js'`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/todos/methods.js
 */
const todos_methods_js='/imports/api/todos/methods.js';

import { Meteor } from 'meteor/meteor';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';

import Ctrl from '/both/ctrl/ctrl';

import { Lists } from '../lists/lists.js';
import { Todos } from './todos.js';

Ctrl.logFileLoaded();
Ctrl.unused(todos_methods_js);

/**
 * <h4>helper</h4>
 * `{ clean: true, filter: false }`
 * @protected
 */
const validate_clean_nofilter = { clean: true, filter: false };

/**
 * <h4>`insert` validator</h4>
 * `Todos.simpleSchema().pick(['listId', 'text']).validator(validate_clean_nofilter)`
 * <p>
 * {@link Todos} <br>
 * @protected
 */
const Todos_insert_validator = Todos.simpleSchema().pick(['listId', 'text']).validator(validate_clean_nofilter);
/**
 * <h4>`insert` method</h4>
 * `'todos.insert'`
 * <p>
 * {@link Todos} <br>
 * {@link Todos_insert_validator}
 * @protected
 */
class Todos_insert {
  /**
   * <h4>`'todos.insert'` </h4>
   * <p>
   * @protected
   */
  name = 'todos.insert';
  /**
   * <h4>validator</h4>
   * <p>
   * {@link Todos_insert_validator} <br>
   * @protected
   */
  validate = Todos_insert_validator;
  /**
   * <h4>`run({ listId, text })`</h4>
   * <p>
   * {@link Todos} <br>
   * {@link Lists} <br>
   * @protected
   * @param {{listId: string, text: string}} parmo
   * @return {WriteResult}
   * @throws {Meteor.Error} - `'todos.insert.accessDenied'`
   */
  run(parmo) {
    const { listId, text } = parmo;
    const list = Lists.findOne(listId);
    if (list.isPrivate() && list.userId !== this.userId) {
      throw new Meteor.Error('todos.insert.accessDenied',
        'Cannot add todos to a private list that is not yours');
    }
    const todo = {
      listId,
      text,
      checked: false,
      createdAt: new Date(),
    };
    return Todos.insert(todo);
  }
}
/**
 * <h4>`insert` export</h4>
 * `'todos.insert'`
 * <p>
 * {@link Todos} <br>
 * {@link Lists} <br>
 * @public
 * @param {{listId: string, text: string}} parmo
 * @return {WriteResult}
 */
export const insert = Todos_insert.asMethod;

/**
 * <h4>`setCheckedStatus` schema</h4>
 * - todoId - `Todos.simpleSchema().schema('_id')`
 * - newCheckedStatus - `Todos.simpleSchema().schema('checked')`
 * <p>
 * {@link Todos} <br>
 * @protected
 */
class Todos_setCheckedStatus_schema {
  todoId =  Todos.simpleSchema().schema('_id');
  newCheckedStatus = Todos.simpleSchema().schema('checked');
}
/**
 * <h4>`setCheckedStatus` validator</h4>
 * `Todos_setCheckedStatus_schema.schemaValidator()`
 * <p>
 * {@link Todos_setCheckedStatus_schema} <br>
 * {@link validate_clean_nofilter} <br>
 * @protected
 */
const Todos_setCheckedStatus_validator = Todos_setCheckedStatus_schema.schemaValidator();
/**
 * <h4>`setCheckedStatus` method</h4>
 * `'todos.mackeCheckedStatus'`
 * <p>
 * {@link Todos} <br>
 * {@link Todos_setCheckedStatus_validator} <br>
 * @protected
 */
class Todos_setCheckedStatus {
  /**
   * <h4>`'todos.setCheckedStatus'` </h4>
   * <p>
   * @protected
   */
  name = 'todos.setCheckedStatus';
  /**
   * <h4>validator</h4>
   * <p>
   * {@link Todos_setCheckedStatus_validator} <br>
   * @protected
   */
  validate = Todos_setCheckedStatus_validator;
  /**
   * <h4>`run({ todoId, newCheckedStatus })` </h4>
   * <p>
   * {@link Todos} <br>
   * {@link Todos_helpers.editableBy} <br>
   * @protected
   * @param {{ todoId: string, newCheckedStatus: boolean }} parmo
   * @return {(WriteResult|undefined)}
   * @throws {Meteor.Error} - `'todos.setCheckedStatus.accessDenied'`
   */
  run(parmo) {
    const { todoId, newCheckedStatus } = parmo;
    const todo = Todos.findOne(todoId);
    if (todo.checked === newCheckedStatus) {
      // The status is already what we want, let's not do any extra work
      return;
    }
    if (!todo.editableBy(this.userId)) {
      throw new Meteor.Error('todos.setCheckedStatus.accessDenied',
        'Cannot edit checked status in a private list that is not yours');
    }
    return Todos.update(todoId, { $set: {
      checked: newCheckedStatus,
    }});
  }
}
/**
 * <h4>`setCheckedStatus` export</h4>
 * `'todos.setCheckedStatus'`
 * <p>
 * {@link Todos} <br>
 * {@link Todos_helpers.editableBy} <br>
 * @public
 * @param {{ todoId: string, newCheckedStatus: boolean }} parmo
 * @return {WriteResult}
 */
export const setCheckedStatus = Todos_setCheckedStatus.asMethod;

//////

/**
 * <h4>`updateText` schema</h4>
 * - todoId - `Todos.simpleSchema().schema('_id')`
 * - newText - `Todos.simpleSchema().schema('text')`
 * <p>
 * {@link Todos} <br>
 * @protected
 */
class Todos_updateText_schema {
  todoId = Todos.simpleSchema().schema('_id');
  newText = Todos.simpleSchema().schema('text');
}
/**
 * <h4>`updateText` validator</h4>
 * `Todos_updateText_schema.schemaValidator()`
 * <p>
 * {@link Todos_updateText_validator} <br>
 * @protected
 */
const Todos_updateText_validator = Todos_updateText_schema.schemaValidator();
/**
 * <h4>`updateText` method</h4>
 * `'todos.updateText'`
 * <p>
 * {@link _} <br>
 * {@link Todos} <br>
 * {@link Todos_updateText_validator} <br>
 * @protected
 */
class Todos_updateText {
  /**
   * <h4>`'todos.updateText` </h4>
   * <p>
   * @protected
   */
  name = 'todos.updateText';
  /**
   * <h4>validator</h4>
   * <p>
   * {@link Todos_updateText_validator} <br>
   * @protected
   */
  validate = Todos_updateText_validator;
  /**
   * <h4>`run({ todoId, newText })`</h4>
   * <p>
   * {@link _} <br>
   * {@link Todos} <br>
   * {@link Todos_helpers.editableBy} <br>
   * @protected
   * @param {{ todoId: string, newText: string }} parmo
   * @return {WriteResult}
   * @throws {Meteor.Error} - `'todos.updateText.accessDenied'`
   */
  run(parmo) {
    const { todoId, newText } = parmo;
    // This is complex auth stuff - perhaps denormalizing a userId onto todos
    // would be correct here?
    const todo = Todos.findOne(todoId);
    if (!todo.editableBy(this.userId)) {
      throw new Meteor.Error('todos.updateText.accessDenied',
        'Cannot edit todos in a private list that is not yours');
    }
    return Todos.update(todoId, {
      $set: {
        text: (_.isUndefined(newText) ? null : newText),
      },
    });
  }
}
/**
 * <h4>`updateText` export</h4>
 * `'todos.updateText'`
 * <p>
 * {@link _} <br>
 * {@link Todos} <br>
 * {@link Todos_helpers.editableBy} <br>
 * @public
 * @param {{ todoId: string, newText: string }} parmo
 * @return {WriteResult}
 */
export const updateText = Todos_updateText.asMethod;

//////

/**
 * <h4>`remove` schema</h4>
 * - todoId - `Todos.simpleSchema().schema('_id')`
 * <p>
 * {@link Todos} <br>
 * @protected
 */
class Todos_remove_schema {
  todoId = Todos.simpleSchema().schema('_id');
}
/**
 * <h4>`remove` validator</h4>
 * `Todos_remove_schema.schemaValidator()`
 * <p>
 * {@link Todos_remove_schema} <br>
 * @protected
 */
const Todos_remove_validator = Todos_remove_schema.schemaValidator();
/**
 * <h4>`remove` method</h4>
 * `'todos.remove'`
 * <p>
 * {@link Todos} <br>
 * {@link Todos_remove_validator} <br>
 * {@link Todos_helpers.editableBy} <br>
 * @protected
 */
class Todos_remove {
  /**
   * <h4>`'todos.remove'` </h4>
   * <p>
   * @protected
   */
  name = 'todos.remove';
  /**
   * <h4>validator</h4>
   * <p>
   * {@link Todos_remove_validator} <br>
   * @protected
   */
  validate = Todos_remove_validator;
  /**
   * <h4>`run({ todoId })`</h4>
   * <p>
   * {@link Todos} <br>
   * {@link Todos_helpers.editableBy} <br>
   * @protected
   * @param {string} todoId
   * @return {WriteResult}
   * @throws {Meteor.Error} - `'todos.remove.accessDenied'`
   */
  run({ todoId }) {
    const todo = Todos.findOne(todoId);
    if (!todo.editableBy(this.userId)) {
      throw new Meteor.Error('todos.remove.accessDenied',
        'Cannot remove todos in a private list that is not yours');
    }
    return Todos.remove(todoId);
  }
}
/**
 * <h4>`remove` export</h4>
 * `'todos.remove'`
 * <p>
 * {@link Todos} <br>
 * {@link Todos_helpers.editableBy} <br>
 * @protected
 * @param {string} todoId
 * @return {WriteResult}
 */
export const remove = Todos_remove.asMethod;

//////

/**
 * <h4>Array of Todos method names</h4>
 * <p>
 * {@link insert} <br>
 * {@link setCheckedStatus} <br>
 * {@link updateText} <br>
 * {@link remove} <br>
 * @protected
 */
const Todos_methods = _.pluck([
  insert,
  setCheckedStatus,
  updateText,
  remove,
], 'name');

/**
 * <h4>Server Rate Limiting for `Todos` methods</h4>
 * <p>
 * {@link Todos_methods} <br>
 * @protected
 */
const Todos_methods_rateLimit = Meteor.isServer;
if (Todos_methods_rateLimit) {
  // Only allow 5 todos operations per connection per second
  //noinspection MagicNumberJS
  DDPRateLimiter.addRule({
    name(name) {
      return _.contains(Todos_methods, name);
    },
    // Rate limit per connection ID
    connectionId() { return true; },
  }, 5, 1000);
}
