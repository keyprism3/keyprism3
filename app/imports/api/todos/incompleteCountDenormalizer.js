/**
 * <h4>`incompleteCountDenormalizer` Static Singleton</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Ctodos%5CincompleteCountDenormalizer%2Ejs">C:\_\socialnitro-app\imports\api\todos\incompleteCountDenormalizer.js</a><br>
 * - {@link Todos_incompleteCountDenormalizer#_updateList}      - `_updateList(listId)`
 * - {@link Todos_incompleteCountDenormalizer#afterInsertTodo}  - `afterInsertTodo(todo)`
 * - {@link Todos_incompleteCountDenormalizer#afterUpdateTodo}  - `afterUpdateTodo(selector, modifier)`
 * - {@link Todos_incompleteCountDenormalizer#afterRemoveTodos} - `afterRemoveTodos(todos)`
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link check}          - `meteor/check`
 * - {@link _}              - `meteor/underscore`
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Todos} - `./todos.js`
 * - {@link Lists} - `../lists/lists.js`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/todos/incompleteCountDenormalizer.js
 */
const imports_api_todos_incompleteCountDenormalizer_js= "/imports/api/todos/incompleteCountDenormalizer.js";

import { check } from 'meteor/check';
import { _ } from 'meteor/underscore';

import Ctrl from '/both/ctrl/ctrl';

import { Todos } from './todos.js';
import { Lists } from '../lists/lists.js';

Ctrl.logFileLoaded();
Ctrl.unused(imports_api_todos_incompleteCountDenormalizer_js);

/**
 * <h4>`incompleteCountDenormalizer` export</h4>
 * - {@link Todos_incompleteCountDenormalizer#_updateList}      - `_updateList(listId)`
 * - {@link Todos_incompleteCountDenormalizer#afterInsertTodo}  - `afterInsertTodo(todo)`
 * - {@link Todos_incompleteCountDenormalizer#afterUpdateTodo}  - `afterUpdateTodo(selector, modifier)`
 * - {@link Todos_incompleteCountDenormalizer#afterRemoveTodos} - `afterRemoveTodos(todos)`
 * <p>
 * @protected
 */
class Todos_incompleteCountDenormalizer {
  /**
   * <h4>`_updateList(listId)` internal function</h4>
   * Set list complete count
   * <p>
   * <h5>Dependencies</h5>
   * - {@link Todos}
   * - {@link Lists}
   * @protected
   */
  static _updateList(listId) {
    // Recalculate the correct incomplete count direct from MongoDB
    const incompleteCount = Todos.find({
      listId,
      checked: false,
    }).count();
    Lists.update(listId, { $set: { incompleteCount }});
  }
  /**
   * <h4>`afterInsertTodo(todo)` public function</h4>
   * Update todos list complete count
   * <p>
   * <h5>Dependencies</h5>
   * - {@link Todos_incompleteCountDenormalizer#_updateList} - `_updateList(listId)`
   * @public
   */
  static afterInsertTodo(todo) {
    this._updateList(todo.listId);
  }
  /**
   * <h4>`afterUpdateTodo(selector, modifier)` public function</h4>
   * Update todos list complete count
   * <p>
   * <h5>Dependencies</h5>
   * - {@link check}
   * - {@link Todos}
   * - {@link Todos_incompleteCountDenormalizer#_updateList} - `_updateList(listId)`
   * @public
   */
  static afterUpdateTodo(selector, modifier) {
    // We only support very limited operations on todos
    check(modifier, { $set: Object });
    // We can only deal with $set modifiers, but that's all we do in this app
    if (_.has(modifier.$set, 'checked')) {
      Todos.find(selector, { fields: { listId: 1 }}).forEach((todo) => {
        this._updateList(todo.listId);
      });
    }
  }
  // Here we need to take the list of todos being removed, selected *before* the update
  // because otherwise we can't figure out the relevant list id(s) (if the todo has been deleted)
  /**
   * <h4>`afterRemoveTodos(todos)` public function</h4>
   * Update todos list complete count
   * <p>
   * <h5>Dependencies</h5>
   * - {@link Todos_incompleteCountDenormalizer#_updateList} - `_updateList(listId)`
   * @public
   */
  static afterRemoveTodos(todos) {
    todos.forEach(todo => this._updateList(todo.listId));
  }
}
const incompleteCountDenormalizer = Todos_incompleteCountDenormalizer.asObject;
export default incompleteCountDenormalizer;
