/**
 * <h4>`generateData` exports</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Cgenerate%2Ddata%2Eapp%2Dtests%2Ejs">C:\_\socialnitro-app\imports\api\generate-data.app-tests.js</a><br>
 * - `generateData(cb)`
 *  This file will be auto-imported in the app-test context,
 *  ensuring the `generateFixtures` method is always available
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor}         - `meteor/meteor`
 * - {@link Factory}        - `meteor/dburles:factory`
 * - {@link resetDatabase}  - `meteor/xolvio:cleaner`
 * - {@link Random}         - `meteor/random`
 * - {@link _}              - `meteor/underscore`
 * <p>
 * <h5>NPM Dependencies</h5>
 * none
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}                        - `'/both/ctrl/ctrl'`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link denodeify}                       - `'../utils/denodeify'`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/server/publications.js
 */
const generate_data_app_tests_js = '/imports/api/generate-data.app-tests.js';

import { Meteor } from 'meteor/meteor';
import { Factory } from 'meteor/dburles:factory';
import { resetDatabase } from 'meteor/xolvio:cleaner';
import { Random } from 'meteor/random';
import { _ } from 'meteor/underscore';

import Ctrl from '/both/ctrl/ctrl';

import { denodeify } from '../utils/denodeify';
Ctrl.logFileLoaded();
Ctrl.unused(generate_data_app_tests_js);

/**
 * <h4>`createList` function</h4>
 * `createList(userId)` <br>
 *  Create list with 3 todos for `userId`
 * <p>
 * @protected
 * @param {string} [userId]
 */
const createList = (userId) => {
  const list = Factory.create('list', { userId });
  _.times(3, () => Factory.create('todo', { listId: list._id }));
};
/**
 * <h4>`generateData` methods</h4>
 * - {@link generateData.generateFixtures}
 * <p>
 * @protected
 */
class Meteor_methods_appTests {
  /**
   * <h4>`generateFixtures` method</h4>
   * reset database, create 3 public and 3 private lists
   * <p>
   * @protected
   */
  static generateFixtures() {
    resetDatabase();
    // create 3 public lists
    _.times(3, () => createList());
    // create 3 private lists
    _.times(3, () => createList(Random.id()));
  }
}
Meteor_methods_appTests.createMeteorMethods();

/**
 * <h4>`generateData` export</h4>
 * `generateData(cb)` <br>
 * Calls `generateFixtures` with new connection   {@link Meteor_methods_appTests.generateFixtures} <br>
 * Client only
 * <p>
 * @public
 */
const GenerateData_generateData_export = Meteor.isClient;
let generateData_; // esdoc does not like exports in if
if (GenerateData_generateData_export) {
  // Create a second connection to the server to use to call test data methods
  // We do this so there's no contention w/ the currently tested user's connection
  const testConnection = Meteor.connect(Meteor.absoluteUrl());
  //
  generateData_ = denodeify((cb) => {
    //noinspection JSUnresolvedFunction
    testConnection.call('generateFixtures', cb);
  });
}
const generateData = generateData_;
export { generateData };

