## README

`C:\_\keyprism3\app\imports\api`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi) _Api Home_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/api/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-61) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Cgenerate-data.app-tests.js) `generate-data.app-tests.js` - export `generateData(cb)` function
 - `generateFixtures()` Meteor method

<hr>


### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Clists) `lists` - Lists api
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Ctodos) `todos` - Todos api
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-light-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Cusers) `users` - Users api

### Anticipates

_none_
