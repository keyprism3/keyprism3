/**
 * <h4>`Lists` Methods</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Clists%5Cmethods%2Ejs">C:\_\socialnitro-app\imports\api\lists\methods.js</a><br>
 * - `lists.insert`       - {@link Lists_insert}, {@link Lists_insert_validator}, {@link Lists_insert_schema}
 * - `lists.makePrivate`  - {@link Lists_makePrivate}, {@link Lists_ID_validator}, {@link Lists_ID_schema}
 * - `lists.makePublic`   - {@link Lists_makePublic}, {@link Lists_ID_validator}, {@link Lists_ID_schema}
 * - `lists.updateName`   - {@link Lists_updateName}, {@link Lists_updateName_validator}, {@link Lists_updateName_schema}
 * - `lists.remove`       - {@link Lists_remove}, {@link Lists_ID_validator}, {@link Lists_ID_schema}
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor}          - `meteor/meteor`
 * - {@link DDPRateLimiter}  - `meteor/ddp-rate-limiter`
 * - {@link _}               - `meteor/underscore`
 * <p>
 * <h5>NPM Dependencies</h5>
 * none
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}                      - `'/both/ctrl/ctrl'`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Lists}                       - `'./lists.js'`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/methods.js
 */
const lists_methods_js='/imports/api/lists/methods.js';

import { Meteor }          from 'meteor/meteor';
import { DDPRateLimiter }  from 'meteor/ddp-rate-limiter';
import { _ }               from 'meteor/underscore';

import Ctrl from '/both/ctrl/ctrl';

import { Lists } from './lists.js';

Ctrl.logFileLoaded();
Ctrl.unused(lists_methods_js);

/**
 * <h4>`insert` schema</h4>
 * - language - `{ type: String }`
 * @protected
 */
class Lists_insert_schema {
  language = { type: String };
}
/**
 * <h4>`insert` validator</h4>
 * `Lists_insert_schema.asValidator`
 * <p>
 * {@link Lists_insert_schema} <br>
 * @protected
 */
const Lists_insert_validator = Lists_insert_schema.asValidator;
/**
 * <h4>`insert` method</h4>
 *  `'lists.insert'`
 * <p>
 * {@link Lists} <br>
 * {@link Lists_insert_validator} <br>
 * @protected
 */
class Lists_insert {
  /**
   * <h4>`'lists.insert'`</h4>
   * @protected
   */
  name = 'lists.insert';
  /**
   * <h4>validator</h4>
   * <p>
   * {@link Lists_insert_validator} <br>
   * @protected
   */
  validate = Lists_insert_validator;
  //noinspection JSMethodCanBeStatic
  /**
   * <h4>`run({ language })`</h4>
   * <p>
   * {@link Lists} <br>
   * {@link ListsCollection#insert} <br>
   * @protected
   * @param {string} language
   * @return {WriteResult}
   */
  run({ language }) {
    return Lists.insert({}, null, language);
  }
}
/**
 * <h4>`insert` export</h4>
 * `'lists.insert'`
 * <p>
 * {@link Lists} <br>
 * {@link Lists_insert} <br>
 * @public
 * @param {{language:string}} param
 * @return {WriteResult}
 */
export const insert = Lists_insert.asMethod;

//////

/**
 * <h4>`listId` schema</h4>
 * - listId - `Lists.simpleSchema().schema('_id')`
 * <p>
 * {@link Lists} <br>
 * @protected
 */
class Lists_ID_schema {
  listId = Lists.simpleSchema().schema('_id');
}

/**
 * <h4>`listId` validator</h4>
 * `Lists_ID_schema.schemaValidator()`
 * <p>
 * {@link Lists_ID_schema} <br>
 * @protected
 */
const Lists_ID_validator = Lists_ID_schema.schemaValidator();

/**
 * <h4>`makePrivate` method</h4>
 * `'lists.makePrivate'`
 * <p>
 * {@link Lists} <br>
 * {@link Lists_ID_validator} <br>
 * @protected
 */
class Lists_makePrivate {
  /**
   * <h4>`'lists.makePrivate'`</h4>
   * @protected
   */
  name = 'lists.makePrivate';
  /**
   * <h4>validator</h4>
   * <p>
   * {@link Lists_ID_validator} <br>
   * @protected
   */
  validate = Lists_ID_validator;
  /**
   * <h4>`run({ listId })`</h4>
   * <p>
   * {@link Lists} <br>
   * {@link Lists_helpers.isLastPublicList} <br>
   * @protected
   * @param {string} listId
   * @return {WriteResult}
   * @throws {Meteor.Error} - `'lists.makePrivate.notLoggedIn'`
   * @throws {Meteor.Error} - `'lists.makePrivate.lastPublicList'`
   */
  run({ listId }) {
    if (!this.userId) {
      throw new Meteor.Error('lists.makePrivate.notLoggedIn',
        'Must be logged in to make private lists.');
    }
    const list = Lists.findOne(listId);
    if (list.isLastPublicList()) {
      throw new Meteor.Error('lists.makePrivate.lastPublicList',
        'Cannot make the last public list private.');
    }
    return Lists.update(listId, {
      $set: { userId: this.userId },
    });
  }
}
/**
 * <h4>`makePrivate` export</h4>
 * `'lists.makePrivate'`
 * <p>
 * {@link Lists} <br>
 * {@link Lists_makePrivate}  <br>
 * @public
 * @param {{listId: String}} param
 * @return {WriteResult}
 */
export const makePrivate = Lists_makePrivate.asMethod;

//////

/**
 * <h4>`makePublic` method</h4>
 * `'lists.makePublic'`
 * <p>
 * {@link Lists} <br>
 * {@link Lists_ID_validator} <br>
 * @protected
 */
class Lists_makePublic {
  /**
   * <h4>`'lists.makePublic'`</h4>
   * @protected
   */
  name = 'lists.makePublic';
  /**
   * <h4>validator</h4>
   * <p>
   * {@link Lists_ID_validator} <br>
   * @protected
   */
  validate = Lists_ID_validator;
  /**
   * <h4>`run({ listId })`</h4>
   * <p>
   * {@link Lists} <br>
   * {@link Lists_helpers.editableBy} <br>
   * @protected
   * @param {string} listId
   * @return {WriteResult}
   * @throws {Meteor.Error} - `'lists.makePublic.notLoggedIn'`
   * @throws {Meteor.Error} - `'lists.makePublic.accessDenied'`
   */
  run({ listId }) {
    if (!this.userId) {
      throw new Meteor.Error('lists.makePublic.notLoggedIn',
        'Must be logged in.');
    }
    const list = Lists.findOne(listId);
    if (!list.editableBy(this.userId)) {
      throw new Meteor.Error('lists.makePublic.accessDenied',
        'You don\'t have permission to edit this list.');
    }
    // XXX the security check above is not atomic, so in theory a race condition could
    // result in exposing private data
    return Lists.update(listId, {
      $unset: { userId: true },
    });
  }
}
/**
 * <h4>`makePublic` export</h4>
 * `'lists.makePublic'`
 * <p>
 * {@link Lists} <br>
 * {@link Lists_makePrivate} <br>
 * @public
 * @param {{listId: String}} param
 * @return {WriteResult}
 */
export const makePublic = Lists_makePublic.asMethod;

//////

/**
 * <h4>`updateName` schema</h4>
 * - listId - `Lists.simpleSchema().schema('_id')`
 * - newName - `Lists.simpleSchema().schema('name')`
 * <p>
 * {@link Lists} <br>
 * @protected
 */
class Lists_updateName_schema {
  listId = Lists.simpleSchema().schema('_id');
  newName = Lists.simpleSchema().schema('name');
}
/**
 * <h4>`updateName` validator</h4>
 * `Lists_updateName_schema.schemaValidator()`
 * <p>
 * {@link Lists_updateName_schema} <br>
 * @protected
 */
const Lists_updateName_validator = Lists_updateName_schema.schemaValidator();
/**
 * <h4>`updateName` method</h4>
 * `'lists.updateName'`
 * <p>
 * {@link Lists} <br>
 * {@link Lists_updateName_validator} <br>
 * @protected
 */
class Lists_updateName {
  /**
   * <h4>`'lists.updateName'`</h4>
   * @protected
   */
  name = 'lists.updateName';
  /**
   * <h4>validator</h4>
   * <p>
   * {@link Lists_updateName_validator} <br>
   * @protected
   */
  validate = Lists_updateName_validator;
  /**
   * <h4>`run({ listId, newName })`</h4>
   * <p>
   * {@link Lists} <br>
   * {@link Lists_helpers.editableBy} <br>
   * @protected
   * @param {{listId: string, newName: string}} parmo
   * @return {WriteResult}
   * @throws {Meteor.Error} - `'lists.updateName.accessDenied'`
   */
  run(parmo) {
    const { listId, newName } = parmo;
    const list = Lists.findOne(listId);
    //
    if (!list.editableBy(this.userId)) {
      throw new Meteor.Error('lists.updateName.accessDenied',
        'You don\'t have permission to edit this list.');
    }
    // XXX the security check above is not atomic, so in theory a race condition could
    // result in exposing private data
    return Lists.update(listId, {
      $set: { name: newName },
    });
  }
}
//noinspection JSCheckFunctionSignatures
/**
 * <h4>`updateName` export</h4>
 * `'lists.updateName'`
 * <p>
 * {@link Lists} <br>
 * {@link Lists_updateName} <br>
 * @public
 * @param {{listId: String, newName:String}} param
 * @return {WriteResult}
 */
export const updateName = Lists_updateName.asMethod;

//////

/**
 * <h4>`remove` method</h4>
 * `'lists.remove'`
 * <p>
 * {@link Lists} <br>
 * {@link ListsCollection#remove} <br>
 * {@link Lists_ID_validator} <br>
 * {@link Lists_helpers.editableBy} <br>
 * {@link Lists_helpers.isLastPublicList} <br>
 * @protected
 */
class Lists_remove {
  /**
   * <h4>`'lists.remove'`</h4>
   * @protected
   */
  name = 'lists.remove';
  /**
   * <h4>validator</h4>
   * <p>
   * {@link Lists_ID_validator} <br>
   * @protected
   */
  validate = Lists_ID_validator;
  /**
   * <h4>`run({ listId })`</h4>
   * <p>
   * {@link Lists} <br>
   * {@link Lists_helpers.editableBy} <br>
   * {@link Lists_helpers.isLastPublicList} <br>
   * @protected
   * @param {string} listId
   * @return {WriteResult}
   * @throws {Meteor.Error} - `'lists.remove.accessDenied'`
   * @throws {Meteor.Error} - `'lists.remove.lastPublicList'`
   */
  run({ listId }) {
    const list = Lists.findOne(listId);
    if (!list.editableBy(this.userId)) {
      throw new Meteor.Error('lists.remove.accessDenied',
        'You don\'t have permission to remove this list.');
    }
    // XXX the security check above is not atomic, so in theory a race condition could
    // result in exposing private data
    if (list.isLastPublicList()) {
      throw new Meteor.Error('lists.remove.lastPublicList',
        'Cannot delete the last public list.');
    }
    return Lists.remove(listId);
  }
}
/**
 * <h4>`remove` export</h4>
 * `'lists.remove'`
 * <p>
 * {@link Lists} <br>
 * {@link Lists_remove} <br>
 * @public
 * @param {{listId: String}} param
 * @return {WriteResult}
 */
export const remove = Lists_remove.asMethod;

//////

/**
 * <h4>Array of Lists method names</h4>
 * <p>
 * {@link insert} <br>
 * {@link makePublic} <br>
 * {@link makePrivate} <br>
 * {@link updateName} <br>
 * {@link remove} <br>
 * @protected
 */
const Lists_methods = _.pluck([
  insert,
  makePublic,
  makePrivate,
  updateName,
  remove,
], 'name');

/**
 * <h4>Server Rate Limiting for `Lists` methods</h4>
 * <p>
 * {@link Lists_methods} <br>
 * @protected
 */
const Lists_methods_rateLimit = Meteor.isServer;
if (Lists_methods_rateLimit) {
  // Only allow 5 list operations per connection per second
  //noinspection MagicNumberJS
  DDPRateLimiter.addRule({
    name(name) {
      return _.contains(Lists_methods, name);
    },
    // Rate limit per connection ID
    connectionId() { return true; },
  }, 5, 1000);
}
