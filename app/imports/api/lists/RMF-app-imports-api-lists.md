## RMF-app-imports-api-lists

`C:\_\keyprism3\app\imports\api\lists`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Clists) _Lists api_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5CREADME.md) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Clists%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FYMnti%31qGAqAAAglg) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/F%32EA%39%37A%34-%38%31%36%34-%34%37B%30-A%36%36%38-A%36%33%32BEA%38%39CA%38) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/api/lists/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-110) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/api/lists/lists.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_api_lists_lists_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Clists%5Clists.js) `lists.js` - `Lists` extended Mongo Collection
 - `Lists.insert(list, callback, language='en')`
 - `Lists.remove(selector, callback)`
 - `Lists.schema`
 - `Lists.publicFields`
 - Factory: `list`
 - Helpers: `isPrivate()`, `isLastPublicList()`, `editableBy(userId)`, `todos()`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Clists%5Clists.tests.js) `lists.tests.js` - method tests
 - `lists : mutators : builds correctly from factory`
 - `lists : publications : lists.public : sends all public lists`
 - `lists : publications : lists.private : sends all owned lists`
 - `lists : methods : markPrivate / makePublic : makes a list private and updates the todos`
 - `lists : methods : markPrivate / makePublic : only works if you are logged in`
 - `lists : methods : markPrivate / makePublic : only works if its not the last public list`
 - `lists : methods : markPrivate / makePublic : only makes the list public if you made it private`
 - `lists : methods : updateName : changes the name, but not if you dont have permission`
 - `lists : methods : remove : does not delete the last public list`
 - `lists : methods : remove : does not delete a private list you dont own`
 - `lists : methods : rate limiting : does not allow more than 5 operations rapidly (fails often)`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Clists%5Cmethods.js) `methods.js` - `Lists` methods:
 - `lists.insert({language})`
 - `lists.makePrivate({listId})`
 - `lists.makePublic({listId})`
 - `lists.updateName({listId, newName})`

<hr>


### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-light-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Clists%5Cserver%5CREADME.md) `server` - Lists Publications

### Anticipates

_none_
