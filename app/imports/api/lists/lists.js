/**
 * <h4>`Lists` Collection</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Clists%5Clists%2Ejs">C:\_\socialnitro-app\imports\api\lists\lists.js</a><br>
 * - `ListsCollection`    - Mongo Collection
 * - `Lists` export       - `new ListsCollection('lists')`
 * - `Lists_deny`         - `Lists` Access
 * - `Lists_schema`       - `Lists` Schema
 * - `Lists_publicFields` - `Lists` Public Fields
 * - `Lists_factory`      - `Lists` Factory
 * - `Lists_helpers`      - `Lists` Helpers
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Mongo}        - `meteor/mongo`
 * - {@link SimpleSchema} - `meteor/aldeed:simple-schema`
 * - {@link TAPi18n}      - `meteor/tap:i18n`
 * <p>
 * <h5>NPM Dependencies</h5>
 * none
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}                        - `'/both/ctrl/ctrl'`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Todos}                       - `'../todos/todos.js'`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/lists.js
 */
const lists_js='/imports/api/lists/methods.js';

import { Mongo }        from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { TAPi18n }      from 'meteor/tap:i18n';

import Ctrl from '/both/ctrl/ctrl';

import { Todos } from '../todos/todos.js';

Ctrl.logFileLoaded();
Ctrl.unused(lists_js);

/**
 * <h4>Mongo Collection</h4>
 * <p>
 * - `insert`
 * - `remove`
 * @protected
 */
class ListsCollection extends Mongo.Collection {
  /**
   * <h4>Insert list item</h4>
   * Auto-naming
   * <p>
   * {@link TAPi18n} <br>
   * @override
   * @param {Object} list
   * @param {function} callback
   * @param {string} language
   * @return {WriteResult}
   */
  insert(list, callback, language = 'en') {
    const ourList = list;
    if (!ourList.name) {
      const defaultName = TAPi18n.__('lists.insert.list', null, language);
      let nextLetter = 'A';
      ourList.name = `${defaultName} ${nextLetter}`;
      //
      while (this.findOne({ name: ourList.name })) {
        // not going to be too smart here, can go past Z
        nextLetter = String.fromCharCode(nextLetter.charCodeAt(0) + 1);
        ourList.name = `${defaultName} ${nextLetter}`;
      }
    }
    return super.insert(ourList, callback);
  }
  /**
   * <h4>Remove list item</h4>
   * plain
   * <p>
   * {@link Todos} <br>
   * @override
   * @param {Object} selector
   * @param {function} callback
   * @return {WriteResult}
   */
  remove(selector, callback) {
    Todos.remove({ listId: selector });
    return super.remove(selector, callback);
  }
}
/**
 * <h4>`Lists` __export__</h4>
 * new {@link ListsCollection} `'lists'`
 * <p>
 * - `insert`
 * - `remove`
 * @public
 */
export const Lists = new ListsCollection('lists');

//////

/**
 * <h4>`Lists` Access</h4>
 * Deny all client-side updates since we will be using methods to manage this collection
 * <p>
 * - `insert` true
 * - `update` true
 * - `true` true
 * <p>
 * @protected
 */
class Lists_deny {
  /**
   * `true`
   * @protected
   */
  static insert() { return true; }
  /**
   * `true`
   * @protected
   */
  static update() { return true; }
  /**
   * `true`
   * @protected
   */
  static remove() { return true; }
}
Lists.deny(Lists_deny);

/**
 * <h4>`Lists` Schema</h4>
 * @protected
 */
class Lists_schema {
  /**
   * `type: String, regEx: SimpleSchema.RegEx.Id`
   * @protected
   */
  _id = { type: String, regEx: SimpleSchema.RegEx.Id };
  /**
   * `type: String`
   * @protected
   */
  name = { type: String };
  /**
   * `type: Number, defaultValue: 0`
   * @protected
   */
  incompleteCount = { type: Number, defaultValue: 0 };
  /**
   * `type: String, regEx: SimpleSchema.RegEx.Id, optional: true`
   * @protected
   */
  userId = { type: String, regEx: SimpleSchema.RegEx.Id, optional: true };
}
//noinspection JSCheckFunctionSignatures
Lists_schema.attachSchemaToCollection(Lists);

/**
 * <h4>`Lists` Public Fields</h4>
 * This represents the keys from Lists objects that should be published
 * to the client. If we add secret properties to List objects, don't list
 * them here to keep them private to the server.
 * <p>
 * - `name`
 * - `incompleteCount`
 * - `userId`
 * @protected
 */
const Lists_publicFields = {
  name: 1,
  incompleteCount: 1,
  userId: 1,
};
Lists.publicFields = Lists_publicFields;

/**
 * <h4>`Lists` Factory</h4>
 * `'list'`
 * <p>
 * no members
 * <p>
 * @protected
 */
class Lists_factory {
}
Lists_factory.definesFactoryForCollection('list', Lists);

/**
 * <h4>`Lists` Helpers</h4>
 * - `isPrivate`
 * - `isLastPublicList`
 * - `editableBy`
 * - `todos`
 * @protected
 */
class Lists_helpers {
  /**
   * <h4>Does List NOT have a `userId`</h4>
   * A list is considered to be private if it has a userId set
   * <p>
   * @protected
   * @return {boolean}
   */
  static isPrivate() {
    return !!this.userId;
  }
  /**
   * <h4>Is List the last List with any `userId`</h4>
   * <p>
   * @protected
   * @return {boolean}
   */
  static isLastPublicList() {
    const publicListCount = Lists.find({ userId: { $exists: false }}).count();
    return !this.isPrivate() && 1 === publicListCount;
  }
  /**
   * <h4>Can `userId` edit List</h4>
   * <p>
   * @param {string} userId
   * @return {boolean}
   */
  static editableBy(userId) {
    if (!this.userId) {
      return true;
    }
    return this.userId === userId;
  }
  /**
   * <h4>{Todos} with this List `_id`, sorted on `createdAt` descending</h4>
   * <p>
   * @return {cursor}
   */
  static todos() {
    return Todos.find({ listId: this._id }, { sort: { createdAt: -1 }});
  }
}
//noinspection JSCheckFunctionSignatures
Lists_helpers.attachHelpersToCollection(Lists);
