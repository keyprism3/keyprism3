/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */
/**
 * <h4>`Lists` Server Tests</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Clists%5Clists%2Etests%2Ejs">C:\_\socialnitro-app\imports\api\lists\lists.tests.js</a><br>
 * - `lists : mutators : builds correctly from factory`
 * - `lists : publications : lists.public : sends all public lists`
 * - `lists : publications : lists.private : sends all owned lists`
 * - `lists : methods : markPrivate / makePublic : makes a list private and updates the todos`
 * - `lists : methods : markPrivate / makePublic : only works if you are logged in`
 * - `lists : methods : markPrivate / makePublic : only works if its not the last public list`
 * - `lists : methods : markPrivate / makePublic : only makes the list public if you made it private`
 * - `lists : methods : updateName : changes the name, but not if you dont have permission`
 * - `lists : methods : remove : does not delete the last public list`
 * - `lists : methods : remove : does not delete a private list you dont own`
 * - `lists : methods : rate limiting : does not allow more than 5 operations rapidly (fails often)`
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor}               - `meteor/meteor`
 * - {@link DDP}                  - `meteor/ddp-client`
 * - {@link Random}               - `meteor/random`
 * - {@link _}                    - `meteor/underscore`
 * - {@link Factory}              - `meteor/dburles:factory`
 * - {@link PublicationCollector} - `meteor/johanbrook:publication-collector`
 * - {@link chai}                 - `meteor/practicalmeteor:chai`
 * - {@link assert}               - `meteor/practicalmeteor:chai`
 * <h5>NPM Dependencies</h5>
 * none
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}                        - `'/both/ctrl/ctrl'`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Todos}                       - `'../todos/todos.js'`
 * - {@link Lists}                       - `'./lists.js'`
 * - {@link socialnitro-app/imports/api/lists/methods.js~insert}      - `'./methods.js'`
 * - {@link socialnitro-app/imports/api/lists/methods.js~makePublic}  - `'./methods.js'`
 * - {@link socialnitro-app/imports/api/lists/methods.js~makePrivate} - `'./methods.js'`
 * - {@link socialnitro-app/imports/api/lists/methods.js~updateName}  - `'./methods.js'`
 * - {@link socialnitro-app/imports/api/lists/methods.js~remove}      - `'./methods.js'`
 * - {@link socialnitro-app/imports/api/lists/server/publications.js~lists_server_publications_js}      - `'./server/publications.js'`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/lists.tests.js
 */
const lists_tests_js='/imports/api/lists/lists.tests.js'; // eslint-disable-line no-unused-vars

import { Meteor }               from 'meteor/meteor';
import { DDP }                  from 'meteor/ddp-client';
import { Random }               from 'meteor/random';
import { _ }                    from 'meteor/underscore';
import { Factory }              from 'meteor/dburles:factory';
import { PublicationCollector } from 'meteor/johanbrook:publication-collector';

import { chai, assert } from 'meteor/practicalmeteor:chai';

import Ctrl from '/both/ctrl/ctrl';

import { Todos } from '../todos/todos.js';
import { Lists } from './lists.js';
import { insert, makePublic, makePrivate, updateName, remove } from './methods.js';

import '../../../lib/i18n/en.i18n.json';

Ctrl.unused(lists_tests_js);
Ctrl.logFileLoaded();

/**
 * <h4>`Lists` Server Tests</h4>
 * - `lists : mutators : builds correctly from factory`
 * - `lists : publications : lists.public : sends all public lists`
 * - `lists : publications : lists.private : sends all owned lists`
 * - `lists : methods : markPrivate / makePublic : makes a list private and updates the todos`
 * - `lists : methods : markPrivate / makePublic : only works if you are logged in`
 * - `lists : methods : markPrivate / makePublic : only works if its not the last public list`
 * - `lists : methods : markPrivate / makePublic : only makes the list public if you made it private`
 * - `lists : methods : updateName : changes the name, but not if you dont have permission`
 * - `lists : methods : remove : does not delete the last public list`
 * - `lists : methods : remove : does not delete a private list you dont own`
 * - `lists : methods : rate limiting : does not allow more than 5 operations rapidly (fails often)`
 * <p>
 * @protected
 */
const Lists_server_tests = Meteor.isServer;
if (Lists_server_tests) {
  // eslint-disable-next-line import/no-unresolved
  require('./server/publications.js');
  /**
   * @test {Lists}
   */
  describe('lists', function () {
    /**
     * @test {Lists_factory}
     */
    describe('mutators', function () {
      /**
       * @test {Lists_factory}
       */
      it('builds correctly from factory', function (done) {
        const list = Factory.create('list');
        assert.typeOf(list, 'object');
        assert.match(list.name, /List /);
        done();
      });
    });
    /**
     * @test {Lists_factory}
     * @test {Todos_factory}
     * @todo make a `listWithTodos` factory
     */
    describe('publications', function () {
      //noinspection MagicNumberJS
      this.timeout(15000);
      const userId = Random.id();
      //
      // TODO -- make a `listWithTodos` factory
      const createList = (props = {}) => {
        const list = Factory.create('list', props);
        _.times(3, () => {
          Factory.create('todo', { listId: list._id });
        });
      };
      //
      before(function (done) {
        //noinspection JSPotentiallyInvalidUsageOfThis,MagicNumberJS
        this.timeout(15000);
        Lists.remove({});
        _.times(3, () => createList());
        _.times(2, () => createList({ userId }));
        _.times(2, () => createList({ userId: Random.id() }));
        done();
      });
      /**
       * @test {Lists_public_publication}
       */
      describe('lists.public', function () {
        /**
         * @test {Lists_public_publication}
         */
        it('sends all public lists', function (done) {
          const collector = new PublicationCollector();
          collector.collect('lists.public', (collections) => {
            chai.assert.equal(collections.lists.length, 3);
          });
          done();
        });
      });
      /**
       * @test {Lists_private_publication}
       */
      describe('lists.private', function () {
        /**
         * @test {Lists_private_publication}
         */
        it('sends all owned lists', function (done) {
          const collector = new PublicationCollector({ userId });
          collector.collect('lists.private', (collections) => {
            chai.assert.equal(collections.lists.length, 2);
          });
          done();
        });
      });
    });
    /**
     * @test {Lists_methods}
     */
    describe('methods', function () {
      let listId;
      let todoId;
      let otherListId;
      let userId;
      //
      beforeEach(function (done) {
        // Clear
        Lists.remove({});
        Todos.remove({});
        //
        // Create a list and a todo in that list
        listId = Factory.create('list')._id;
        todoId = Factory.create('todo', { listId })._id;
        //
        // Create throwaway list, since the last public list can't be made private
        otherListId = Factory.create('list')._id;
        //
        // Generate a 'user'
        userId = Random.id();
        done();
      });
      /**
       * @test {Lists_helpers#isPrivate}
       * @test {Lists_helpers#editableBy}
       */
      describe('makePrivate / makePublic', function () {
        function assertListAndTodoArePrivate() {
          assert.equal(Lists.findOne(listId).userId, userId);
          assert.isTrue(Lists.findOne(listId).isPrivate());
          assert.isTrue(Todos.findOne(todoId).editableBy(userId));
          assert.isFalse(Todos.findOne(todoId).editableBy(Random.id()));
        }
        /**
         * @test {Lists_makePrivate}
         * @test {Lists_makePublic}
         * @test {Todos}
         */
        it('makes a list private and updates the todos', function (done) {
          // Check initial state is public
          assert.isFalse(Lists.findOne(listId).isPrivate());
          //
          // Set up method arguments and context
          const methodInvocation = { userId };
          const args = { listId };
          //
          // Making the list private adds userId to the todo
          makePrivate._execute(methodInvocation, args);
          assertListAndTodoArePrivate();
          //
          // Making the list public removes it
          makePublic._execute(methodInvocation, args);
          assert.isUndefined(Todos.findOne(todoId).userId);
          assert.isTrue(Todos.findOne(todoId).editableBy(userId));
          done();
        });
        /**
         * @test {Lists_makePrivate}
         * @test {Lists_makePublic}
         */
        it('only works if you are logged in', function (done) {
          // Set up method arguments and context
          const methodInvocation = { };
          const args = { listId };
          //
          assert.throws(() => {
            makePrivate._execute(methodInvocation, args);
          }, Meteor.Error, /lists.makePrivate.notLoggedIn/);
          //
          assert.throws(() => {
            makePublic._execute(methodInvocation, args);
          }, Meteor.Error, /lists.makePublic.notLoggedIn/);
          done();
        });
        /**
         * @test {Lists}
         * @test {Lists_makePrivate}
         * @test {ListsCollaction#remove}
         */
        it('only works if its not the last public list', function (done) {
          // Remove other list, now we're the last public list
          Lists.remove(otherListId);
          //
          // Set up method arguments and context
          const methodInvocation = { userId };
          const args = { listId };
          //
          assert.throws(() => {
            makePrivate._execute(methodInvocation, args);
          }, Meteor.Error, /lists.makePrivate.lastPublicList/);
          done();
        });
        /**
         * @test {Lists}
         * @test {Lists_makePrivate}
         * @test {Lists_makePublic}
         */
        it('only makes the list public if you made it private', function (done) {
          // Set up method arguments and context
          const methodInvocation = { userId };
          const args = { listId };
          //
          makePrivate._execute(methodInvocation, args);
          //
          const otherUserMethodInvocation = { userId: Random.id() };
          //
          // Shouldn't do anything
          assert.throws(() => {
            makePublic._execute(otherUserMethodInvocation, args);
          }, Meteor.Error, /lists.makePublic.accessDenied/);
          //
          // Make sure things are still private
          assertListAndTodoArePrivate();
          done();
        });
      });
      /**
       * @test {Lists}
       * @test {Lists_updateName}
       * @test {Lists_makePrivate}
       */
      describe('updateName', () => {
        /**
         * @test {Lists}
         * @test {Lists_updateName}
         * @test {Lists_makePrivate}
         */
        it('changes the name, but not if you dont have permission', function (done) {
          updateName._execute({}, {
            listId,
            newName: 'new name',
          });
          //
          assert.equal(Lists.findOne(listId).name, 'new name');
          //
          // Make the list private
          makePrivate._execute({ userId }, { listId });
          //
          // Works if the owner changes the name
          updateName._execute({ userId }, {
            listId,
            newName: 'new name 2',
          });
          //
          assert.equal(Lists.findOne(listId).name, 'new name 2');
          //
          // Throws if another user, or logged out user, tries to change the name
          assert.throws(() => {
            updateName._execute({ userId: Random.id() }, {
              listId,
              newName: 'new name 3',
            });
          }, Meteor.Error, /lists.updateName.accessDenied/);
          //
          assert.throws(() => {
            updateName._execute({}, {
              listId,
              newName: 'new name 3',
            });
          }, Meteor.Error, /lists.updateName.accessDenied/);
          //
          // Confirm name didn't change
          assert.equal(Lists.findOne(listId).name, 'new name 2');
          done();
        });
      });
      /**
       * @test {Lists}
       * @test {Lists_remove}
       * @test {Lists_makePrivate}
       */
      describe('remove', function () {
        /**
         * @test {Lists}
         * @test {Lists_remove}
         */
        it('does not delete the last public list', function (done) {
          const methodInvocation = { userId };
          //
          // Works fine
          remove._execute(methodInvocation, { listId: otherListId });
          //
          // Should throw because it is the last public list
          assert.throws(() => {
            remove._execute(methodInvocation, { listId });
          }, Meteor.Error, /lists.remove.lastPublicList/);
          done();
        });
        /**
         * @test {Lists}
         * @test {Lists_remove}
         * @test {Lists_makePrivate}
         */
        it('does not delete a private list you dont own', function (done) {
          // Make the list private
          makePrivate._execute({ userId }, { listId });
          //
          // Throws if another user, or logged out user, tries to delete the list
          assert.throws(() => {
            remove._execute({ userId: Random.id() }, { listId });
          }, Meteor.Error, /lists.remove.accessDenied/);
          //
          assert.throws(() => {
            remove._execute({}, { listId });
          }, Meteor.Error, /lists.remove.accessDenied/);
          done();
        });
      });
      /**
       * @test {Lists}
       * @test {Lists_insert}
       * @test {Lists_methods_rateLimit}
       */
      describe('rate limiting', function () {
        /**
         * @test {Lists}
         * @test {Lists_insert}
         * @test {Lists_methods_rateLimit}
         */
        it('does not allow more than 5 operations rapidly (fails often)', function (done) {
          //noinspection MagicNumberJS
          this.timeout(45000);
          const connection = DDP.connect(Meteor.absoluteUrl());
          //
          _.times(5, () => {
            //noinspection JSUnresolvedFunction
            connection.call(insert.name, { language: 'en' });
          });
          //
          assert.throws(() => {
            //noinspection JSUnresolvedFunction
            connection.call(insert.name, { language: 'en' });
          }, Meteor.Error, /too-many-requests/);
          //
          connection.disconnect();
          done();
        });
      });
    });
  });
}
