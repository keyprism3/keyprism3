/* eslint-disable prefer-arrow-callback */
/**
 * <h4>`Lists` Publications</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Clists%5Cserver%5Cpublications%2Ejs">C:\_\socialnitro-app\imports\api\lists\server\publications.js</a><br>
 * - `lists.public` - {@link Lists_public_publication}
 * - `lists.private` - {@link Lists_private_publication}
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor}        - `meteor/meteor`
 * <p>
 * <h5>NPM Dependencies</h5>
 * none
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}                        - `'/both/ctrl/ctrl'`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Lists}                       - `'../lists.js'`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/server/publications.js
 */
const lists_server_publications_js = '/imports/api/lists/server/publications.js';

import { Meteor } from 'meteor/meteor';

import Ctrl from '/both/ctrl/ctrl';

import { Lists } from '../lists.js';

Ctrl.logFileLoaded();
Ctrl.unused(lists_server_publications_js);

/**
 * <h4>`'lists.public'` Publication</h4>
 * <p>
 * <h5>Dependencies</h5>
 * - {@link Lists}
 * - {@link Lists_publicFields}
 * @public
 * @return {cursor}
 */
const Lists_public_publication = function listsPublic() {
  return Lists.find({
    userId: { $exists: false },
  }, {
    fields: Lists.publicFields,
  });
};
Meteor.publish('lists.public', Lists_public_publication);

/**
 * <h4>`'lists.private'` Publication</h4>
 * <p>
 * <h5>Dependencies</h5>
 * - {@link Lists}
 * - {@link Lists_publicFields}
 * @public
 * @return {cursor}
 */
const Lists_private_publication = function listsPrivate() {
  if (!this.userId) {
    return this.ready();
  }
  return Lists.find({
    userId: this.userId,
  }, {
    fields: Lists.publicFields,
  });
};
Meteor.publish('lists.private', Lists_private_publication);
