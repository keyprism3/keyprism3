## RMF-app-imports-api-lists-server

`C:\_\keyprism3\app\imports\api\lists\server`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-light-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Clists%5Cserver) _Lists Publications_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/api/lists/server/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/31133E8E-E8DB-9F0A-32C8-10ADBD89AC1D#-4563) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/api/lists/server/publications.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_api_lists_server_publications_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Clists%5Cserver%5Cpublications.js) `publications.js` - publishes:
 - `lists.public`
 - `lists.private`

<hr>


### Folder Built-ins

_none_

### Anticipates

_none_
