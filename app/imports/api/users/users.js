/**
 * <h4>`users` schemas</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Cusers%5Cusers%2Ejs">C:\_\socialnitro-app\imports\api\users\users.js</a><br>
 * - `Schemas.changePassword`
 * - `Schemas.updateProfile`
 * - `Schemas.UserProfile`
 * - `Schemas.User`
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor}        - `meteor/meteor`
 * - {@link SimpleSchema}  - `meteor/aldeed:simple-schema`
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Schemas} - `/imports/startup/both/modules/schemas/_schemas`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/users/users.js
 */
const users_js = '/imports/api/users/users.js';

import { Meteor } from 'meteor/meteor';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

import Ctrl from '/both/ctrl/ctrl';

import Schemas from '/imports/startup/both/modules/schemas/_schemas';

Ctrl.logFileLoaded();
Ctrl.unused(users_js);

/**
 * <h4>`users` schema messages</h4>
 * - `passwordMismatch` -  `"Passwords do not match"`
 * <p>
 * @protected
 */
const Users_schema_messages = {
  "passwordMismatch": "Passwords do not match"
};
SimpleSchema.messages(Users_schema_messages);

/**
 * <h4>`users changePassword` schema</h4>
 * - {@link Users_changePassword_schema#password}
 * - {@link Users_changePassword_schema#passwordConfirmation}
 * <p>
 * @protected
 */
class Users_changePassword_schema {
  /**
   * `type: String, label: "Password", min: 5`
   * @protected
   */
  password = { type: String, label: "Password", min: 5 };
  /**
   * `type: String, min: 5, label: "Password confirmation", custom: function?passwordMismatch`
   * @protected
   */
  passwordConfirmation = { type: String, min: 5, label: "Password confirmation",
    custom: function () {
      if (this.value !== this.field('changePassword.password').value) {
        return "passwordMismatch";
      }
    }
  };
}
Schemas.changePassword = Users_changePassword_schema.asSchema;

/**
 * <h4>`users updateProfile` schema</h4>
 * - {@link Users_updateProfile_schema#name}
 * - {@link Users_updateProfile_schema#email}
 * - {@link Users_updateProfile_schema#company}
 * - {@link Users_updateProfile_schema#address}
 * - {@link Users_updateProfile_schema#changePassword}
 * - {@link Users_updateProfile_schema#currentPassword}
 * <p>
 * @protected
 */
class Users_updateProfile_schema {
  /**
   * `type: String, label: 'Full Name'`
   * @protected
   */
  name = { type: String, label: 'Full Name' };
  /**
   * `type: String, regEx: SimpleSchema.RegEx.Email, label: "Email"`
   * @protected
   */
  email = { type: String, regEx: SimpleSchema.RegEx.Email, label: "Email" };
  /**
   * `type: String, label: 'Your Company', regEx: /^[a-z0-9A-z .]{3,30}$/, optional: true`
   * @protected
   */
  company = { type: String, label: 'Your Company', regEx: /^[a-z0-9A-z .]{3,30}$/, optional: true };
  /**
   * `type: String, label: 'Your Address', optional: true`
   * @protected
   */
  address = { type: String, label: 'Your Address', optional: true };
  /**
   * `type: Schemas.changePassword, optional: true`
   * @protected
   */
  changePassword = { type: Schemas.changePassword, optional: true };
  /**
   * `type: String, optional: true`
   * @protected
   */
  currentPassword = { type: String, optional: true };
}
Schemas.updateProfile = Users_updateProfile_schema.asSchema;

/**
 * <h4>`users UserProfile` schema</h4>
 * - {@link Users_UserProfile_schema#name}
 * - {@link Users_UserProfile_schema#company}
 * - {@link Users_UserProfile_schema#address}
 * - {@link Users_UserProfile_schema#currentProfile}
 * - {@link Users_UserProfile_schema#intro}
 * - {@link Users_UserProfile_schema#selectedProfiles}
 * - {@link Users_UserProfile_schema#schedule}
 * - {@link Users_UserProfile_schema#schedule_timeFormat}
 * - {@link Users_UserProfile_schema#schedule_timezone}
 * - {@link Users_UserProfile_schema#emailReminders}
 * - {@link Users_UserProfile_schema#emailReminders_emptyQueue}
 * - {@link Users_UserProfile_schema#emailReminders_postingFailure}
 * - {@link Users_UserProfile_schema#emailReminders_weeklyDigest}
 * - {@link Users_UserProfile_schema#tooltips}
 * <p>
 * @protected
 */
class Users_UserProfile_schema {
  /**
   * `type: String, label: 'Full Name', optional: true`
   * @protected
   */
  name = { type: String, label: 'Full Name', optional: true };
  /**
   * `type: String, label: 'Your Company', regEx: /^[a-z0-9A-z .]{3,30}$/, optional: true`
   * @protected
   */
  company = { type: String, label: 'Your Company', regEx: /^[a-z0-9A-z .]{3,30}$/, optional: true };
  /**
   * `type: String, label: 'Your Address', optional: true`
   * @protected
   */
  address = { type: String, label: 'Your Address', optional: true };
  /**
   * `type: String, optional: true`
   * @protected
   */
  currentProfile = { type: String, optional: true };
  /**
   * `type: Boolean, optional: true`
   * @protected
   */
  intro = { type: Boolean, optional: true };
  /**
   * `type: [String], optional: true`
   * @protected
   */
  selectedProfiles = { type: [String], optional: true };
  /**
   * `type: Object, optional: true`
   * @protected
   */
  schedule = { type: Object, optional: true };
  /**
   * <h4>`schedule.timeFormat`</h4>
   * `type: String, optional: true`
   * @protected
   */
  schedule_timeFormat = undefined; // esdoc
  'schedule.timeFormat' = { type: String, optional: true };
  /**
   * <h4>`schedule.timezone`</h4>
   * `type: String, optional: true`
   * @protected
   */
  schedule_timezone = undefined; // esdoc
  'schedule.timezone' = { type: String, optional: true };
  /**
   * `type: Object, optional: true`
   * @protected
   */
  emailReminders = { type: Object, optional: true };
  /**
   * <h4>`emailReminders.emptyQueue`</h4>
   * `type: Boolean, optional: true`
   * @protected
   */
  emailReminders_emptyQueue = undefined; // esdoc
  'emailReminders.emptyQueue' = { type: Boolean, optional: true };
  /**
   * <h4>`emailReminders.postingFailure`</h4>
   * `type: Boolean, optional: true`
   * @protected
   */
  emailReminders_postingFailure = undefined; // esdoc
  'emailReminders.postingFailure' = { type: Boolean, optional: true };
  /**
   * <h4>`emailReminders.weeklyDigest`</h4>
   * `type: Boolean, optional: true`
   * @protected
   */
  emailReminders_weeklyDigest = undefined; // esdoc
  'emailReminders.weeklyDigest' = { type: Boolean, optional: true };
  /**
   * `type: Boolean, optional: true`
   * @protected
   */
  tooltips = { type: Boolean, optional: true };
}
Schemas.UserProfile = Users_UserProfile_schema.asSchema;

/**
 * <h4>`users User` schema</h4>
 * - {@link Users_User_schema#emails}
 * - {@link Users_User_schema#emails_$_address}
 * - {@link Users_User_schema#emails_$_verified}
 * - {@link Users_User_schema#createdAt}
 * - {@link Users_User_schema#profile}
 * - {@link Users_User_schema#services}
 * - {@link Users_User_schema#roles}
 * - {@link Users_User_schema#_impersonateToken}
 * - {@link Users_User_schema#registered_emails}
 * <p>
 * @protected
 */
class Users_User_schema {
  /**
   * `type: [Object], optional: true`
   * <p>
   * this must be optional if you also use other login services like facebook,
   *  but if you use only accounts-password, then it can be required
   * @protected
   */
  emails = { type: [Object], optional: true };
  /**
   * <h4>`emails.$.address`</h4>
   * `type: String, regEx: SimpleSchema.RegEx.Email, label: "Email"`
   * @protected
   */
  emails_$_address = undefined; // esdoc
  "emails.$.address" = { type: String, regEx: SimpleSchema.RegEx.Email, label: "Email" };
  /**
   * <h4>`emails.$.verified`</h4>
   * `type: Boolean`
   * @protected
   */
  emails_$_verified = undefined; // esdoc
  "emails.$.verified" = { type: Boolean };
  /**
   * `type: Date`
   * @protected
   */
  createdAt = { type: Date };
  /**
   * `type: Schemas.UserProfile, optional: true`
   * @protected
   */
  profile = { type: Schemas.UserProfile, optional: true };
  /**
   * `type: Object, optional: true, blackbox: true`
   * @protected
   */
  services = { type: Object, optional: true, blackbox: true };
  /**
   * `type: Object, optional: true, blackbox: true`
   * <p>
   * Add `roles` to your schema if you use the meteor-roles package.
   * <br>
   * Option 1: Object type <br>
   * If you specify that type as Object, you must also specify the
   * `Roles.GLOBAL_GROUP` group whenever you add a user to a role. <br>
   * Example: <br>
   * `Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);` <br>
   * You can't mix and match adding with and without a group since
   *  you will fail validation in some cases.
   * @protected
   */
  roles = { type: Object, optional: true, blackbox: true };
  /**
   * `type: String, optional: true`
   * @protected
   */
  _impersonateToken = { type: String, optional: true };
  /**
   * `type: [Object], blackbox: true, optional: true`
   * @protected
   */
  registered_emails = { type: [Object], blackbox: true, optional: true };
}
Schemas.User = Users_User_schema.asSchema;
Meteor.users.attachSchema(Schemas.User);
