## RMF-app-imports-api-users

`C:\_\keyprism3\app\imports\api\users`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-light-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Cusers) _Users api_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/api/users/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-114) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/api/users/users.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_api_users_users_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Cusers%5Cusers.js) `users.js` - Uses schemas
 - `SimpleSchema.messages.passwordMismatch`
 - `Schemas.changePassword`
 - `Schemas.updateProfile`
 - `Schemas.UserProfile`
 - `Schemas.User`

<hr>


### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-light-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Capi%5Cusers%5Cserver) `server` - Users publications

### Anticipates

_none_
