/**
 * <h4>`users` Publications</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Capi%5Cusers%5Cserver%5Cpublications%2Ejs">C:\_\socialnitro-app\imports\api\users\server\publications.js</a><br>
 * - `user` - {@link Users_user_publication}
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor}        - `meteor/meteor`
 * <p>
 * <h5>NPM Dependencies</h5>
 * none
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}                        - `'/both/ctrl/ctrl'`
 * <p>
 * <h5>Local Dependencies</h5>
 * none
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/users/server/publications.js
 */
const users_server_publications_js = '/imports/api/users/server/publications.js';

import { Meteor } from 'meteor/meteor';

import Ctrl from '/both/ctrl/ctrl';

Ctrl.logFileLoaded();
Ctrl.unused(users_server_publications_js);

/**
 * <h4>`'user'` Publication</h4>
 * `_id: this.userId }, { fields: { profile: 1, 'registered_emails': 1 }`
 * <p>
 * <h5>Dependencies</h5>
 * - {@link Meteor}
 * @public
 * @return {(Mongo.Cursor|Array)}
 */
const Users_user_publication = function () {
  if (this.userId) {
    return Meteor.users.find({ _id: this.userId }, { fields: { profile: 1, 'registered_emails': 1 }});
  } else {
    return [];
  }
};
Meteor.publish('user', Users_user_publication);
