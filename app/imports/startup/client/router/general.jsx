/* globals */

import React from 'react';
import { FlowRouter } from 'meteor/kadira:flow-router';
//noinspection JSUnresolvedVariable
import { mount } from 'react-mounter';

import Layouts from '/imports/ui/layouts';
const { Auth, App } = Layouts;

import Pages from '/imports/ui/pages/pages';
const { Dashboard, NotFound } = Pages;

import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();

FlowRouter.notFound = {
  action: function () {
    console.log('notFound');
    mount(NotFound);
  }
};

FlowRouter.route('/', {
  name: 'home',
  action: function () {
    console.log('/');
    mount(Auth, {
      main: <App main={ <Dashboard/> } />
    });
  }
});
