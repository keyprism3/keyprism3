## RMF-app-imports-startup-client-router

`C:\_\keyprism3\app\imports\startup\client\router`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Crouter) _Client Router Startup_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/client/router/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-129) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/startup/client/router/index.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_startup_client_router_index_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Crouter%5Caccounts.jsx) `accounts.jsx` - `FlowRouter.route`:
 - `/login` - `Layouts.Public` - `Pages.Login`
 - `/signup` - `Layouts.Public` - `Pages.Signup`
 - `/forgot-password` - `Layouts.Public` - `Pages.ForgotPassword`
 - `/reset-password/:resetToken` - `Layouts.Public` - `Pages.ResetPassword`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Crouter%5Cgeneral.jsx) `general.jsx` - `FlowRouter.route`:
 - `notFound` - `NotFound`
 - `/` - `App` - `Dashboard`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Crouter%5Cindex.js) `index.js` - imports:
 - `./accounts`
 - `./general`

<hr>


### Folder Built-ins

_none_

### Anticipates

_none_
