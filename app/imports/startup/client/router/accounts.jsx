/* globals sAlert */
/**
 * <h4>`FlowRouter` Accounts Routes</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Cstartup%5Cclient%5Crouter%5Caccounts%2Ejsx">C:\_\socialnitro-app\imports\startup\client\router\accounts.jsx</a><br>
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor}     - `meteor/meteor`
 * - {@link FlowRouter} - `meteor/kadira:flow-router`
 * - {@link sAlert}     - `juliancwirko:s-alert` - globaled
 * <p>
 * <h5>NPM Dependencies</h5>
 * - {@link React} - `react`
 * - {@link mount} - `react-mounter`
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Layouts}            - `/imports/ui/layouts`
 * - {@link AuthContainer}      - `/imports/ui/layouts/auth/auth-container`
 * - {@link Public}             - `/imports/ui/layouts/public/public`
 * - {@link AccountsPages}      - `/imports/ui/pages/accounts_/accounts`
 * - {@link LoginPage}          - `/imports/ui/pages/accounts_/accounts/login/login`
 * - {@link SignupPage}         - `/imports/ui/pages/accounts_/accounts/signup/signup`
 * - {@link ForgotPasswordPage} - `/imports/ui/pages/accounts_/accounts/forgot_password/forgot_password`
 * - {@link ResetPasswordPage}  - `/imports/ui/pages/accounts_/accounts/reset_password/reset_password`
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/startup/client/router/accounts.jsx
 */
const imports_startup_client_router_accounts_jsx= "/imports/startup/client/router/accounts.jsx";

import { Meteor } from 'meteor/meteor';
import React from 'react';
import { FlowRouter } from 'meteor/kadira:flow-router';
//noinspection JSUnresolvedVariable
import { mount } from 'react-mounter';
import Layouts from '/imports/ui/layouts';
import Pages from '/imports/ui/pages/accounts_/accounts';

import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();
Ctrl.unused(imports_startup_client_router_accounts_jsx);

/**
 * <h4>`/login` route</h4>
 * If user, go home, else login and goto `/start`<br>
 * Name: `login`
 * <p>
 * <h5>Dependencies</h5>
 * - {@link Meteor} - `meteor/meteor`
 * - {@link sAlert} - `juliancwirko:s-alert` - globaled
 * - {@link AuthContainer}      - `/imports/ui/layouts/auth/auth-container`
 * - {@link Public}             - `/imports/ui/layouts/public/public`
 * - {@link LoginPage}          - `/imports/ui/pages/accounts_/accounts/login/login`
 * @protected
 */
const Accounts_route_login = {
  triggersEnter: [
    // If user id exists go to home page
    function (context, redirect, stop) {
      if (Meteor.userId()) {
        redirect('/');
      } else {
        const token = context.queryParams.token || null;
        if (token) {
          Meteor.loginWithToken(token, function (err) {
            if (err) {
              return sAlert.error(err.reason);
            }
          });
          // dont let them wait to be redirect to this automatically
          // token usually used on signups not logins.
          // to do if user has visited start go to /
          redirect('/start');
          stop();
        }
      }
    }
  ],
  action: function() {
    console.log('/login');
    mount(Layouts.Auth, {
      main: <Layouts.Public main={ <Pages.Login/> } />
    });
  },
  name: "login" // optional
};
FlowRouter.route('/login', Accounts_route_login);

/**
 * <h4>`/signup` route</h4>
 * Goto Signup Page <br>
 * Name: `signup`
 * <p>
 * <h5>Dependencies</h5>
 * - {@link AuthContainer}      - `/imports/ui/layouts/auth/auth-container`
 * - {@link Public}             - `/imports/ui/layouts/public/public`
 * - {@link SignupPage}         - `/imports/ui/pages/accounts_/accounts/signup/signup`
 * <p>
 * @protected
 * @todo goto home if logged in?
 */
const Accounts_route_signup = {
  action: function() {
    console.log('/signup');
    mount(Layouts.Auth, {
      main: <Layouts.Public main={ <Pages.Signup/> } />
    });
  },
  name: "signup"
};
FlowRouter.route('/signup', Accounts_route_signup);

/**
 * <h4>`/forgot-password` route</h4>
 * Goto Forgot Password Page<br>
 * Name: `forgotPassword`
 * <p>
 * <h5>Dependencies</h5>
 * - {@link AuthContainer}      - `/imports/ui/layouts/auth/auth-container`
 * - {@link Public}             - `/imports/ui/layouts/public/public`
 * - {@link ForgotPasswordPage} - `/imports/ui/pages/accounts_/accounts/forgot_password/forgot_password`
 * <p>
 * @protected
 * @todo goto home if logged in?
 */
const Accounts_route_forgotPassword = {
  action: function() {
    console.log('/forgot-password');
    mount(Layouts.Auth, {
      main: <Layouts.Public main={ <Pages.ForgotPassword/> } />
    });
  },
  name: "forgotPassword" // optional
};
FlowRouter.route('/forgot-password', Accounts_route_forgotPassword);

// Logout route
// FlowRouter.route('/logout', {
//   action: function(params, queryParams) {
//     mount(Auth, {
//       main: <App main={ <login/> } />
//     });
//   },
//
//   name: "logout" // optional
// });

/**
 * <h4>`/reset-password/:resetToken` route</h4>
 * Goto Reset Password Page<br>
 * Name: `resetPassword`
 * <p>
 * <h5>Dependencies</h5>
 * - {@link AuthContainer}      - `/imports/ui/layouts/auth/auth-container`
 * - {@link Public}             - `/imports/ui/layouts/public/public`
 * - {@link ResetPasswordPage}  - `/imports/ui/pages/accounts_/accounts/reset_password/reset_password`
 * <p>
 * @protected
 */
const Accounts_route_resetPassword = {
  name: 'resetPassword',
  action: function() {
    console.log('/reset-password/:resetToken');
    mount(Layouts.Auth, {
      main: <Layouts.Public main={ <Pages.ResetPassword/> } />
    });
  }
};
FlowRouter.route('/reset-password/:resetToken', Accounts_route_resetPassword);
