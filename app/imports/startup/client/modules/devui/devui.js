/* globals */

import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();

//noinspection JSAnnotator,JSUnusedGlobalSymbols,JSUnusedGlobalSymbols
const DevUi={// eslint-disable-line no-global-assign
  Data:{
    icon:{
    }
  },
  Utils:{
    sled(zdot) {
      location.href='sled://z/'+zdot;
    },
    cas(e) {
      let s='';
      if (e.ctrlKey) {s+='c';}
      if (e.altKey) {s+='a';}
      if (e.shiftKey) {s+='s';}
      return s;
    },
    caso(e) {
      return this.cas(e.originalEvent);
    },
    menuItemContainer(o) {
      const oo = {};
      let name = '';
      if (o.file) {
        name+='['+o.file+']';
      }
      if (o.cls) {
        name+='&lt;'+o.cls+'&gt;';
      }
      if (o.zdot) {
        oo.z=o.zdot;
        oo.title='z'+o.zdot;
      }
      oo.name=name;
      if (o.file) {
        if (o.file.indexOf('.jsx')!==-1) {
          //noinspection JSUnusedAssignment
          oo.img=DevUi.Data.icon.reactContainer;
        }
      }
      if (o.zdot) {
        oo.fun=function() {
          DevUi.Utils.sled(o.zdot);
        };
      }
      return oo;
    },
    menuItem($el) {
      const el = $el.get(0);
      let name = '';
      let sname = '';
      const type = $el.attr('data-node-type');
      const cls = $el.attr('data-class');
      const file = $el.attr('data-file');
      const id = $el.attr('id');
      const classes = $el.attr('class');
      if (file) {
        name+='['+file+']';
        sname+='['+file+']';
      }
      if (cls) {
        name+='&lt;'+cls+'&gt;';
        sname+='&lt;'+cls+'&gt;';
      }
      if (el.tagName) {
        name+=el.tagName.toUpperCase();
        sname+=el.tagName.toUpperCase();
      }
      const o={};
      if (id) {
        name+=' #'+id;
      }
      if (classes) {
        name+=' '+classes;
        o.classes=classes;
      }
      o.sname=sname;
      o.name=name;
      const zd=$el.attr('data-zdot');
      o.title='z'+zd;
      o.z=zd;
      if (file) {
        if (file.indexOf('.jsx')!==-1) {
          //noinspection JSUnusedAssignment
          o.img=DevUi.Data.icon.react;
        }
      }
      o.fun=function() {
        DevUi.Utils.sled($el.attr('data-zdot'));
      };
      if ('blaze' === type) {
        const options=JSON.parse($el.attr('data-blaze'));
        //noinspection JSUnusedAssignment
        o.img=DevUi.Data.icon.blaze;
        // {
        //   zdot:'53173497006952301',
        //   cls:'App',
        //   file:'app.jsx',
        //   code:<file>
        //   ctor:{
        //     zdot:
        //   }
        //   dtor:{
        //     zdot:
        //   }
        //   rend:{
        //     zdot:
        //   }
        //   fns:{
        //     zdot:
        //   }
        // }
        const aa=[];
        const keymap={
          'Constructor':'ctor'
          , 'Destructor':'dtor'
          , 'Render':'rend'
          , 'Functions':'fns'
          , 'Events':'evts'
        };
        const ooo = {};
        let name2 = 'Template';
        if (cls) {
          name2+='&lt;'+cls+'&gt;';
        }
        if (file) {
          name2+='['+file+']';
        }
        ooo.name=name2;
        ooo.title=o.title;
        ooo.fun=o.fun;
        //noinspection JSUnusedAssignment
        ooo.img=DevUi.Data.icon.template;
        aa.push(ooo);
        const keys='Constructor Destructor Render Functions Events'.split(' ');
        //noinspection OverlyComplexFunctionJS
        _.each(keys, function(el) {
          // name, title, fun, img
          const oo={};
          oo.name='';
          const abbr=keymap[el];
          const opt=options[abbr];
          if ('undefined' === typeof opt) {
            oo.disable=true;
          }
          oo.name=el;
          if (cls) {
            oo.name+='&lt;'+cls+'&gt;';
          }
          if (opt) {
            if (opt.file) {oo.name+='['+opt.file+']';
            }else{if (options.code) {oo.name+='['+options.code+']';}
            }
          }else{if (options.code) {oo.name+='['+options.code+']';}
          }
          if (opt) {
            if (opt.zdot) {
              oo.title='z'+opt.zdot;
              oo.fun=function() {
                DevUi.Utils.sled(opt.zdot);
              };
            }else{
              oo.disable=true;
            }
          }
          if (DevUi.Data.icon[abbr]) {
            oo.img=DevUi.Data.icon[abbr];
          }
          if (o.title) {
            if (oo.title) {
              oo.title+=' '+o.title;
            }else{
              oo.title=o.title;
            }
          }
          aa.push(oo);
        });
        o.subMenu=aa;
      }//-blaze
      return o;
    },
    buildOne($node) {
      const arr = [];
      if ($node.attr('data-zdot')) {
        const me = this.menuItem($node);
        arr.push(me);
      }
      // push container
      if ($node.attr('data-container')) {
        const me = this.menuItemContainer(JSON.parse($node.attr('data-container')));
        arr.push(me);
      }
      return arr;
    },
    build($node) {
      let arr = [];
      // push me
      const arrMe=this.buildOne($node);
      arr=arr.concat(arrMe);
      const pars = $node.parents('[data-zdot]');
      const self=this;
      pars.each(function(i, el) {
        const $itm = $(el);
        // push it
        const mi = self.menuItem($itm);
        arr.push(mi);
        // push container
        if ($itm.attr('data-container')) {
          const me = self.menuItemContainer(JSON.parse($itm.attr('data-container')));
          arr.push(me);
        }
      });
      return arr;
    },
    getTip(o) {
      //noinspection MagicNumberJS
      const def = {
        style: {
          classes: 'qtip-dark qtip-rounded qtip-shadow'
        }
        , show: {
          delay: 500
        }
        , content: {}
      };
      return $.extend(true, {}, def, o);
    },
    onEvent($that, jqEvent, cas) {
      const cas_=cas;
      $that.on(jqEvent, function(e) {
        const cas=DevUi.Utils.cas(e);
        const $me = $(e.target);
        if (cas!==cas_) {
          //noinspection EmptyCatchBlockJS,UnusedCatchParameterJS
          try {
            $me.contextMenu('destroy');
          } catch (ee) {// eslint-disable-line no-empty
          }
          return;
        }
        e.preventDefault();
        const menu = DevUi.Utils.build($me);
        if (menu.length) {
          $me.contextMenu('popup', menu);
          $me.contextMenu('open', { event:e });
        }
      });
    }
  },
  React:{
    onComponentDidMount(that, options) {
      const $node = $(that);
      $node.attr('data-node-type', 'react');
      if (options.zdot) {
        $node.attr('data-zdot', options.zdot);
      }
      if (options.cls) {
        $node.attr('data-class', options.cls);
      }
      if (options.file) {
        $node.attr('data-file', options.file);
      }
      if (options.container) {
        $node.attr('data-container', JSON.stringify(options.container));
      }
      //noinspection JSUnusedAssignment
      DevUi.Utils.onEvent($node, 'click', 'c');
      //noinspection JSUnusedAssignment
      DevUi.Utils.onEvent($node, 'dblclick', '');
    }
  },
  Blaze:{
    onRendered(that, options) {
      const $els=that.$('>*');
      $els.attr('data-node-type', 'blaze');
      $els.attr('data-blaze', JSON.stringify(options));
      if (options.zdot) {
        $els.attr('data-zdot', options.zdot);
      }
      if (options.cls) {
        $els.attr('data-class', options.cls);
      }
      if (options.file) {
        $els.attr('data-file', options.file);
      }
      //noinspection JSUnusedAssignment
      DevUi.Utils.onEvent($els, 'click', 'c');
      //noinspection JSUnusedAssignment
      DevUi.Utils.onEvent($els, 'dblclick', '');
    }
  },
  Inspect:{
    allZdots() {
      const $zel=$('[data-zdot]');
      let menu=[];
      const $root=$('body');
      //noinspection EmptyCatchBlockJS,UnusedCatchParameterJS
      try{
        $root.contextMenu('destroy');
      }catch (ee) {// eslint-disable-line no-empty
      }
      $zel.each(function(i, el) {
        const $me = $(el);
        const m2=DevUi.Utils.buildOne($me);
        menu=menu.concat(m2);
      });
      if (menu.length) {
        $root.contextMenu('popup', menu, {
          afterOpen:function(data, event) {
            this && this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](event); // !unup - lint hack for webstorm
            //console.log('onOpen:data,event',data,event);
            const menu = data.menu[0];
            const $menu = $(menu);
            $menu.css('position', 'absolute');
            $menu.css('top', '0');
            $menu.css('left', '0');
          },
          onClose:function(data, event) {
            this && this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](event); // !unup - lint hack for webstorm
            const menu = data.menu[0];
            const $menu = $(menu);
            $menu.remove();
          }
        });
        $root.contextMenu('open', { top:0, left:0 });
      }
      //console.log('menu',menu);
    }
  }
};
export default DevUi;
