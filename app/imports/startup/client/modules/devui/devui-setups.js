/* globals Audio */
import DevUi from './devui';
import Ctrl from '/both/ctrl/ctrl';
//import Reload from 'meteor/aldeed:reload-extras';
//import Reload from 'meteor/jwall149:advance-reload';
import { TraceX } from 'meteor/arsnebula:tracex';
Ctrl.logFileLoaded();

DevUi.Setups={
  audio() {
    const playMedia = true;
    // CATCHING Uncaught (in promise) DOMException: Failed to load because no supported source was found
    if (playMedia) {
      let ls=localStorage.getItem('beforeHook');
      if (!ls) {
        localStorage.setItem('beforeHook', 'beforeHook');
        console.log('developer audio cue - ready');
        const audio = new Audio('loaded.mp3');
        let prom = audio.play();
        if (prom!== undefined) {
          prom.then(function() {
          }).catch(function(err) {
            console.log('play error 1:', err);
          });
        }
      }
      //noinspection JSUnresolvedFunction
      //
    }
  },
  contextMenuHook() {
    if (!window.CONTEXTMENUHOOK) {
      const $body=$('body');
      const path='.iw-contextMenu.iw-created.iw-cm-menu.iw-curMenu>li';
      const highlight='contextmenu-highlight';
      // only use first zdot
      function high($item, verb) {// eslint-disable-line no-inner-declarations
        const title = $item.attr('title');
        if (!title) {
          return;
        }
        const title_a=title.split(' ');
        const t=title_a[0];
        $('[data-zdot="'+t.substr(1)+'"]')[verb+'Class'](highlight);
      }
      $body.on('mouseenter', path, function(e) {
        const $item = $(e.currentTarget);
        high($item, 'add');
        //const title = $item.attr('title');
        //$('[data-zdot="'+title.substr(1)+'"]').addClass(highlight);
      });
      $body.on('mouseleave', path, function(e) {
        const $item = $(e.currentTarget);
        high($item, 'remove');
      });
      window.CONTEXTMENUHOOK=true;
    }
  },
  qtipHook() {
    if (!window.QTIPHOOK) {
      window.QTIPHOOK=true;
      window.QTIPHOOKON=false;
      const $body=$('body');
      $body.on('click', function(e) {
        const caso=DevUi.Utils.caso(e);
        if ('s' !== caso) {
          return;
        }
        e.preventDefault();
        window.QTIPHOOKON=!window.QTIPHOOKON;
        console.log('window.QTIPHOOKON', window.QTIPHOOKON);
      });
      $body.on('mouseover', '[data-zdot]', function(e) {
        if (!window.QTIPHOOKON) {
          if (window.QTIPLAST) {
            window.QTIPLAST.qtip('hide');
          }
          return;
        }
        const $node = $(e.currentTarget);
        const meo = DevUi.Utils.menuItem($node);
        const highlight='contextmenu-highlight';
        $node.addClass(highlight);
        //console.log('meo',meo);
        //return;
        //console.log(e.shiftKey);
        //if (!e.shiftKey){
        //  return;
        //}
        e.preventDefault();
        e.stopPropagation();
        const $me = $(this);
        const titleFn = function() {
          let s = '';
          if (meo.img) {
            s += '<img src="' + meo.img + '"/>';
          }
          if (meo.sname) {
            s+= meo.sname;
          }
          return s;
        };
        const textFn = function() {
          let s = '';
          if (meo.z) {
            s += meo.z;
          }
          if (meo.classes) {
            s += '<br>' + meo.classes;
          }
          return s;
        };
        const title = titleFn();
        const text = textFn(e);
        window.QTIPLAST=$me.qtip({
          show:{
            solo:true,
          },
          position:{
            viewport: $('body'),
            target:'mouse',
            adjust:{
              mouse:true
            },
            my:'bottom right'
          },
          content:{
            text:text,
            title:title
          },
          events:{
            hide:function(event, api) {
              const $node = $(e.currentTarget);
              $node.removeClass(highlight);
              if (!window.QTIPHOOKON) {
                api.destroy();
              }
            },
            show:function(event, api) {
              if (!window.QTIPHOOKON) {
                api.destroy();
                //noinspection UnusedCatchParameterJS,EmptyCatchBlockJS
                try{
                  event.preventDefault();
                } catch (e) {// eslint-disable-line no-empty
                }
              }
              // if (!event.originalEvent.shiftKey) {
              //   try{
              //     event.preventDefault();
              //   }catch (e) {// eslint-disable-line no-empty
              //   }
              // }
            }
          }
        });
        window.QTIPLAST.qtip('show');
      });
    }
  },
  inspectorHook() {
    if (!window.INSPECTORHOOK) {
      window.INSPECTORHOOK=true;
      $(document).on('keyup', function(e) {
        const caso=DevUi.Utils.caso(e);
        if ('' !== caso) {
          return;
        }
        // 113 f2
        //noinspection MagicNumberJS
        if (113 === e.keyCode) {
          DevUi.Inspect.allZdots();
        }
        //console.log(e.keyCode);
      });
    }
  },
  init() {
    TraceX.trace.info('Initialize setups');
    if (!TraceX) {
      const myTrace = new TraceX.TraceLogger({ "extra": "data" });
      myTrace.debug("This is some debug information.");
    }
    this.audio();
    this.contextMenuHook();
    this.qtipHook();
    this.inspectorHook();
  }
};
