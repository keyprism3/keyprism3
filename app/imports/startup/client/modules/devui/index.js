import './devui';
import './devui-icons';
import './devui-setups';
import DevUi from './devui';

import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();

export default DevUi;
