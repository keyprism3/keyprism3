## RMF-app-imports-startup-client-modules-devui

`C:\_\keyprism3\app\imports\startup\client\modules\devui`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-light-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cdevui) _Developer UI_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/client/modules/devui/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-125) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/startup/client/modules/devui/index.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_startup_client_modules_devui_index_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cdevui%5Cdevui-icons.js) `devui-icons.js` - Assign:
 - `DevUi.Data.icon.react`
 - `DevUi.Data.icon.reactContainer`
 - `DevUi.Data.icon.blaze`
 - `DevUi.Data.icon.ctor`
 - `DevUi.Data.icon.dtor`
 - `DevUi.Data.icon.rend`
 - `DevUi.Data.icon.fns`
 - `DevUi.Data.icon.evts`
 - `DevUi.Data.icon.template`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cdevui%5Cdevui-setups.js) `devui-setups.js` - Assign `DevUi.Setups`
 - `DevUi.Setups.audio()`
 - `DevUi.Setups.contextMenuHook()`
 - `DevUi.Setups.qtipHook()`
 - `DevUi.Setups.inspectorHook()`
 - `DevUi.Setups.init()`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cdevui%5Cdevui.js) `devui.js` - export `DevUi` static singleton
 - `Data`
 - `Data.icon` object
 - `Utils`
 - `Utils.sled(zdot)`
 - `Utils.cas(e)`
 - `Utils.caso(e)`
 - `Utils.menuItemContainer(o)`
 - `Utils.menuItem($el)`
 - `Utils.buildOne($node)`
 - `Utils.build($node)`
 - `Utils.getTip(o)`
 - `Utils.onEvent($that, jqEvent, cas)`
 - `React`
 - `React.onComponentDidMount(that, options)`
 - `Blaze`
 - `Blaze.onRendered(that, options)`
 - `Inspect`
 - `Inspect.allZdots()`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cdevui%5Cindex.js) `index.js` - imports:
 - `./devui`
 - `./devui-icons`
 - `./devui-setups`

<hr>


### Folder Built-ins

_none_

### Anticipates

_none_
