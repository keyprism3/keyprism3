## RMF-app-imports-startup-client-modules

`C:\_\keyprism3\app\imports\startup\client\modules`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules) _Client Modules_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/client/modules/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-124) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/startup/client/modules/index.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_startup_client_modules_index_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cindex.js) `index.js` - imports:
 - `./_modules`
 - `./devui`
 - `./flow_helpers`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5C_modules.js) `_modules.js` - `Modules.client` object

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-light-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cdevui) `devui` - Developer UI
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-light-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cflow_helpers) `flow_helpers` - FlowRouter Helpers
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-orange-light-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cfoo) `foo` - Demo Test Class

### Anticipates

_none_
