/**
 * <h4>`Mocules.client` object</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Cstartup%5Cclient%5Cmodules%5C%5Fmodules%2Ejs">C:\_\socialnitro-app\imports\startup\client\modules\_modules.js</a><br>
 * Add to Modules
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl`
 * <p>
 * <h5>Local Dependencies</h5>
 * - {@link Modules} - `../../both/modules/_modules`
 *
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/startup/client/modules/_modules.js
 */
const imports_startup_client_modules__modules_js= "/imports/startup/client/modules/_modules.js";

import Ctrl from '/both/ctrl/ctrl';

import Modules from '../../both/modules/_modules';

Ctrl.logFileLoaded();
Ctrl.unused(imports_startup_client_modules__modules_js);

Modules.client = {};

///////// FINAL
