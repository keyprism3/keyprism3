/* globals  */
import { FlowRouter } from 'meteor/kadira:flow-router';
import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();

let pathFor = (path, params) => {
  let query = params && params.query ? FlowRouter._qs.parse(params.query) : {};
  return FlowRouter.path(path, params, query);
};

let urlFor = (path, params) => {
  return Meteor.absoluteUrl(pathFor(path, params));
};

let currentRoute = (route) => {
  FlowRouter.watchPathChange();
  let routeName = FlowRouter.current().route.name;
  if (route.indexOf('|') !== -1) {
    const routes = route.split('|');
    if (-1 < _.indexOf(routes, routeName)) {
      return 'active';
    } else {
      return '';
    }
  }
  return routeName === route ? 'active' : '';
};

let FlowHelpers;// eslint-disable-line no-unused-vars
//noinspection AssignmentResultUsedJS
export default FlowHelpers = { // eslint-disable-line no-undef
  pathFor: pathFor,
  urlFor: urlFor,
  currentRoute: currentRoute
};
//export default FlowHelpers;

