## README

`C:\_\keyprism3\app\imports\startup\client\modules\flow_helpers`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-light-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cflow_helpers) _FlowRouter Helpers_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/client/modules/flow_helpers/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-127) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/startup/client/modules/flow_helpers/index.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_startup_client_modules_flow_helpers_index_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cflow_helpers%5Cflow_helpers.jsx) `flow_helpers.jsx` - export `FlowHelpers`
 - `FlowHelpers.pathFor(path, params)`
 - `FlowHelpers.urlFor(path,params)`
 - `FlowHelpers.currentRoute(route)`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient%5Cmodules%5Cflow_helpers%5Cindex.js) `index.js` - imports:
 - `./flow_helpers`

<hr>


### Folder Built-ins

_none_

### Anticipates

_none_
