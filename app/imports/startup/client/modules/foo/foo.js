//noinspection JSClassNamingConvention
export default class Foo {
  //noinspection ParameterNamingConventionJS
  constructor(Meteor) {
    this.Meteor=Meteor;
  }
  //noinspection FunctionNamingConventionJS
  bar() {
    this && this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    return "woo!";
  }
  doSomething() {
    console.log('a foo doing something');
    //noinspection JSUnresolvedFunction
    this.Meteor.call("something", this.bar());
  }
}

