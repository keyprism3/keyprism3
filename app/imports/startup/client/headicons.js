/**
 * Created by Administrator on 10/29/2016.
 */
(function(){
  //return;
  //console.log('headicons');
  const head = document.getElementsByTagName('head')[0];
  function addApple(sizes){
    //  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/icon/apple-touch-icon-57x57.png" />
    const el = document.createElement('link');
    el.rel="apple-touch-icon-precomposed";
    el.sizes=sizes;
    el.href="images/icon/apple-touch-icon-"+sizes+".png";
    head.appendChild(el);
  }
  const apples=[57,114,72,144,60,76,152];
  apples.forEach(function(n){
    const nn=""+n+"x"+n;
    addApple(nn);
  });

  const pngs=[196,96,32,16,128];
  function addPng(sizes){
    //  //<link rel="icon" type="image/png" href="images/icon/favicon-196x196.png" sizes="196x196" />
    const el = document.createElement('link');
    el.rel="icon";
    el.type="image/png";
    el.sizes=sizes;
    el.href="images/icon/favicon-"+sizes+".png";
    head.appendChild(el);
  }
  pngs.forEach(function(n){
    const nn=""+n+"x"+n;
    addPng(nn);
  });
  const ms={
    TileImage: '144x144'
    ,square70x70logo: '70x70'
    ,square150x150logo: '150x150'
    ,square310x150logo: '310x150'
    ,square310x310logo: '310x310'
  };
  function addMs(n,sizes){
    // <meta name="msapplication-TileImage" content="images/icon/mstile-144x144.png" />
    const el = document.createElement('meta');
    el.name="msapplication-"+n;
    el.content="images/icon/mstile-"+sizes+".png";
    head.appendChild(el);
  }
  Object.keys(ms).forEach(function(k){
    addMs(k,ms[k]);
  });
}());
