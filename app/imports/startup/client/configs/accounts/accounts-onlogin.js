/**
 * <h4>`Accounts.onLogin` </h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Cstartup%5Cclient%5Cconfigs%5Caccounts%5Caccounts%2Donlogin%2Ejs">C:\_\socialnitro-app\imports\startup\client\configs\accounts\accounts-onlogin.js</a><br>
 * nada for now
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Accounts} - `'meteor/accounts-base'`
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl`
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/startup/client/configs/accounts/accounts-onlogin.js
 */
const imports_startup_client_configs_accounts_accounts_onlogin_js = "/imports/startup/client/configs/accounts/accounts-onlogin.js";

import { Accounts } from 'meteor/accounts-base';

import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();
Ctrl.unused(imports_startup_client_configs_accounts_accounts_onlogin_js);

/**
 * <h4>`onLogin` function</h4>
 * nada for now
 * <p>
 * @protected
 */
const Accounts_onLogin = function () {
};
Accounts.onLogin(Accounts_onLogin);
