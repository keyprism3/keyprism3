/* globals */
import Ctrl from '/both/ctrl/ctrl';

// console.log('fixtures.js, Ctrl', Ctrl);
// console.log('fixtures.js, Ctrl.StackTrace', Ctrl.StackTrace);
// console.log('fixtures.js, Ctrl.StackTrace.raw', Ctrl.StackTrace.raw);
//console.log('fixtures.js, fileline', Ctrl.StackTrace.fileline());
Ctrl.logFileLoaded();

Meteor.startup(()=>{
  //console.log('Meteor.startup',Ctrl.StackTrace.raw());
  //console.log('Meteor.startup', Ctrl);
  console.log('Meteor.startup', Ctrl.StackTrace.fileline());
  if (0 === Meteor.users.find().count()||0 === Meteor.users.find({ username: 'markrobbins' }).fetch().length) {
    if (0 === Meteor.users.find({ 'emails.address': "mark.robbins@mrobbinsassoc.com" }).fetch().length) {
      //Meteor.trace.info('create user markrobbins');
      Accounts.createUser({
        username:'markrobbins',
        email:"mark.robbins@mrobbinsassoc.com",
        password:"password"
      });
    }
  }else{
    //Meteor.trace.info('NOT create user markrobbins');
  }
  const newUserEmail="mindprism@gmail.com";
  Meteor.methods({
    //
    'fixtures-test'() {
      Meteor.trace.info('fixtures-test');
      return 'test';
      //Meteor.users.remove({});
    },
    'fixtures-removeNewUser'() {
      const what = {
        'emails.address': newUserEmail
      };
      Meteor.users.remove(what);
    },
    'fixtures-isEmailLoggedIn'(email) {
      const what = {
        'emails.address':email,
        'services.resume.loginTokens.when':{ $exists:true }
      };
      return 1 === Meteor.users.find(what).fetch().length;
    },
    'fixtures-logout'() {
      Meteor.trace.info('fixtures-logout');
      if (Meteor.user()) {
        Meteor.logout();
      }
    },
    'fixtures-hasNewUser'() {
      const what = {
        'emails.address': newUserEmail
      };
      return 1 === Meteor.users.find(what).fetch().length;
    },
    'fixtures-isNewUserEmailSent'() {
      const what = {
        'meta.kind':'email',
        'meta.type':'confirm',
        'meta.email':newUserEmail
      };
      return 1 === Meteor.tracex.find(what).fetch().length;
    },
    'fixtures-resetApp'() {
      Meteor.trace.info('fixtures-resetApp');
      Meteor.users.remove({});
    },
    'fixtures-clearUsers'() {
      Meteor.users.remove({});
    },
    'fixtures-countUsers'() {
      return Meteor.users.find({}).fetch().length;
    },
    'fixtures-getUsers'() {
      return Meteor.users.find({}).fetch();
    },
    'fixtures-createUser'(email, password) {
      Accounts.createUser({ email, password });
    },
    'isEmailAvailable'(email) {
      const what = {
        'emails.address': email
      };
      return 0 === Meteor.users.find(what).fetch().length;
    }
  });

});
