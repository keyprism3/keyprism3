## README

`C:\_\keyprism3\app\imports\startup\server\methods`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-pink-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cmethods) _Server Methods_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/server/methods/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-131) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/startup/server/methods/index.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_startup_server_methods_index_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cmethods%5Cfixtures.js) `fixtures.js` - `Meteor.startup` Methods:
 - `fixtures-test()`
 - `fixtures-removeNewUser()`
 - `fixtures-isEmailLoggedIn(email)`
 - `fixtures-logout()`
 - `fixtures-hasNewUser()`
 - `fixtures-isNewUserEmailSent()`
 - `fixtures-resetApp()`
 - `fixtures-clearUsers()`
 - `fixtures-getUsers()`
 - `fixtures-createUser(email, password)`
 - `fixtures-isEmailAvailable(email)`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cmethods%5Cindex.js) `index.js` - imports:
 - `./fixtures`
 - `./users`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cmethods%5Cusers.js) `users.js` - `Meteor.methods`:
 - `impersonate(userId)`
 - `sendWelcomeEmail(userData)`
 - `updateProfile(doc)`

<hr>


### Folder Built-ins

_none_

### Anticipates

- `<set-name>.js` - other method groups
