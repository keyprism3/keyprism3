/* globals Schemas */

import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();

//noinspection JSUnusedGlobalSymbols
Meteor.methods({
  impersonate: function (userId) {
    this && this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](userId); // !unup - lint hack for webstorm
    /// check(userId, String);
    /// if ('4Zmgp7pcjcoYRDrx2' === this.userId) {
    ///   //noinspection JSUnresolvedVariable
    ///   return Accounts.impSvc.set(userId);
    /// }
  },
  sendWelcomeEmail: function(userData) {
    "use strict";
    check(userData, {
      email: String,
      name: String
    });
    //
    const name = "" === userData.name ? null : userData.name;
    const greeting = name ? ("Hey " + name + ",") : "Hey,";
    //
    const text = greeting + '\n\n' +
      "My name is Mark and I'm the creator of Social Nitro. Just wanted to say thank you for trying out our product!\n\n" +
      "Whether you need help with simple setup or advanced integrations, I'm here to help! I reply every email, everyday, so please don't hesitate to reach out :)\n\n" +
      "What're your first impressions of Social Nitro so far - are you getting the hang of it?\n\n" +
      "Sincerely,\n" +
      "Mark";
    //
    return Email.send({
      to: userData.email,
      //from: "Mark from Socialteria <"+"markuretsky@socialteria.com>",
      from: "Mark from Social Nitro <"+"mark.robbins@socialnitro.com>",
      subject: "Re: Signup",
      text: text
    });
  },
  updateProfile: function (doc) {
    "use strict";
    check(doc, Schemas.updateProfile);
    const user = Meteor.user();
    if (!user) {
      throw new Meteor.Error('Please login');
    }
    //
    // modifiers
    const modifier = {};
    if (doc.name !== user.profile.name) {
      modifier['profile.name'] = doc.name;
    }
    if (doc.email !== user.registered_emails[0].address) {
      modifier['registered_emails.0.email'] = doc.email;
    }
    if (doc.company && doc.company !== user.profile.company) {
      modifier['profile.company'] = doc.company;
    }
    if (doc.address && doc.address !== user.profile.address) {
      modifier['profile.address'] = doc.address;
    }
    //
    // Change password
    if (doc.password) {
      Accounts.setPassword(user._id, doc.password, { logout: false });
    }
    //
    // Update user record if there something to update
    if (!_.isEmpty(modifier)) {
      Meteor.users.update({ _id: user._id }, { $set: modifier });
    }
  }
});
