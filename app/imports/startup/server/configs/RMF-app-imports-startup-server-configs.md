## RMF-app-imports-startup-server-configs

`C:\_\keyprism3\app\imports\startup\server\configs`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-pink-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cconfigs) _`Accounts` Server Configuration_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/server/configs/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-130) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/startup/server/configs/index.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_startup_server_configs_index_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cconfigs%5Caccounts.js) `accounts.js` - Accounts configuration
 - `Accounts.config({oathSecretKey})`
 - `Accounts.emailTemplates.siteName`
 - `Accounts.emailTemplates.from`
 - `Accounts.emailTemplates.resetPassword.text`
 - `Accounts.emailTemplates.verifyEmail.subject`
 - `Accounts.emailTemplates.verifyEmail.text`
 - `Accounts.onLogin` - defer, stripe
 - `Accounts.onCreateUser` - send welcome email

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cconfigs%5Cindex.js) `index.js` - imports:
 - `./accounts`
 - `./services`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cconfigs%5Cservices.js) `services.js` - `Meteor.startup` - `ServiceConfigurations.configurations.upsert()`

### Folder Built-ins

_none_

### Anticipates

_none_
