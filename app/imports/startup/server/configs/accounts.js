/**
 * <h4>`Accounts` Server Configuration</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Cstartup%5Cserver%5Cconfigs%5Caccounts%2Ejs">C:\_\socialnitro-app\imports\startup\server\configs\accounts.js</a><br>
 * - {@link Accounts_server_config}
 * - {@link Accounts_emailTemplates_resetPassword_text}
 * - {@link Accounts_emailTemplates_verifyEmail_subject}
 * - {@link Accounts_emailTemplates_verifyEmail_text}
 * - {@link Accounts_server_Meteor_startup}
 * - {@link Accounts_server_onLogin_defer}
 * - {@link Accounts_server_onLogin}
 * - {@link Accounts_server_onCreateUser_sendWelcomeEmail}
 * - {@link Accounts_server_determineEmail}
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor} - `'meteor/meteor'`
 * - {@link Accounts} - `'meteor/accounts-base'`
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl` <br>
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/startup/server/configs/accounts.js
 */
const imports_startup_server_configs_accounts_js= "/imports/startup/server/configs/accounts.js";

import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();
Ctrl.unused(imports_startup_server_configs_accounts_js);

/**
 * <h4>`Accounts.config` private object</h4>
 * `oauthSecretKey: Meteor.settings.oauthSecretKey` <br>
 * Set configuration
 * <p>
 * <h5>Dependencies</h5>
 * - {@link Meteor}.settings
 * @protected
 */
const Accounts_server_config = {
  oauthSecretKey: Meteor.settings.oauthSecretKey
};
Accounts.config(Accounts_server_config); /// ACTION

//////

/**
 * <h4>`Accounts.emailTemplates.resetPassword.text` private function</h4>
 * Get email body using user and url
 * <p>
 * @protected
 * @param {Object} user
 * @param {string} url
 * @return {string} email body
 */
const Accounts_emailTemplates_resetPassword_text = function(user, url) {
  const greeting = (user.profile && user.profile.name) ?
    ("Hello " + user.profile.name + ",") : "Hello,";
  const _url = url.replace('#/', '');
  return greeting + "\n"
    + "\n"
    + "To reset your password, simply click the link below.\n"
    + "\n"
    + _url + "\n"
    + "\n"
    + "Thanks.\n";
};
/**
 * <h4>`Accounts.emailTemplates.verifyEmail.subject` private function</h4>
 * Get email subject using user.
 * <p>
 * @protected
 * @param {Object} user
 * @return {string} email subject
 */
const Accounts_emailTemplates_verifyEmail_subject = function (user) {
  Ctrl.unused(user);
  return "Verify your Social Nitro email address";
};
/**
 * <h4>`Accounts.emailTemplates.verifyEmail.text` private function</h4>
 * Get email body using user and url
 * <p>
 * @protected
 * @param {Object} user
 * @param {string} url
 * @return {string} email body
 */
const Accounts_emailTemplates_verifyEmail_text = function (user, url) {
  "use strict";
  const greeting = (user.profile && user.profile.name) ?
    ("Hello " + user.profile.name + ",") : "Hello,";
  return greeting + "\n"
    + "\n"
    + "Verifying this address will let you receive notifications and password resets from Social Nitro.\n\n"
    + "To verify your account email, simply click the link below.\n"
    + "\n"
    + url + "\n"
    + "\n"
    + "Thanks.\n";
};
/**
 * <h4>`Accounts.emailTemplates` inits private function</h4>
 * Initialize email templates on Meteor startup
 * <p>
 * @protected
 */
const Accounts_server_Meteor_startup = function () {
  // ToDo move some of these to Meteor Settings
  Accounts.emailTemplates.siteName = "Social Nitro";
  Accounts.emailTemplates.from = "Social Nitro <"+"help@socialnitro.com>";
  Accounts.emailTemplates.resetPassword.text = Accounts_emailTemplates_resetPassword_text;
  Accounts.emailTemplates.verifyEmail.subject = Accounts_emailTemplates_verifyEmail_subject;
  Accounts.emailTemplates.verifyEmail.text = Accounts_emailTemplates_verifyEmail_text;
};
Meteor.startup(Accounts_server_Meteor_startup); /// ACTION

//////

/**
 * <h4>`Accounts.onLogin` deferred private function</h4>
 * ToDo setup stripe account
 * <p>
 * @protected
 * @param {Object} data
 */
const Accounts_server_onLogin_defer = function (data) {
  "use strict";
  const userId = data.user._id;
  Ctrl.unused(userId);
  //noinspection OverlyComplexBooleanExpressionJS,JSUnusedLocalSymbols
  const userEmail = // eslint-disable-line no-unused-vars
    data.user.registered_emails && data.user.registered_emails[0] && data.user.registered_emails[0].address ||
    data.user.emails && data.user.emails[0] && data.user.emails[0].address ||
    data.user.services && data.user.services.facebook && data.user.services.facebook.email ||
    null;
  // const plan = Meteor.plan.findOne({ userId: userId }, {
  //   fields: { userId: 1 }
  // });
  //
  // if (!plan) {
  //   // StripeSubscriptions.createCustomer(userId, 'free-plan', userEmail);
  // }
};
/**
 * <h4>`Accounts.onLogin` defer private function</h4>
 * Calls Meteor.defer<br>
 * Set `Accounts.onLogin`
 * <p>
 * @protected
 * @param {Object} data
 */
const Accounts_server_onLogin = function (data) {
  Meteor.defer(Accounts_server_onLogin_defer.bind(null, data));
};
Accounts.onLogin(Accounts_server_onLogin); /// ACTION

//////

/**
 * <h4>`sendWelcomeEmail` private function</h4>
 * Call Meteor Method <br>
 * Traces to tracex for testing
 * <p>
 * @protected
 * @param {Object} userData
 */
const Accounts_server_onCreateUser_sendWelcomeEmail = function (userData) {
  //noinspection JSUnresolvedFunction
  Meteor.call('sendWelcomeEmail', userData, function(error) {
    //{'meta.kind':'email','meta.type':'confirm'}
    Meteor.tracex.remove({ 'meta.kind':'email', 'meta.email':userData.email });
    if (error&&Object.keys(error).length) { //wierd
      Meteor.trace.info('email not sent', { kind:'email', type:'error', error:error, email:userData.email });
      return console.log(error);
    } else {
      Meteor.trace.info('email sent', { kind:'email', type:'confirm', email:userData.email });
    }
  });
};
/**
 * <h4>Find email from user, private function </h4>
 * Use `user.services.facebook`
 * <p>
 * @protected
 * @param {Object} user
 * @return {(string|null)}
 */
const Accounts_server_determineEmail = function(user) {
  "use strict";
  let emailAddress, services;
  if (user.emails) {
    emailAddress = user.emails[0].address;
    return emailAddress;
  } else if (user.services) {
    services = user.services;
    if (services.facebook) {
      emailAddress = services.facebook.email;
    } else {
      emailAddress = null;
    }
    return emailAddress;
  } else {
    return null;
  }
};
/**
 * <h4>`Accounts.onCreateUser` private function</h4>
 * Send Welcome Email and set `user.profile`
 * <p>
 * @protected
 */
const Accounts_server_onCreateUser = function(options, user) {
  "use strict";
  const userData = {
    email: Accounts_server_determineEmail(user),
    name: options.profile ? options.profile.name : ""
  };
  if (null !== userData.email) {
    //noinspection MagicNumberJS
    Meteor.setTimeout(Accounts_server_onCreateUser_sendWelcomeEmail.bind(null, userData), 10000);
  }
  if (options.profile) {
    user.profile = options.profile;
  }
  return user;
};
Accounts.onCreateUser(Accounts_server_onCreateUser); /// ACTION

