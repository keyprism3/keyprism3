## README

`C:\_\keyprism3\app\imports\startup\server`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-pink-dark-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver) _Server Startups_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/server/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-67) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/startup/server/index.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_startup_server_index_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Caccount-hooks.js) `account-hooks.js` - `Accounts.onCreateUser`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cindex.js) `index.js` - imports:
 - _none_ - todo fix

<hr>


### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-pink-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cconfigs) `configs` - `Accounts` Server Configuration
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-pink-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cmethods) `methods` - Server Methods
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-pink-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver%5Cmodules) `modules` - Server Modules Home

### Anticipates

_none_
