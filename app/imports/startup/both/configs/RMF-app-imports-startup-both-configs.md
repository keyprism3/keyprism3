## RMF-app-imports-startup-both-configs

`C:\_\keyprism3\app\imports\startup\both\configs`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cconfigs) _Startup Both Configurations Home_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/both/configs/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-116) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/startup/both/configs/index.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_startup_both_configs_index_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cconfigs%5Cindex.js) `index.js` - import `./accounts/accounts`

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-light-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cconfigs%5Caccounts) `accounts` - Accounts Configuration

### Anticipates

- `<subject-folder>`
