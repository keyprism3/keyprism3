/**
 * <h4>`./accounts/accounts` imports</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Cstartup%5Cboth%5Cconfigs%5Cindex%2Ejs">C:\_\socialnitro-app\imports\startup\both\configs\index.js</a><br>
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl`<p>
 * <h5>Imports</h5>
 * - {@link startup_both_configs_accounts_accounts_js} - `./accounts/accounts`
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/startup/both/configs/index.js
 */
const startup_both_configs_index_js = '/imports/startup/both/configs/index.js';

import './accounts/accounts';

import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();
Ctrl.unused(startup_both_configs_index_js);

/// FINAL
