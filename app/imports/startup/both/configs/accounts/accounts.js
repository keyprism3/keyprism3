/**
 * <h4>`Accounts.config` </h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Cstartup%5Cboth%5Cconfigs%5Caccounts%5Caccounts%2Ejs">C:\_\socialnitro-app\imports\startup\both\configs\accounts\accounts.js</a><br>
 * {@link Accounts_config} <br>
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl`
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/startup/both/configs/accounts/accounts.js
 */
const startup_both_configs_accounts_accounts_js = '/imports/startup/both/configs/accounts/accounts.js';

import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();
Ctrl.unused(startup_both_configs_accounts_accounts_js);

/**
 * <h4>`Accounts.config` object</h4>
 * - `sendVerificationEmail: true`
 * - `forbidClientAccountCreation: false`
 * <p>
 * @protected
 */
const Accounts_config = {
  sendVerificationEmail: true,
  forbidClientAccountCreation: false
};
Accounts.config(Accounts_config);

///////// FINAL

