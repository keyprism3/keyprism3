## README

`C:\_\keyprism3\app\imports\startup\both\configs\accounts`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-light-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cconfigs%5Caccounts) _Accounts Configuration_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/both/configs/accounts/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-117) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/startup/both/configs/accounts/accounts.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_startup_both_configs_accounts_accounts_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cconfigs%5Caccounts%5Caccounts.js) `accounts.js` - `Accounts.config(config)`
 - `config.sendVerificationEmail: true`
 - `config.forbidClientAccountCreation: true`

<hr>


### Folder Built-ins

_none_

### Anticipates

_none_
