## README

`C:\_\keyprism3\app\imports\startup\both`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth) _Startup Both_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/both/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-65) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/startup/both/index.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_startup_both_index_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cindex.js) `index.js` - imports:
 - `./useraccounts-configuration.js`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Croutes.jsx) `routes.jsx` - export `RouteHelpers`, Routes - move this
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cuseraccounts-configuration.js) `useraccounts-configuration.js` - notation - no code

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cconfigs) `configs` - Startup Both Configurations Home
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cmodules) `modules` - Modules Home

### Anticipates

_none_
