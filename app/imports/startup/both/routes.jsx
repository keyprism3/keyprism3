/* global Meteor*/
import { FlowRouter } from 'meteor/kadira:flow-router';

import Home from '../../ui/pages/Home/Home';
import About from '../../ui/pages/About/About';
import Register from '../../ui/pages/Register/Register';
import Login from '../../ui/pages/Login/Login';
import Logout from '../../ui/pages/Logout/Logout';
import Recovery from '../../ui/pages/Recovery/Recovery';

import MainLayout from '../../ui/layouts/MainLayout';
import Header from '../../ui/components/Header/Header';
import Footer from '../../ui/components/Footer/Footer';

//noinspection JSUnresolvedVariable
import { mount } from 'react-mounter';

const PageTitles={
  Home: "Home"
  ,About: "About KeyPrism3"
  ,Login: "Login"
  ,Logout: "Logout"
  ,Register: "Register Account"
  ,Recovery: "Recover Password"
};

function renderMainLayoutWith(component,page){
  //debugger;
  mount(MainLayout,{
    header: <Header pageTitle={PageTitles[page]}/>
    ,content: component
    ,footer: <Footer />
    ,page: page
  });
  /*
  ReactLayout.render(MainLayout,{
    header: <Header />,
    content: component,
    footer: <Footer />
  });
  */
}

class RouteHelpers{
  static Webstorm(){
    this["UNUSED CLASS\n"] && this["UNUSED CLASS\n"](RouteHelpers); // !unuc - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.doNothing); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.goLogin); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.checkLoggedIn); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.redirectIfLoggedIn); // !unuf - lint hack for webstorm
  }
  static doNothing(ctx, redirect) {
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](ctx); // !unup - lint hack for webstorm
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](redirect); // !unup - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.Webstorm); // !unuf - lint hack for webstorm
  }
  static goLogin(ctx, redirect) {
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](ctx); // !unup - lint hack for webstorm
    if (!Meteor.userId()) {
      redirect('/login');
    }
  }
  static checkLoggedIn(ctx, redirect) {
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](ctx); // !unup - lint hack for webstorm
    if (!Meteor.userId()) {
      redirect('/');
    }
  }
  static redirectIfLoggedIn(ctx, redirect) {
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](ctx); // !unup - lint hack for webstorm
    if (Meteor.userId()) {
      redirect('/user-profile');
    }
  }
}
export default RouteHelpers;

FlowRouter.route("/", {
  name: "Home",
  action(/*params*/) {
    //params=params;
    renderMainLayoutWith(<Home />,"Home");
  }
});

FlowRouter.route("/about", {
  name: "About",
  action(/*params*/) {
    //params=params;
    renderMainLayoutWith(<About />,"About");
  }
});

FlowRouter.route("/register", {
  name: "Register",
  action(/*params*/) {
    //params=params;
    renderMainLayoutWith(<Register />,"Register");
  }
});
FlowRouter.route("/login", {
  name: "Login",
  action(/*params*/) {
    //params=params;
    renderMainLayoutWith(<Login />,"Login");
  }
});
FlowRouter.route("/recovery", {
  name: "Recovery",
  action(/*params*/) {
    //params=params;
    renderMainLayoutWith(<Recovery />,"Recovery");
  }
});
FlowRouter.route("/logout", {
  name: "Logout",
  action(/*params*/) {
    //params=params;
    renderMainLayoutWith(<Logout />,"Logout");
  }
});

FlowRouter.notFound = {
  action() {
    renderMainLayoutWith(<h1>404 Not Found</h1>);
  },
};
/*
 FlowRouter.route("/login",{
 name: "Login",
 triggersEnter:[redirectIfLoggedIn],
 action(params){
  renderMainLayoutWith(<UserLogin />);
}
});
FlowRouter.route("/register",{
  name: "Register",
  triggersEnter:[redirectIfLoggedIn],
  action(params){
    renderMainLayoutWith(<UserRegister />);
  }
});
FlowRouter.route("/account-created",{
  name: "Account Created",
  triggersEnter:[redirectIfLoggedIn],
  action(params/){
    renderMainLayoutWith(<UserAccountCreated />);
  }
});
FlowRouter.route("/user-profile",{
  name: "User Profile",
  triggersEnter:[goLogin],
  action(params){
    renderMainLayoutWith(<UserProfile />);
  }
});
*/
