## README

`C:\_\keyprism3\app\imports\startup\both\modules`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cmodules) _Modules Home_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/both/modules/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-118) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/imports/startup/both/modules/index.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/imports_startup_both_modules_index_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-index-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cmodules%5Cindex.js) `index.js` - imports:
 - `./schemas`
 - `./modules.js`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cmodules%5C_modules.js) `_modules.js` - export `Modules` object
 - `Modules.both` object

<hr>


### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-light-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth%5Cmodules%5Cschemas) `schemas` - Schemas Module

### Anticipates

_none_
