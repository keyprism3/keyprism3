/**
 * <h4>`./schemas`, `./_modules.js` </h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Cstartup%5Cboth%5Cmodules%5Cindex%2Ejs">C:\_\socialnitro-app\imports\startup\both\modules\index.js</a><br>
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl`
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/startup/both/index.js
 */
const startup_both_modules_index_js = '/imports/startup/both/modules/index.js';

import './schemas';
import './_modules.js';

import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();
Ctrl.unused(startup_both_modules_index_js);

/// FINAL
