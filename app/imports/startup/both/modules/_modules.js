/**
 * <h4>`Modules`, `Modules.both` objects</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Cstartup%5Cboth%5Cmodules%5C%5Fmodules%2Ejs">C:\_\socialnitro-app\imports\startup\both\modules\_modules.js</a><br>
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl`
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/startup/both/modules/_modules.js
 */
const imports_startup_both_modules__modules_js= "/imports/startup/both/modules/_modules.js";

import Ctrl from '/both/ctrl/ctrl';

Ctrl.logFileLoaded();
Ctrl.unused(imports_startup_both_modules__modules_js);

/**
 * <h4>`Modules` export</h4>
 * `both: {}`
 * <p>
 * @protected
 */
const Modules = { both: {}};
export default Modules;
