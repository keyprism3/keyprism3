/**
 * <h4>`Schemas` module export</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cimports%5Cstartup%5Cboth%5Cmodules%5Cschemas%5C%5Fschemas%2Ejs">C:\_\socialnitro-app\imports\startup\both\modules\schemas\_schemas.js</a><br>
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl`
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/startup/both/modules/schemas/_schemas.js
 */
const imports_startup_both_modules_schemas__schemas_js= "/imports/startup/both/modules/schemas/_schemas.js";

import Ctrl from '/both/ctrl/ctrl';

Ctrl.logFileLoaded();
Ctrl.unused(imports_startup_both_modules_schemas__schemas_js);

/**
 * <h4>`Schemas` module export</h4>
 * `{}`
 * <p>
 * @protected
 */
const Schemas = {};
export default Schemas;

/**
 * <h4>`Schemas` template helper</h4>
 * `Template.registerHelper("Schemas", Schemas)`
 * <p>
 * @protected
 */
const Schemas_register_template_helper = Meteor.isClient;
if (Schemas_register_template_helper) {
  Template.registerHelper("Schemas", Schemas);
}
