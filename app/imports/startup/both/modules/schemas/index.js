/**
 * <h4>`./_schemas` </h4>
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - `/both/ctrl/ctrl`
 * <p>
 * @protected
 */
const startup_both_modules_schemas_index_js= "/imports/startup/both/modules/schemas/index.js";

import './_schemas';
import Ctrl from '/both/ctrl/ctrl';
Ctrl.logFileLoaded();
Ctrl.unused(startup_both_modules_schemas_index_js);

/// FINAL
