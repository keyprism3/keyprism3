## README

`C:\_\keyprism3\app\imports\startup`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup) _Startup Home_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/imports/startup/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-62) - webbrain

### File Built-ins

_none_

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cboth) `both` - Startup Both
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cclient) `client` - Startup Client
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-pink-dark-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cimports%5Cstartup%5Cserver) `server` - Server Startups

### Anticipates

_none_
