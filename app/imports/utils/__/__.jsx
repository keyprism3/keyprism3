//noinspection JSLint,JSLint
export default class __{
  constructor(){
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](__.callIfFn); // jshint ignore:line
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](__.getId); // jshint ignore:line
  }
  static callIfFn(that,name, ...args){
    if (typeof that[name]==='function') {
      return that[name](...args);
    }
  }
  static getId(el){
    let ele=$(el);
    if (ele.attr('name')) {
      return ele.attr('name');
    }
    if (ele.attr('data-id')) {
      return ele.attr('data-id');
    }
    if (ele.attr('id')) {
      return ele.attr('id');
    }
  }
}
