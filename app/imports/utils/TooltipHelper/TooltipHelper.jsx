/*
Global          Specific          Type             Values                                Description
 place           data-place        String           top, right, bottom, left              placement
 type            data-type         String           success, warning, error, info, light  theme
 effect          data-effect       String           float, solid                          behaviour of tooltip
 event           data-event        String           e.g. click                            custom event to
                                                                                            trigger tooltip
 eventOff        data-event-off    String           e.g. click                            custom event to hide
                                                                                            tooltip (only makes
                                                                                            effect after setting
                                                                                            event attribute)
 globalEventOff  -                 String           e.g. click                            global event to
                                                                                            hide tooltip
                                                                                            (global only)
 isCapture       data-iscapture    Bool             true, false                           when set to true, custom
                                                                                            event's propagation mode
                                                                                            will be capture
 offset          data-offset       Object           top, right, bottom, left              data-offset=
                                                                                           "{'top': 10, 'left': 10}"
                                                                                           for specific and offset=
                                                                                           {{top: 10, left: 10}}
                                                                                           for global
 multiline       data-multiline    Bool             true, false                           support <br>, <br />
                                                                                            to make multiline
 class           data-class        String           <names>                               extra custom class, can
                                                                                            use !important to
                                                                                            overwrite
                                                                                            react-tooltip's default
                                                                                            class
 html            data-html         Bool             true, false                           <p data-tip=
                                                                                            "<p>HTML tooltip</p>"
                                                                                            data-html={true}></p> or
                                                                                            <ReactTooltip
                                                                                            html={true} />
 delayHide       data-delay-hide   Number           <number>                              <p data-tip="tooltip"
                                                                                            data-delay-hide='1000'>
                                                                                            </p> or <ReactTooltip
                                                                                            delayHide={1000} />
 delayShow       data-delay-show   Number           <number>                              <p data-tip="tooltip"
                                                                                            data-delay-show='1000'>
                                                                                            </p> or <ReactTooltip
                                                                                            delayShow={1000} />
 border          data-border       Bool             true, false                           Add one pixel white border
 getContent      null              Func or Array    () => {}, [() => {}, Interval]        Generate the tip content
                                                                                            dynamically
 afterShow       null              Func             () => {}                              Function that will be
                                                                                            called after tooltip
                                                                                            show
 afterHide       null              Func             () => {}                              Function that will be
                                                                                            called after tooltip
                                                                                            hide
 disable         data-tip-disable  Bool             true, false                           Disable the tooltip
                                                                                            behaviour, default is
                                                                                            false
 scrollHide      data-scroll-hide  Bool             true, false                           Hide the tooltip when
                                                                                            scrolling, default is
                                                                                            true
 resizeHide      null              Bool             true, false                           Hide the tooltip when
                                                                                            resizing the window,
                                                                                            default is true

 https://github.com/wwayne/react-tooltip
*/

export default class TooltipHelper{
  constructor(itemProps){
    this.itemProps={};
    const dataTips={
      tip: 'string'
      ,place: 'string'
      ,type: 'string'
      ,effect: 'string'
      ,event: 'string'
      ,'event-off': 'string'
      ,iscapture: 'boolean'
      ,offset: 'object'
      ,multiline: 'boolean'
      ,class: 'string'
      ,html: 'boolean'
      ,'delay-hide': 'number'
      ,'delay-show': 'number'
      ,border: 'boolean'
      ,'tip-disable': 'boolean'
      ,'scroll-hide': 'boolean'
    };
    //var otherTipNames='globalEventOff getContent afterShow afterHide resizeHide';
    //var dataTipNamesArray=dataTipNames.split(' ');
    Object.keys(dataTips).forEach((name)=>{
      let n='data-'+name;
      let type=dataTips[name];
      let val=itemProps[n];
      if(val) {//!==undefined
        if(type!==typeof val) {
          throw 'improper type:'+(typeof val)+' in props.'+n+', expected '+type;
        }
        this.itemProps[n]=val;
      }
    });
  }
}
