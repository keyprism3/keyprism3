import _ from 'underscore';
export default class PropsHelper{
  constructor(){
    this["UNUSED MEMBER\n"] && this["UNUSED MEMBER\n"](this.Webstorm); // !unum - lint hack for webstorm
  }
  Webstorm(){
    this["UNUSED MEMBER\n"] && this["UNUSED MEMBER\n"](PropsHelper.eventHooksFor); // !unum - lint hack for webstorm
  }
  static eventListStd(){
    return ["onKeyDown", "onKeyPress", "onKeyUp", "onCopy", "onCut", "onPaste", "onFocus", "onBlur", "onChange"
      , "onInput", "onSubmit", "onClick", "onContextMenu", "onDoubleClick", "onDrag", "onDragEnd", "onDragEnter"
      , "onDragExit", "onDragLeave", "onDragOver", "onDragStart", "onDrop", "onMouseDown", "onMouseEnter"
      , "onMouseLeave", "onMouseMove", "onMouseOut", "onMouseOver", "onMouseUp", "onSelect", "onTouchCancel"
      , "onTouchEnd", "onTouchMove", "onTouchStart", "onScroll", "onWheel"];
  }
  static eventListFor(that){
    if (!that) {
      return this.eventListStd();
    }
    if (typeof that.constructor.eventList==='function') {
      const event_list=that.constructor.eventList();
      if (typeof event_list==='string') {
        return this['eventList'+event_list]();
      }else if ($.isArray(event_list)){
        return event_list;
      }
    }
  }
  static eventHooksFor(that){
    const props=that.props;
    const o={};
    const list=this.eventListFor(that);
    list.forEach((n)=>{
      const fn=props[n];
      const handle_name="handle"+n.substring(2);
      const handler=that[handle_name];
      const has_handler=typeof handler==='function';
      const has_prop=typeof fn==='function';
      if (has_handler&&has_prop) {
        o[n]=_.wrap(handler, function(func, event) {
          func(event);
          fn(event); // client
        });
      }else if (has_handler&&!has_prop) {
        o[n]=handler;
      }else if (!has_handler&&has_prop) {
        o[n]=fn;
      }else if (!has_handler&&!has_prop) {
        this["EMPTY BLOCK\n"] && this["EMPTY BLOCK\n"](has_prop); // !unup - lint hack for webstorm
      }
    });
    return o;
  }
}
