Package.describe({
  name: 'voidale:helpers',
  version: '0.0.1',
  summary: 'Server / Client misc global functions'
});

Package.onUse(function (api) {
  api.versionsFrom('0.9.0');
  api.use(['url', 'underscore'], ['server', 'client']);
  api.addFiles('client.js', 'client');
  api.addFiles('server.js', 'server');
  api.addFiles('both.js', ['server', 'client']);
  api.export('helpers', ['server', 'client']);
});
