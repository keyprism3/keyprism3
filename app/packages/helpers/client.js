/* globals helpers sAlert */
helpers = helpers || {};// eslint-disable-line no-global-assign

helpers.validateEmail = function(email) {
  if (helpers.isValidEmail(email)) {
    return true;
  }
  sAlert.error('Invalid Email');
  return false;
};

helpers.validatePassword = function(password) {
  if (helpers.isValidPassword(password)) {
    return true;
  }
  sAlert.error('Password must be at least 6 characters long');
  return false;
};
