/* globals helpers */
helpers = helpers || {};// eslint-disable-line no-global-assign

//noinspection JSUnusedGlobalSymbols
_.extend(helpers, {
  example() {
    //noinspection UnnecessaryLocalVariableJS
    const rv='example';
    return rv;
  },
  capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  },
  isValidEmail(email) {
    const re = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    return re.test(email);
  },
  isValidPassword(password) {
    return 6 <= password.length;
  }
});

