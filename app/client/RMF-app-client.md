## RMF-app-client

`C:\_\keyprism3\app\client`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient) _Client Main_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/client/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-46) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/client/main.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/client_main_js/index.html) - plato

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5CFlowHelpers.js) `FlowHelpers.js` - move this
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Chead.html) `head.html` - title, description, viewport, application-name, msapplication-TileColor, Material Icons font
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Cmain.js) `main.js` - imports:
 - `/imports/startup/client`
 - `/imports/startup/both`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Cmain.scss) `main.scss` - imports:
 - `{poetic:materialize-scss}/sass/components/_color.scss`
 - `./scss/_variables.scss`
 - `{poetic:materialize-scss}/sass/materialize.scss`
 - `./main2.scss`

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Cmain%32.scss) `main2.scss` - generated, imports `scss` from project

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Ccompatibility) `compatibility` - Global JavaScript Libraries
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib) `lib` - Developer Helpers and `semantic-ui`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-pink-dark-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Cscss) `scss` - Sass Home
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-light-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Cstylesheets) `stylesheets` - Global Css Styles

### Anticipates

_none_
