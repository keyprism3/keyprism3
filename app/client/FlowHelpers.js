/*global FlowRouter, Meteor */
export default class FlowHelpers{
  constructor(){
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.Webstorm); // !unuf - lint hack for webstorm
  }
  Webstorm(){
    this["UNUSED MEMBER\n"] && this["UNUSED MEMBER\n"](FlowHelpers.urlFor); // !unum - lint hack for webstorm
  }
  static pathFor(path, params){
    let query = params && params.query ? FlowRouter._qs.parse(params.query) : {};
    return FlowRouter.path(path, params, query);
  }
  static urlFor(path, params){
    return Meteor.absoluteUrl(this.pathFor(path, params));
  }
  static currentRoute(route) {
    FlowRouter.watchPathChange();
    return FlowRouter.current().route.name === route ? 'active' : '';
  }
}

// <a href={FlowHelpers.pathFor( 'cookies', { cookie: 'peanut-butter' } )}>To the peanut butter!</a>

// <li className={FlowHelpers.currentRoute( 'cookies' )}><a href="/cookies/peanut-butter">Cookies</a></li>

