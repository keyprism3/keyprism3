## README

`C:\_\keyprism3\app\client\scss`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-pink-dark-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Cscss) _Sass Home_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/client/scss/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-60) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_scss-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Cscss%5C_variables.scss) `_variables.scss` - Globals

### Folder Built-ins

_none_

### Anticipates

_none_
