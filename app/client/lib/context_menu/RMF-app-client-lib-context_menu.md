## RMF-app-client-lib-context_menu

`C:\_\keyprism3\app\client\lib\context_menu`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Ccontext_menu) _For `devui`_

Source [https://github.com/s-yadav/contextMenu.js](https://github.com/s-yadav/contextMenu.js)

Docs [http://ignitersworld.com/lab/contextMenu.html](http://ignitersworld.com/lab/contextMenu.html)

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/client/lib/context_menu/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-104) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Ccontext_menu%5CcontextMenu.css) `contextMenu.css` - may tweak
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Ccontext_menu%5CcontextMenu.min.js) `contextMenu.min.js` - library

### Folder Built-ins

_none_

### Anticipates

_none_
