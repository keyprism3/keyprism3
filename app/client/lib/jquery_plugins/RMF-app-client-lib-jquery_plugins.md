## RMF-app-client-lib-jquery_plugins

`C:\_\keyprism3\app\client\lib\jquery_plugins`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Cjquery_plugins) _Handmade and Open-source jQuery Plugins_

One per file

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/client/lib/jquery_plugins/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-105) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Cjquery_plugins%5Cjquery-migrate-%31.%34.%31.js) `jquery-migrate-1.4.1.js` - maybe necessary for Meteor built-in jquery to use `metadata` plugin
 - Source: [https://blog.jquery.com/2016/05/19/jquery-migrate-1-4-1-released-and-the-path-to-jquery-3-0/](https://blog.jquery.com/2016/05/19/jquery-migrate-1-4-1-released-and-the-path-to-jquery-3-0/)
 - CDN: [https://code.jquery.com/jquery-migrate-1.4.1.js](https://code.jquery.com/jquery-migrate-1.4.1.js)

<hr>

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Cjquery_plugins%5Cjquery.metadata.js) `jquery.metadata.js` - jQuery plugin for parsing metadata from elements, can use with `qtip2`
 - Source: [https://github.com/jquery-archive/jquery-metadata](https://github.com/jquery-archive/jquery-metadata)

<hr>


### Folder Built-ins

_none_

### Anticipates

- `<plugin-name>.js`
