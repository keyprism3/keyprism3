## README

`C:\_\keyprism3\app\client\lib`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib) _Developer Helpers and `semantic-ui`_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/client/lib/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-103) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_less-import-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Ccustom.bootstrap.import.less) `custom.bootstrap.import.less` - bootstrap tweaks, may not be needed we are using semantic-ui

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Ccontext_menu) `context_menu` - For `devui`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Cjquery_plugins) `jquery_plugins` - Handmade and Open-source jQuery Plugins
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Cqtip%32) `qtip2` - jQuery Tooltip Plugin for `devui` use
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Csemantic-ui) `semantic-ui` - Semantic UI Home
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-light-16.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Cstylesheets) `stylesheets` - Layouts Styling

### Anticipates

- `<library-name>`
