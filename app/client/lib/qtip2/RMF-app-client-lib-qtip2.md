## RMF-app-client-lib-qtip2

`C:\_\keyprism3\app\client\lib\qtip2`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Cqtip%32) _jQuery Tooltip Plugin for `devui` use_

Download: [http://qtip2.com/download](http://qtip2.com/download)

Guide: [http://qtip2.com/guides](http://qtip2.com/guides)

Options: [http://qtip2.com/options](http://qtip2.com/options)

Plugins: [http://qtip2.com/plugins](http://qtip2.com/plugins)

Api: [http://qtip2.com/api](http://qtip2.com/api)

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/client/lib/qtip2/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-106) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Cqtip%32%5Cjquery.qtip.js) `jquery.qtip.js` - library
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Cqtip%32%5Cjquery.qtip.min.css) `jquery.qtip.min.css` - styles

### Folder Built-ins

_none_

### Anticipates

_none_
