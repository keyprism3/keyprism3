## RMF-app-client-lib-stylesheets

`C:\_\keyprism3\app\client\lib\stylesheets`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-light-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Cstylesheets) _Layouts Styling_

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/client/lib/stylesheets/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-108) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_less-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Clib%5Cstylesheets%5Clayout.less) `layout.less` - global styles for layout

### Folder Built-ins

_none_

### Anticipates

- `<layout-name>.less`
