## README

`C:\_\keyprism3\app\client\compatibility`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-48.png)](http://localhost/aip.html?cmd=open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Ccompatibility) _Global JavaScript Libraries_

JavaScript libraries that rely on variables declared with var at the top level being exported as globals. Files in this directory are executed without being wrapped in a new variable scope. These files are executed before other client-side JavaScript files.

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/client/compatibility/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-59) - webbrain

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_txt-16.png)](http://localhost/aip.html?cmd=slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cclient%5Ccompatibility%5Cclient-compatability.txt) `client-compatability.txt` - Explanation

### Folder Built-ins

_none_

### Anticipates

- `<library>.js`
