/**
 * <h4>Javascript Built-ins Extensions</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Clib%5Cextensions%5Cextensions%2Ejs">C:\_\socialnitro-app\lib\extensions\extensions.js</a><br>
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link Meteor} - `meteor/meteor`
 * - {@link SimpleSchema}    - `meteor/aldeed:simple-schema`
 * - {@link ValidatedMethod} - `meteor/mdg:validated-method`
 * - {@link Factory}         - `meteor/dburles:factory`
 * <p>
 * <h5>NPM Dependencies</h5>
 * none
 * <p>
 * <h5>Project Dependencies</h5>
 * none
 * <p>
 * <h5>Local Dependencies</h5>
 * none
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/lib/extensions/extensions.js
 */
const lib_extensions_extensions_js= "/lib/extensions/extensions.js";

import { Meteor } from 'meteor/meteor';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { Factory } from 'meteor/dburles:factory';

const esdocIgnore = (dummy) => {return dummy;};
esdocIgnore(esdocIgnore);
esdocIgnore(lib_extensions_extensions_js);

/**
 * <h4>`Function.prototype.asObject` readonly property</h4>
 * Create Plain Object from `this` class static functions and return it<br>
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * @public
 */
const Function_prototype_asObject_descriptor = {
  get: function() {
    const exclude='length name prototype constructor'.split(' ');
    const a=Object.getOwnPropertyNames(this);
    const o = {};
    let x;
    for (x=0;x<a.length;x++) {
      const n = a[x];
      const v = this[n];
      if (exclude.indexOf(n)===-1 && 'undefined' !== typeof v) {
        o[n]=v;
      }
    }
    return o;
  }
};
Object.defineProperty(Function.prototype, "asObject", Function_prototype_asObject_descriptor);

/**
 * <h4>`Function.prototype.asValueObject` readonly property</h4>
 * Create Plain Object from `this` class instance functions and return it<br>
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * @public
 */
const Function_prototype_asValueObject_descriptor = {
  get: function() {
    const klass=this;
    const inst=new klass();
    const o={};
    for (let i in inst) {
      //noinspection JSUnfilteredForInLoop
      o[i]=inst[i];
    }
    return o;
  }
};
Object.defineProperty(Function.prototype, "asValueObject", Function_prototype_asValueObject_descriptor);

/**
 * <h4>`Function.prototype.asSchema` readonly property</h4>
 * Create Schema from `this` class instance functions and return it<br>
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * @public
 */
const Function_prototype_asSchema_descriptor = {
  get: function() {
    const klass=this;
    return new SimpleSchema(klass.asValueObject);
  }
};
Object.defineProperty(Function.prototype, "asSchema", Function_prototype_asSchema_descriptor);

/**
 * <h4>`Function.prototype.asValidator` readonly property</h4>
 * Create Schema Validator from `this` class instance functions and return its validator<br>
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * See also {@link Function_prototype_schemaValidator}
 * @public
 */
const Function_prototype_asValidator_descriptor = {
  get: function() {
    const klass=this;
    return new SimpleSchema(klass.asValueObject).validator();
  }
};
Object.defineProperty(Function.prototype, "asValidator", Function_prototype_asValidator_descriptor);

/**
 * <h4>`Function.prototype.asMethod` readonly property</h4>
 * Return new ValidatedMethod from `this` class instance
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * @public
 */
const Function_prototype_asMethod_descriptor = {
  get: function() {
    const klass=this;
    const inst=new klass();
    const o={};
    for (let i in inst) {
      //noinspection JSUnfilteredForInLoop
      o[i]=inst[i];
    }
    if ('function'===typeof inst.run&&'undefined'===typeof o.run) {
      o.run=inst.run;
    }
    if ('function'===typeof inst.validate&&'undefined'===typeof o.validate) {
      o.validate=inst.validate;
    }
    return new ValidatedMethod(o);
  }
};
Object.defineProperty(Function.prototype, "asMethod", Function_prototype_asMethod_descriptor);

/**
 * <h4>`Function.prototype.schemaValidator` function</h4>
 * Create Schema Validator from `this` class instance functions and return its validator<br>
 * <p>
 * See also {@link Function_prototype_asValidator_descriptor}
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * @public
 * @param {Object} o - validator param
 */
const Function_prototype_schemaValidator = (o={ clean: true, filter: false }) => { // esdoc hack
  const klass=this;
  return klass.asSchema.validator(o);
};
esdocIgnore(Function_prototype_schemaValidator);
const Function_prototype_schemaValidator_descriptor = {
  get: function() {
    return (o={ clean: true, filter:false })=>{
      const klass=this;
      return klass.asSchema.validator(o);
    };
  }
};
Object.defineProperty(Function.prototype, "schemaValidator", Function_prototype_schemaValidator_descriptor);

/**
 * <h4>`Function.prototype.attachSchemaToCollection` command</h4>
 * Attach `this` class static functions to collection as schema<br>
 * sets `collection.schema` <br>
 * calls `collection.attachSchema`
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * @public
 * @param {Mongo.Collection} collection
 */
const Function_prototype_attachSchemaToCollection = (collection) => {esdocIgnore(collection);}; // esdock hack
esdocIgnore(Function_prototype_attachSchemaToCollection);
const Function_prototype_attachSchemaToCollection_descriptor = {
  get: function() {
    return (collection)=>{
      const klass=this;
      collection.schema= klass.asSchema;
      collection.attachSchema(collection.schema);
    };
  }
};
Object.defineProperty(Function.prototype, "attachSchemaToCollection", Function_prototype_attachSchemaToCollection_descriptor);

/**
 * <h4>`Function.prototype.attachHelpersToCollection` command</h4>
 * Attach `this` class static functions to collection as helpers
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * @public
 * @param {Mongo.Collection} collection
 */
const Function_prototype_attachHelpersToCollection = (collection) => {esdocIgnore(collection);}; // esdoc hack
esdocIgnore(Function_prototype_attachHelpersToCollection);
const Function_prototype_attachHelpersToCollection_descriptor = {
  get: function() {
    return (collection)=>{
      const klass=this;
      collection.helpers(klass.asObject);
    };
  }
};
Object.defineProperty(Function.prototype, "attachHelpersToCollection", Function_prototype_attachHelpersToCollection_descriptor);

/**
 * <h4>`Function.prototype.definesFactoryForCollection` commands</h4>
 * Define Factory from `this` class static functions
 * <p>
 * Alias {@link Function_prototype_defineFactoryForCollection}
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * @public
 * @param {string} name - Factory name
 * @param {Mongo.Collection} collection
 */
const Function_prototype_definesFactoryForCollection = (name, collection) => {esdocIgnore(name); esdocIgnore(collection);}; // esdoc hack
esdocIgnore(Function_prototype_definesFactoryForCollection);
const Function_prototype_definesFactoryForCollection_descriptor = {
  get: function() {
    return (name, collection)=>{
      const klass=this;
      Factory.define(name, collection, klass.asObject);
    };
  }
};
Object.defineProperty(Function.prototype, "definesFactoryForCollection", Function_prototype_definesFactoryForCollection_descriptor);

/**
 * <h4>`Function.prototype.defineFactoryForCollection` command</h4>
 * Define Factory from `this` class static functions
 * <p>
 * Alias {@link Function_prototype_definesFactoryForCollection}
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * @public
 * @param {string} name - Factory name
 * @param {Mongo.Collection} collection
 */
const Function_prototype_defineFactoryForCollection = (name, collection) => {esdocIgnore(name); esdocIgnore(collection);}; // esdoc hack
esdocIgnore(Function_prototype_defineFactoryForCollection);
const Function_prototype_defineFactoryForCollection_descriptor = {
  get: function() {
    return (name, collection)=>{
      const klass=this;
      Factory.define(name, collection, klass.asObject);
    };
  }
};
Object.defineProperty(Function.prototype, "defineFactoryForCollection", Function_prototype_defineFactoryForCollection_descriptor);

/**
 * <h4>`Function.prototype.createMeteorMethods` command</h4>
 * Creates Meteor methods from `this` class static functions
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * @public
 * @todo use and make example
 */
const Function_prototype_createMeteorMethods = () =>{}; // esdoc hack
esdocIgnore(Function_prototype_createMeteorMethods);
const Function_prototype_createMeteorMethods_descriptor = {
  get: function() {
    return ()=>{
      const klass=this;
      Meteor.methods(klass.asObject);
    };
  }
};
Object.defineProperty(Function.prototype, "createMeteorMethods", Function_prototype_createMeteorMethods_descriptor);

/**
 * <h4>`Function.prototype.setPropTypesFor` command</h4>
 * Creates Prop Types from `this` class instance members and assignes them to a React Component
 * <p>
 * - file {@link lib_extensions_extensions_js} - `/lib/extensions/extensions.js`
 * @protected
 */
const Function_prototype_setPropTypesFor = (reactClass) => {esdocIgnore(reactClass);}; // esdoc hack
esdocIgnore(Function_prototype_setPropTypesFor);
const Function_prototype_setPropTypesFor_descriptor = {
  get: function() {
    return (reactClass)=>{
      const klass=this;
      reactClass.propTypes = klass.asValueObject;
    };
  }
};
Object.defineProperty(Function.prototype, "setPropTypesFor", Function_prototype_setPropTypesFor_descriptor);

///

const dummy=function(x) { return x; };
export default dummy;

//asObject
//asValueObject new
//asSchema  new
//asMethod  new
//attachSchemaToCollection(collection)
//attachHelpersToCollection(collection)
//definesFactoryForCollection(name,collection)
//createMeteorMethods()
//setPropTypesFor(class)
