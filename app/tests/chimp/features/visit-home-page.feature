@_focus
Feature: Visit Home Page

  As a visitor to Social Nitro
  I want to visit the home page
  So that I can browse the website
  @_watch
  Scenario: Page Load
  Given I visit the home page
  Then I should see a way to create an account
  And I should see a way to log in with twitter
  And I should see a way to log in with facebook
  And I should see a way to login with email and password
  And I should see a way to recover my password
  And I should see the page title is "Social Nitro - The Ultimate Tool for Social Media Management"
