@_focus
Feature: Sign up

  As a visitor to Social Nitro
  I want to sign up
  So that I can use the website

  @_watch
  Scenario: From Home Page
    Given I visit the home page
    Then I chose to create an account
    And I should be directed to a signup page
    And I should see a way to signup with twitter
    And I should see a way to signup with facebook
    And I should see a way to signup with email and password
    And I should see a way to go to the login page
    And I should see the page title is "Social Nitro - The Ultimate Tool for Social Media Management"
