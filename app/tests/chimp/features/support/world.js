/* globals browser server expect */
//noinspection JSUnusedLocalSymbols
const fiveSecs=5000;
//noinspection JSUnusedLocalSymbols
const tenSecs=10000;
//noinspection JSUnusedLocalSymbols
const twentySecs=20000;

//String.prototype.q=
if (!String.prototype.fwdslash) {
  Object.defineProperty(String.prototype, 'fwdslash', {
    get:function() {
      let s=''+this;
      s=s.replace(/\\/g, '/');
      return s;
    }
  });
  Object.defineProperty(String.prototype, 'fwdslash0', {
    get:function() {
      let s=''+this;
      s=s.replace(/\\/g, '/');
      return s+':0';
    }
  });
}
if (!String.prototype.q) {
  Object.defineProperty(String.prototype, 'q', {
    get:function() {
      let s=''+this;
      return $(s);
    }
  });
}
if (!String.prototype.afterHas) {
  Object.defineProperty(String.prototype, 'afterHas', {
    get:function() {
      let s=''+this;
      browser.waitForExist(s, tenSecs);
      return true;
    }
  });
}
console.log("WORLD");
//console.log('\u0007');
const beep = require('node-beep');
beep(3);
//const beep = require('beepbeep');

//beep();
const World= function World() {
};
const WP=World.prototype;
WP.hostRoot='http://localhost:3000/';
WP.newUserEmail='mindprism@gmail.com';
WP.fiveSecs=fiveSecs;
WP.tenSecs=tenSecs;
WP.twentySecs=twentySecs;

WP.test=function() {
  //return 'test '+(typeof this.browser);
  return 'test '+(typeof browser);
};
WP.stringFromObj=
  function stringFromObj(o) {
    if ('object' === typeof o) {
      return o.value;
    }
    if ('string' === typeof o) {
      return o;
    }
  };
WP.waitNavUrl=
  function waitNavUrl(path) {
    if (path === undefined) {
      //noinspection AssignmentToFunctionParameterJS
      path = '';
    }
    browser.url(this.hostRoot+path);
    this.waitUrl(path);
  };
WP.wait1Second=
  function wait1Second() {
    //noinspection MagicNumberJS
    browser.wait(1000);
  };
WP.wait2Seconds=
  function wait2Seconds() {
    //noinspection MagicNumberJS
    browser.wait(2000);
  };
WP.wait5Seconds=
  function wait5Seconds() {
    //noinspection MagicNumberJS
    browser.wait(5000);
  };
WP.afterHas=
  function afterHas(selector) {
    //noinspection MagicNumberJS
    browser.waitForExist(selector);
  };
WP.expectSelectorLabel=
  function expectSelectorLabel(selector, label) {
    browser.waitForExist(selector);
    const txt = browser.getText(selector);
    expect(txt).toBe(label);
  };
WP.expectUrl=
  function expectUrl(path) {
    if (path === undefined) {
      //noinspection AssignmentToFunctionParameterJS
      path = '';
    }
    //noinspection JSUnresolvedVariable
    expect(browser.url().value).toBe(this.hostRoot + path);
  };
WP.waitUrl=
  function waitUrl(path, ms = tenSecs) {
    if (path === undefined) {
      //noinspection AssignmentToFunctionParameterJS
      path = '';
    }
    const that=this;
    //noinspection MagicNumberJS
    browser.waitUntil(function () {
      return that.hostRoot+path === browser.getUrl();
    }, ms, 'expected browser.url to be '+this.hostRoot+path+' '+(ms/1000)+'s');
  };
WP.waitClick=
  function waitClick(sel, ms = tenSecs) {
    browser.waitForExist(sel, ms);
    browser.click(sel);
  };
WP.waitServer=
  function waitServer(val, ms = tenSecs, ...args) {
    //noinspection MagicNumberJS
    browser.waitUntil(function () {
      return server.call(...args) === val;
    }, ms, 'expected server.call("'+args+'") to be '+val+' after '+(ms/1000)+'ms');
  };
WP.expectServer=
  function expectServer(val, ...args) {
    //noinspection JSUnresolvedFunction
    expect(server.call(...args)).toBe(val);
  };
WP.serverCall=
  function serverCall(...args) {
    return server.call(...args);
  };
WP.waitForExist=
  function waitForExist(selector, ms, reverse) {
    return browser.waitForExist(selector, ms, reverse);
  };
WP.expectTitle=
  function expectTitle(title) {
    //expect(this.stringFromObj(browser.title())).toBe(title);
    expect(this.stringFromObj(browser.getTitle())).toBe(title);
  };
////////
module.exports = function() {
  this.World = World;
};
//module.exprexport default World;
