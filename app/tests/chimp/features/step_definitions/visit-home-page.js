/* globals describe xdescribe context specify it xit expect assert demand insist before after beforeEach afterEach sinon mochajsdom browser document window navigator server server.call JsDom React Reactor Shallow Mount Render */ // eslint-disable-line no-unused-vars
//noinspection FunctionTooLongJS
module.exports = function() {
  //
  this.World = require('../support/world').World;
  //noinspection JSUnresolvedFunction
  this.BeforeFeatures(function() {
    //this.serverCall('fixtures-logout'); //seems ok maybe to slow?
    //console.log("before features");
  });
  //noinspection JSUnresolvedFunction
  this.AfterFeatures(function() {
    //console.log("after features");
  });

  // 1) Scenario: Page Load - tests\chimp\features\visit-home-page.feature:9
  // Step: Given I visit the home page - tests\chimp\features\visit-home-page.feature:10
  this.Given(/^I visit the home page$/, function () {
    this.serverCall('fixtures-logout'); //seems ok maybe to slow??
    this.waitNavUrl();
    //browser.url(this.hostRoot);
    //this.expectUrl();
     //expect(server.call('fixtures-countUsers')).toBe(1);
  });

  // 2) Scenario: Page Load - tests\chimp\features\visit-home-page.feature:9
  // Step: Then I should see a way to create an account - tests\chimp\features\visit-home-page.feature:11
  this.Then(/^I should see a way to create an account$/, function () {
    const selector = 'a[href="/signup"]';
    const label = 'Sign Up';
    this.expectSelectorLabel(selector, label);
  });

  // 3) Scenario: Page Load - tests\chimp\features\visit-home-page.feature:9
  // Step: And I should see a way to log in with twitter - tests\chimp\features\visit-home-page.feature:12
  this.Then(/^I should see a way to log in with twitter$/, function () {
    const selector = 'button.ui.twitter';
    const label = 'Log in with Twitter';
    this.expectSelectorLabel(selector, label);
  });

  // 4) Scenario: Page Load - tests\chimp\features\visit-home-page.feature:9
  // Step: And I should see a way to log in with facebook - tests\chimp\features\visit-home-page.feature:13
  this.Then(/^I should see a way to log in with facebook$/, function () {
    const selector = 'button.ui.facebook';
    const label = 'Log in with Facebook';
    this.expectSelectorLabel(selector, label);
  });

  // 5) Scenario: Page Load - tests\chimp\features\visit-home-page.feature:9
  // Step: And I should see a way to login with email and password - tests\chimp\features\visit-home-page.feature:14
  this.Then(/^I should see a way to login with email and password$/, function () {
    //this.afterHas('input[type=email]');
    'input[type=email]'.afterHas();
    this.afterHas('input[type=password]');
    const selector = 'button[type=submit]';
    const label = 'Log In';
    this.expectSelectorLabel(selector, label);
  });

  // 6) Scenario: Page Load - tests\chimp\features\visit-home-page.feature:9
  // Step: And I should see a way to recover my password - tests\chimp\features\visit-home-page.feature:15
  this.Then(/^I should see a way to recover my password$/, function () {
    const selector = 'a[href="/forgot-password"]';
    const label = 'Forgot password?';
    this.expectSelectorLabel(selector, label);
  });

  // 7) Scenario: Page Load - tests\chimp\features\visit-home-page.feature:9
  // Step: And I should see the page title is "Social Nitro" - tests\chimp\features\visit-home-page.feature:16
  this.Then(/^I should see the page title is "([^"]*)"$/, function (title) {
    //noinspection MagicNumberJS
    this.wait1Second();
    //noinspection JSUnresolvedFunction
    this.expectTitle(title);
  });
};
