/* globals describe xdescribe context specify it xit expect assert demand insist before after beforeEach afterEach sinon mochajsdom browser document window navigator server JsDom React Reactor Shallow Mount Render */ // eslint-disable-line no-unused-vars
//noinspection FunctionTooLongJS
module.exports = function() {
  this.World = require('../support/world').World;
  console.log(__filename.fwdslash0);
  //noinspection JSUnresolvedFunction
  this.BeforeFeatures(function() {
    console.log("before features");
    console.log('C:/_/socialnitro-app/tests/chimp/features/step_definitions/new-user-signup-with-email.js:49:18');
    console.log('C:/_/socialnitro-app/tests/chimp/features/step_definitions/new-user-signup-with-email.js:0');
    //console.log('/tests/chimp/features/step_definitions/new-user-signup-with-email.js:0');
    console.log('__filename:', __filename.fwdslash);
  });
  //noinspection JSUnresolvedFunction
  this.AfterFeatures(function() {
    console.log("after features");
  });
  ////
  this.Given(/^I am a new user$/, function() {
    console.log(this.test());
    //noinspection JSUnresolvedFunction
    this.serverCall('fixtures-removeNewUser');// ok123
    this.waitServer(false, this.tenSecs, 'fixtures-hasNewUser');
    //console.log('h.test()',h.test());
  });
  this.Then(/^by providing my information$/, function() {
    const emailInput=browser.element('#ls-field-email');
    const passwordInput=browser.element('#ls-field-password');
    const passwordMatchInput=browser.element('#ls-field-password_again');
    const signupButton=browser.element('button[type="submit"]');
    const email=this.newUserEmail;
    const pass='password';  ////
    emailInput.setValue(email);
    passwordInput.setValue(pass);
    passwordMatchInput.setValue(pass);
    signupButton.click();
  });
  this.Then(/^a new account should be created if the email does not exist$/, function() {
    this.waitServer(true, this.tenSecs, 'fixtures-hasNewUser');
  });
  this.Then(/^I should be sent a welcome email$/, function () {
    this.waitServer(true, this.twentySecs, 'fixtures-isNewUserEmailSent');
  });
  this.Then(/^I should be logged in$/, function() {
    this.waitServer(true, undefined, 'fixtures-isEmailLoggedIn', this.newUserEmail);
  });
  this.Then(/^I should be taken to the start url$/, function() {
    this.waitUrl('start');
  });
};
