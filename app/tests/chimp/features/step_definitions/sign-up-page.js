/* globals describe xdescribe context specify it xit expect assert demand insist before after beforeEach afterEach sinon mochajsdom browser document window navigator server JsDom React Reactor Shallow Mount Render */ // eslint-disable-line no-unused-vars
//noinspection FunctionTooLongJS
module.exports = function() {
  this.World = require('../support/world').World;
  //noinspection JSUnresolvedFunction
  this.BeforeFeatures(function() {
    //console.log("before features");
  });
  //noinspection JSUnresolvedFunction
  this.AfterFeatures(function() {
    //console.log("after features");
  });

  // 1) Scenario: From Home Page - tests\chimp\features\sign-up-page.feature:9
  // Step: Then I chose to create an account - tests\chimp\features\sign-up-page.feature:11
  this.Then(/^I chose to create an account$/, function () {
    this.waitClick('a[href="/signup"]');
  });

  // 2) Scenario: From Home Page - tests\chimp\features\sign-up-page.feature:9
  // Step: And I should be directed to a signup page - tests\chimp\features\sign-up-page.feature:12
  this.Then(/^I should be directed to a signup page$/, function () {
    this.waitUrl('signup');
  });

  // 3) Scenario: From Home Page - tests\chimp\features\sign-up-page.feature:9
  // Step: And I should see a way to signup with twitter - tests\chimp\features\sign-up-page.feature:13
  this.Then(/^I should see a way to signup with twitter$/, function () {
    const selector = 'button.ui.twitter';
    const label = 'Sign up with Twitter';
    this.expectSelectorLabel(selector, label);
  });

  // 4) Scenario: From Home Page - tests\chimp\features\sign-up-page.feature:9
  // Step: And I should see a way to signup with facebook - tests\chimp\features\sign-up-page.feature:14
  this.Then(/^I should see a way to signup with facebook$/, function () {
    const selector = 'button.ui.facebook';
    const label = 'Sign up with Facebook';
    this.expectSelectorLabel(selector, label);
  });

  // 5) Scenario: From Home Page - tests\chimp\features\sign-up-page.feature:9
  // Step: And I should see a way to signup with email and password - tests\chimp\features\sign-up-page.feature:15
  this.Then(/^I should see a way to signup with email and password$/, function () {
    this.afterHas('input[type=email]');
    this.afterHas('input[placeholder=Password]');
    this.afterHas('input[placeholder="Password (again)"]');
    const selector = 'button[type=submit]';
    const label = 'Sign Up';
    this.expectSelectorLabel(selector, label);
  });

  // 6) Scenario: From Home Page - tests\chimp\features\sign-up-page.feature:9
  // Step: And I should see a way to go to the login page - tests\chimp\features\sign-up-page.feature:16
  this.Then(/^I should see a way to go to the login page$/, function () {
    const selector = 'a[href="/login"]';
    const label = 'Log In';    //
    this.expectSelectorLabel(selector, label);
  });
};
