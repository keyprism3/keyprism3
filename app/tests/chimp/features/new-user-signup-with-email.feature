@focus
Feature: New User Signup with Email

  As a new user of Social Nitro
  I want to visit the home page
  So that I can signup to the website
  @watch
  Scenario: Page Load non-existant user
    Given I am a new user
    Given I visit the home page
    Then I chose to create an account
    And I should be directed to a signup page
    And I should see a way to signup with email and password
    And by providing my information
    And a new account should be created if the email does not exist
    And I should be sent a welcome email
    And I should be logged in
    And I should be taken to the start url
