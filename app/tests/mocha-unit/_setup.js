/**
 * <h4>`mocha-unit` asserts and environment</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Ctests%5Cmocha%2Dunit%5C%5Fsetup%2Ejs">C:\_\socialnitro-app\tests\mocha-unit\_setup.js</a><br>
 * <p>
 * <h5>NPM Dependencies</h5>
 * - {@link chai} - `chai@^3.5.0`
 * - `chai-enzyme@^0.6.1`
 * - `chai-string@^1.3.0`
 * - `enzyme@^2.7.1`
 * - `jsdom@^9.9.1`
 * - `mocha@latest`
 * - `mocha-jsdom@^1.1.0`
 * - `react-addons-test-utils@^15.4.2`
 * - `should@^11.1.2`
 * - {@link sinon} - `sinon@^1.17.7`
 * - {@link unexpected} - `unexpected^10.23.0`
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}
 * <p>
 * <h5>Creates</h5>
 * - {@link expect} - chai
 * - {@link assert} - chai
 * - {@link insist} - chai should
 * - `.must` - chai `.should`
 * - {@link demand} - unexpected
 * - {@link sinon}
 * - {@link JsDom}
 * - {@link mochajsdom}
 * - `document` - `jsdom('')`
 * - {@link enzyme}
 * - {@link React}
 * - {@link Reactor}
 * - {@link Shallow}
 * - {@link Mount}
 * - {@link Render}
 * <h5>Packages</h5>
 * - <a href="https://cucumber.io/docs/reference">Cucumber</a>
 * - <a href="http://webdriver.io/api.html">WebdriverIO</a>
 * - <a href="https://mochajs.org/#usage">Mocha</a>
 * - <a href="https://jasmine.github.io/edge/introduction.html">Jasmine</a>
 * - <a href="http://chaijs.com/">Chai</a>
 * - <a href="https://github.com/producthunt/chai-enzyme">Chai-Enzyme</a>
 * - <a href="http://chaijs.com/plugins/chai-string/">Chai-String</a>
 * - <a href="http://unexpected.js.org/">Unexpected</a>
 * - <a href="https://github.com/tmpvar/jsdom">JsDom</a>
 * - <a href="https://www.npmjs.com/package/mocha-jsdom">MochaJsDom</a>
 * - <a href="https://facebook.github.io/react/docs/hello-world.html">React</a>
 * - <a href="https://facebook.github.io/react/docs/test-utils.html">Reactor</a>
 * - <a href="http://airbnb.io/enzyme/docs/api/">Enzyme</a>
 * - <a href="http://sinonjs.org/docs/">Sinon</a>
 * <p>
 * <h5>Notes</h5>
 * __Mocha__<br>
 * <p>
 * __Mocha-BDD__<br>
 * - `describe()`, `context()`
 * - `it()`, `specify()`
 * - `before()`, `after()`
 * - `beforeEach()`, `afterEach()`
 * <p>
 * __Mocha-TDD__<br>
 * - `suite()`
 * - `test()`
 * - `suiteSetup()`, `suiteTeardown()`
 * - `setup()`, `teardown()`
 * <p>
 * __Jasmine__<br>
 * `expect()` and `assert()` are Jasmine<br>
 * <p>
 * __Chai__<br>
 * `expect()` is chai expect<br>
 * `assert()` is chai assert<br>
 * chai `.should`<br>
 * `insist()` is `should()`<br>
 * `.must` is chai `insist()`, ie chai `should()` <br>
 * <p>
 * __Chai-Enzyme__<br>
 * - `.checked()`
 * - `.className(str)`
 * - `.contain(node)`
 * - `.decendants(selector)`
 * - `.exactly()`
 * - `.disabled()`
 * - `.blank()`
 * - `.present()`
 * - `.html(str)`
 * - `.id(str)`
 * - `.match(selector)`
 * - `.ref(key)`
 * - `.selected()`
 * - `.tagName(str)`
 * - `.text(str)`
 * - `.type(str)`
 * - `.value(str)`
 * - `.attr(key, [val])`
 * - `.data(key, [val])`
 * - `.style(key, [val])`
 * - `.state(key, [val])`
 * - `.prop(key, [val])`
 * - `.props(key, [val])`
 * <p>
 * __Chai-String__<br>
 * - `.startsWith(str)`
 * - `.endsWith(str)`
 * - `.equalIgnoreCase(str)`
 * - `.equalIgnoreSpaces(str)`
 * - `.singleLine()`
 * - `.reverseOf(str)`
 * - `.palindrome(str)`
 * - `.entriesCount(str, strfind, count)`
 * - `.indexOf(str, start)`
 * <p>
 * __Unexpected__<br>
 * `demand()` is `unexpected()`<br>
 * <p>
 * __JsDom__<br>
 * `JsDom` is `jsdom` <br>
 * `document` is `jsdom('')` <br>
 * `window` is `document.window` <br>
 * <p>
 * __MochaJsDom__<br>
 * `mochajsdom` is `mocha-jsdom`<br>
 * <p>
 * __React__<br>
 * `React` is react <br>
 * <p>
 * __Reactor__<br>
 * `Reactor` is `react-addons-test-utils`<br>
 * <p>
 * __Sinon__<br>
 * `sinon` is sinon <br>
 * <br>
 * __See Also__<br>
 * {@link tests_mocha_unit__asserts_js}
 * <br>
 * __Is Like__<br>
 * {@link tests_mocha_bdd__setup_js}
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/tests/mocha-unit/_setup.js
 */
const tests_mocha_unit__setup_js= "/tests/mocha-unit/_setup.js";
global.UNUSED = tests_mocha_unit__setup_js;

//noinspection ES6UnusedImports
import * as chai from 'chai';
// FOR SETUP
import { jsdom } from "jsdom"; // POSITION IMPORTANT
//import * as chaiEnzyme from 'chai-enzyme'; // WRONG
chai.use(require('chai-enzyme')());
chai.use(require('chai-string'));
// FOR NORMAL
global.expect=chai.expect;
global.assert=chai.assert;
chai.should();

global.insist= require('should/as-function');//should;
if ('undefined' === typeof Object.prototype.must) {
  Object.defineProperty(Object.prototype, 'must', { get:function() { return global.insist(this); }, isEnumerable:false });
}

//noinspection JSAnnotator
global.demand = require('unexpected');

// FOR SETUP
global.JsDom = jsdom;
global.mochajsdom = require('mocha-jsdom');

const exposedProperties = ['window', 'navigator', 'document'];

global.document = jsdom('');
global.window = document.defaultView;
Object.keys(document.defaultView).forEach((property) => {
  if ('undefined' === typeof global[property]) {
    exposedProperties.push(property);
    global[property] = document.defaultView[property];
  }
});

global.navigator = {
  userAgent: 'node.js'
};

global.React = require('react');
//const documentRef = document;
global.Reactor = require('react-addons-test-utils');
//noinspection ES6UnusedImports
const enzyme= require('enzyme');
global.Shallow=enzyme.shallow;
global.Mount=enzyme.mount;
global.Render=enzyme.render;
global.sinon = require('sinon');

///////// FINAL
