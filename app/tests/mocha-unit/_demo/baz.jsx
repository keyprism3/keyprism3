/* globals */

/// NO ESDOC
import React from 'react';
import * as ReactDOM from 'react-dom';
//noinspection JSUnusedGlobalSymbols,JSClassNamingConvention
export default class Baz extends React.Component {
  static displayName = 'Baz';
  static propTypes = {};
  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
  }

  componentDidMount() {
    let node = ReactDOM.findDOMNode(this);
    this && this["UNUSED LOCAL\n"] && this["UNUSED LOCAL\n"](node); // !unul - lint hack for webstorm
    /// DevUi.React.onComponentDidMount(node, {
    ///   zdot:'',
    ///   cls:'Baz',
    ///   file:'baz.jsx',
    ///   container:{
    ///     zdot:'',
    ///     cls:'bazContainer',
    ///     file:'baz-container.jsx'
    ///   }
    /// });
  }

  componentWillReceiveProps(nextProps) {
    this && this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps); // !unup - lint hack for webstorm
  }

  shouldComponentUpdate(nextProps, nextState) {
    this && this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps, nextState); // !unup - lint hack for webstorm
    return true;
  }

  componentWillUpdate(nextProps, nextState) {
    this && this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](nextProps, nextState); // !unup - lint hack for webstorm
  }

  componentDidUpdate(prevProps, prevState) {
    this && this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](prevProps, prevState); // !unup - lint hack for webstorm
  }

  componentWillUnmount() {
  }

  render() {
    return (
      <div className="Baz" data-react-class="Baz" data-react-file="baz.jsx">
        <p>Baz:baz.jsx</p>
      </div>
    );
  }
}
