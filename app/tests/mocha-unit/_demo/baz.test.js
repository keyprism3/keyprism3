/* globals suite xsuite test xtest suiteSetup xsuiteSetup suiteTeardown xsuiteTeardown setup xsetup teardown xteardown expect assert demand insist sinon mochajsdom browser document window navigator server JsDom React Reactor Shallow Mount Render */ // eslint-disable-line no-unused-vars

/// NO ESDOC

// Meteor Mocha Unit Test JS File - ins m

// PACKAGES
  // Mocha
  // https://mochajs.org/#usage
  // ** The TDD interface provides suite(), test(), suiteSetup(), suiteTeardown(), setup(), and teardown()
  // The BDD interface provides describe(), context(), it(), specify(), before(), after(), beforeEach(), and afterEach().
  //
  // Chai
  // http://chaijs.com/
  // expect is chai expect
  // assert is chai assert
  // chai .should
  // insist is chai insist()
  // .must is chai insist, should
  //
  // Unexpected
  // http://unexpected.js.org/
  // demand is unexpected
  //
  // JsDom
  // https://github.com/tmpvar/jsdom
  // JsDom is jsdom
  // document is jsdom('')
  // window is document.window
  //
  // MochaJsDom
  // https://www.npmjs.com/package/mocha-jsdom
  // mochajsdom is mocha-jsdom
  //
  // React
  // https://facebook.github.io/react/docs/hello-world.html
  // React is react
  //
  // Reactor
  // https://facebook.github.io/react/docs/test-utils.html
  // Reactor is react-addons-test-utils
  //
  // Enzyme
  // http://airbnb.io/enzyme/docs/api/
  // enzyme is enzyme //no ref needed
  // Shallow is enzyme.shallow
  // Mount is enzyme.mount
  // Render is enzyme.render
  //
  // Sinon
  // http://sinonjs.org/docs/
  // sinon is sinon

// use tdd and --compilers js:babel-register

// template = baz
// uname = Baz
// fileNameWithoutExtension = baz

import '../_setup.js';
import Baz from './baz';

suite('Testing my div with jsdom', function() {
  JsDom({ skipWindowCheck: true });

  //noinspection JSCheckFunctionSignatures
  test('should contain text: Baz:baz.jsx', function(done) {

    const theBaz = Reactor.renderIntoDocument(<Baz />);
    const divText = Reactor.findRenderedDOMComponentWithTag(theBaz, 'p');

    assert.equal(divText.textContent, 'Baz:baz.jsx');
    done();
  });
});

suite("A suite using enzyme", function() {
  test("contains spec with an expectation", function() {
    expect(Shallow(<Baz />).contains(<p>Baz:baz.jsx</p>)).to.equal(true);
  });

  test("contains spec with an expectation", function() {
    expect(Shallow(<Baz />).is('[data-react-class="Baz"]')).to.equal(true);
    Shallow(<Baz />).should.have.className('Baz');//chai should+ chai-enzyme
  });

  test("contains spec with an expectation", function() {
    expect(Mount(<Baz />).find('p').length).to.equal(1);
  });
  // it('should have an image to display the gravatar', function () {
  //   const wrapper = shallow(<Avatar/>);
  //   expect(wrapper.find('img')).to.have.length(1);
  // });
  //
  // it('should have props for email and src', function () {
  //   const wrapper = shallow(<Avatar/>);
  //   expect(wrapper.props().email).to.be.defined;
  //   expect(wrapper.props().src).to.be.defined;
  // });
  //
  // it('should have an initial email state', function () {
  //   const wrapper = mount(<Gravatar/>);
  //   expect(wrapper.state().email).to.equal('someone@example.com');
  // });
  //
  // it('should update the src state on clicking fetch', function () {
  //   const wrapper = mount(<Gravatar/>);
  //   wrapper.setState({ email: 'hello@ifelse.io' });
  //   wrapper.find('button').simulate('click');
  //   expect(wrapper.state('email')).to.equal('hello@ifelse.io');
  //   expect(wrapper.state('src')).to.equal(`http://gravatar.com/avatar/${md5('markthethomas@gmail.com')}?s=200`);
  // });
  //
  // it('calls componentDidMount', () => {
  //   const wrapper = mount(<Foo />);
  //   expect(Foo.prototype.componentDidMount.calledOnce).to.equal(true);
  // });
});
