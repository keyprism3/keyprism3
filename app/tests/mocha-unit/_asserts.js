/**
 * <h4>`mocha-unit` asserts</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Ctests%5Cmocha%2Dunit%5C%5Fasserts%2Ejs">C:\_\socialnitro-app\tests\mocha-unit\_asserts.js</a><br>
 * <p>
 * <h5>NPM Dependencies</h5>
 * - {@link chai} - `chai@^3.5.0`
 * - `chai-enzyme@^0.6.1`
 * - `chai-string@^1.3.0`
 * - `mocha@latest`
 * - `react-addons-test-utils@^15.4.2`
 * - `should@^11.1.2`
 * - {@link sinon} - `sinon@^1.17.7`
 * - {@link unexpected} - `unexpected^10.23.0`
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}
 * <p>
 * <h5>Creates</h5>
 * - {@link expect} - chai
 * - {@link assert} - chai
 * - {@link insist} - chai should
 * - `.must` - chai `.should`
 * - {@link demand} - unexpected
 * <h5>Packages</h5>
 * - <a href="https://cucumber.io/docs/reference">Cucumber</a>
 * - <a href="http://webdriver.io/api.html">WebdriverIO</a>
 * - <a href="https://mochajs.org/#usage">Mocha</a>
 * - <a href="https://jasmine.github.io/edge/introduction.html">Jasmine</a>
 * - <a href="http://chaijs.com/">Chai</a>
 * - <a href="http://unexpected.js.org/">Unexpected</a>
 * - <a href="https://github.com/tmpvar/jsdom">JsDom</a>
 * - <a href="https://www.npmjs.com/package/mocha-jsdom">MochaJsDom</a>
 * - <a href="https://facebook.github.io/react/docs/hello-world.html">React</a>
 * - <a href="https://facebook.github.io/react/docs/test-utils.html">Reactor</a>
 * - <a href="http://airbnb.io/enzyme/docs/api/">Enzyme</a>
 * - <a href="http://sinonjs.org/docs/">Sinon</a>
 * <p>
 * <h5>Notes</h5>
 * __Mocha__<br>
 * <p>
 * __Mocha-BDD__<br>
 * - `describe()`, `context()`
 * - `it()`, `specify()`
 * - `before()`, `after()`
 * - `beforeEach()`, `afterEach()`
 * <p>
 * __Mocha-TDD__<br>
 * - `suite()`
 * - `test()`
 * - `suiteSetup()`, `suiteTeardown()`
 * - `setup()`, `teardown()`
 * <p>
 * __Jasmine__<br>
 * `expect()` and `assert()` are Jasmine<br>
 * <p>
 * __Chai__<br>
 * `expect()` is chai expect<br>
 * `assert()` is chai assert<br>
 * chai `.should`<br>
 * `insist()` is `should()`<br>
 * `.must` is chai `insist()`, ie chai `should()` <br>
 * <p>
 * __Unexpected__<br>
 * `demand()` is `unexpected()`<br>
 * <p>
 * __JsDom__<br>
 * `JsDom` is `jsdom` <br>
 * `document` is `jsdom('')` <br>
 * `window` is `document.window` <br>
 * <p>
 * __MochaJsDom__<br>
 * `mochajsdom` is `mocha-jsdom`<br>
 * <p>
 * __React__<br>
 * `React` is react <br>
 * <p>
 * __Reactor__<br>
 * `Reactor` is `react-addons-test-utils`<br>
 * <p>
 * __Sinon__<br>
 * `sinon` is sinon <br>
 * <br>
 * __See Also__<br>
 * {@link tests_mocha_unit__setup_js}
 * <br>
 * __Is Like__<br>
 * {@link tests_mocha_bdd__asserts_js}
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/tests/mocha-unit/_asserts.js
 */
const tests_mocha_unit__asserts_js= "/tests/mocha-unit/_asserts.js";
global.UNUSED = tests_mocha_unit__asserts_js;

//noinspection ES6UnusedImports
import * as chai from 'chai';
global.expect=chai.expect;
global.assert=chai.assert;
chai.should();

global.insist= require('should/as-function');//should;
if ('undefined'===typeof Object.prototype.must) {
  Object.defineProperty(Object.prototype, 'must', { get:function() { return global.insist(this); }, isEnumerable:false });
}
//noinspection JSAnnotator
global.demand = require('unexpected');

///////// FINAL
