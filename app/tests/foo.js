/**
 * Created by Administrator on 1/21/2017.
 */

import { describe, it, beforeEach } from 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';
import Foo from '../imports/startup/client/modules/foo/foo';

let Meteor;
//noinspection JSCheckFunctionSignatures
beforeEach("before", function(done) {
  //noinspection JSUnresolvedFunction
  Meteor = {
    call: sinon.spy()
  };
  done();
});

describe("Foo", () => {
  //noinspection JSCheckFunctionSignatures
  it("returns woo", function(done) {
    let foo = new Foo(Meteor);
    let bar = foo.bar();
    expect(bar).to.equal("woo!");
    expect(bar).to.equal("woo!");
    done();
  });
  
});

