/* globals describe xdescribe context specify it xit expect assert demand insist before after beforeEach afterEach sinon mochajsdom browser document window navigator server JsDom React Reactor Shallow Mount Render */ // eslint-disable-line no-unused-vars

// https://mochajs.org/#usage
// The BDD interface provides describe(), context(), it(), specify(), before(), after(), beforeEach(), and afterEach().
import '../_asserts.js';
import isValidSs from './is-valid-ss';

describe("Testing IsValidSs(ss)", ()=>{
  function noop() {}
  noop(noop, isValidSs);
  // p1
  // it("should throw on undefined input", function(done) {
  //   expect(isValidSs).to.throw();
  //   done();
  // });
  // p1
  // it("should throw on non-string input", function(done) {
  //   expect(isValidSs.bind(null, null)).to.throw();
  //   done();
  // });
  // p2
  // it("should return true or false on string input", function(done) {
  //   insist(isValidSs('')).be.a.Boolean();
  //   done();
  // });
  // p3
  // it("should return false when input length is not 11", function(done) {
  //   noop([
  //     expect(false).to.be.false
  //     , expect(isValidSs('')).to.be.false      // expect is chai
  //     , insist(isValidSs('')).be.false         //insist is should
  //     , isValidSs('').must.be.false            //must is insist is should
  //     , demand(isValidSs(''), 'to equal', false) // demand is unexpected
  //   ]);
  //   done();
  // });
  // p4
  // it("should return false when input has invalid characters", function(done) {
  //   noop([
  //     expect(isValidSs('aaaaaaaaaaa')).to.be.false      // expect is chai
  //     , expect(isValidSs(')))))))))))')).to.be.false
  //     , expect(isValidSs('000a0000000')).to.be.false
  //   ]);
  //   done();
  // });
  // p5
  // it("should return false when dashes in bad positions", function(done) {
  //   noop([
  //     expect(isValidSs('-9999999999')).to.be.false      // expect is chai
  //     , expect(isValidSs('999-99-9999')).to.be.true
  //     , expect(isValidSs('00000000000')).to.be.false
  //   ]);
  //   done();
  // });
});

