/* globals describe xdescribe context xcontext specify xspecify it xit before xbefore after xafter beforeEach xbeforeEach afterEach xafterEach expect assert demand insist sinon mochajsdom browser document window navigator server JsDom React Reactor Shallow Mount Render */ // eslint-disable-line no-unused-vars

// https://mochajs.org/#usage
// The BDD interface provides describe(), context(), it(), specify(), before(), after(), beforeEach(), and afterEach().
import '../_setup.js';
import Reactor from 'react-addons-test-utils';

import subject from './subject';
import Baz from '../../mocha-unit/_demo/baz';

describe("subject", () => {
  //noinspection JSCheckFunctionSignatures
  it("returns 'subject'", function(done) {
    let res=subject();
    expect(res).to.equal("subject");
    insist(5).be.exactly(5).and.be.a.Number();
    demand(1, 'to be', 1);
    (5).must.be.exactly(5).and.be.a.Number();
    (5).should.equal(5);
    //(6).should.equal(true);
    done();
  });
});
xdescribe('Testing my div with jsdom', function() {
  JsDom({ skipWindowCheck: true });

  //noinspection JSCheckFunctionSignatures
  it('should contain text: Baz:baz.jsx', function(done) {
    //var VeryFirstDiv = require('../imports/ui/components/baz/baz.jsx');

    const theBaz = Reactor.renderIntoDocument(<Baz />);
    const divText = Reactor.findRenderedDOMComponentWithTag(theBaz, 'p');

    assert.equal(divText.textContent, 'Baz:baz.jsx');
    done();
  });
});

xdescribe("A suite using enzyme", function() {
  it("contains spec with an expectation", function() {
    expect(Shallow(<Baz />).contains(<p>Baz:baz.jsx</p>)).to.equal(true);
  });

  it("contains spec with an expectation", function() {
    expect(Shallow(<Baz />).is('[data-react-class="Baz"]')).to.equal(true);
  });

  it("contains spec with an expectation", function() {
    expect(Mount(<Baz />).find('p').length).to.equal(1);
  });
});
