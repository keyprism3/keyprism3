/* global debug */
/**
 * <h4>Meta control point for developer services</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cboth%5Cctrl%5Cctrl%2Ejs">C:\_\socialnitro-app\both\ctrl\ctrl.js</a><br>
 * - {@link Ctrl}
 * <p>
 * Offers debug, logging, and flags
 * <p>
 * <h5>Meteor Dependencies</h5>
 * none
 * <p>
 * <h5>External Dependencies</h5>
 * - <a href="https://github.com/stacktracejs/stacktrace.js" target="_blank">StackTrace</a> - `'meteor/peerlibrary:stacktrace'`
 * - {@link TraceX}     - `'meteor/arsnebula:tracex'`
 * - <a href="https://atmospherejs.com/robodo/meteor-debug">debug</a>      - `'meteor/robodo:meteor-debug'`
 * - {@link debug}     - `'meteor/robodo:meteor-debug'`
 * <p>
 * <h5>External Dependencies</h5>
 * - {@link TraceX_init} - `'../tracex/tracex'`
 * <p>
 * <h5>Creates</h5>
 * - {@link Ctrl}
 * - {@link StackTrace_}
 * <p>
 * <h5>Exports</h5>
 * - {@link Ctrl}
 * - {@link TraceX_init}
 * <p>
 * @public
 * @version 0.0.1
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/both/ctrl/ctrl.js
 * @todo nada
 */
const both_ctrl_ctrl_js= "/both/ctrl/ctrl.js";

import { StackTrace } from 'meteor/peerlibrary:stacktrace';
import { TraceX }     from 'meteor/arsnebula:tracex';
/// import debug          from 'meteor/robodo:meteor-debug'; THIS WILL FAIL

import dummy from '/lib/extensions/extensions';
dummy(dummy);

/**
 * <h4>`TraceX` configuration export</h4>
 * Initialize `TraceX`, then `TraceX.trace` becomes valid <br>
 * {@link Ctrl} uses this if it needs it first<br>
 * - `event.id` - `'tracex'`
 * - `console.format` - `'meta'`
 * - `console.levels.client` - `["debug", "info", "warning", "exception", "error"]`
 * - `console.levels.server` - `["debug", "info", "warning", "exception", "error"]`
 * - `database.collection` - `'tracex'`
 * - `database.levels` - `["db", "debug", "info", "warning", "exception", "error"]`
 * @protected
 */
export const TraceX_init = {
  "event": {
    "id": "tracex" // the id of the event to listen and auto-log
  },
  "console": {
    "format": "meta", // format for console logging (text, meta or json)
    "levels": {
      //"client": null,          // array of log levels to log to the client console (or null)
      "client": [// array of log levels to log to the server console (or null)
        "debug", "info", "warning", "exception", "error"],
      "server": [// array of log levels to log to the server console (or null)
        "debug", "info", "warning", "exception", "error"]
    }
  },
  "database": {
    "collection": "tracex", // name of the database collection
    "levels": [// array of log levels to log to the database (or null)
      "db", "debug", "info", "warning", "exception", "error"]
  }
};
// Z_DOT TraceX.init() ::tracex.js z.90687113004872301.2017.02.16.08.39.38.609|tag
TraceX.init(TraceX_init);

/**
 * <h4>Debug `imports`</h4>
 * @protected
 */
const debug_imports = debug('imports');
/**
 * <h4>Debug `server`</h4>
 * @protected
 */
const debug_server = debug('server'); // eslint-disable-line no-unused-vars

/**
 * <h4>Debug `bug`</h4>
 * @protected
 */
const debug_bug = debug('bug');

/**
 * <h4>StackTrace Helper for Ctrl</h4>
 * Does specific output
 * <p>
 * @public
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/both/ctrl/ctrl.js
 */
class StackTrace_ {
  //noinspection JSValidateTypes
  /**
   * <h4>`_select` private function</h4>
   * Get Limited Trace Array<br>
   * Filter out items with `.meteor` filenames <br>
   * `_select(t, options = { type:'source' })`
   * <p>
   * <h5>`options`</h5>
   * - `type` = source|fileline
   * - `includeMeteor` = do not filter meteor (deeper stack) items
   * <p>
   * <h5>`options.type`</h5>
   * - source = returns at &lt;function&gt; &lt;filename&gt;:&lt;line&gt;:&lt;column&gt;
   * - fileline = returns &lt;filename&gt;:&lt;line&gt;:&lt;column&gt;
   * <p>
   * @protected
   * @param {{fileName:string, lineNumber:number, columnNumber:number, source:string}[]} t - stack
   * @param {{type: string}} options
   * @return {string[]} of `.source` for now
   */
  static _select(t, options = { type:'source' }) {
    // fileName: 'C:\\_\\socialnitro-app\\.meteor\\local\\build\\programs\\server\\boot.js',
    // lineNumber: 295,
    // columnNumber: 34,
    // source: '    at C:\\_\\socialnitro-app\\.meteor\\local\\build\\programs\\server\\boot.js:295:34'
    const file_prefix="C:/_/socialnitro-app/";
    function file_fix(v) {
      let file=v.fileName.trim().replace(/\\\\/g, '\\').replace(/\\/g, '/');
      if (file.indexOf(':') === -1) {
        file=file_prefix+file;
      }
      return file;
    }
    let t1 = t;
    if (!options.includeMeteor) {
      t1=t.filter(function(v) {
        return v.fileName.indexOf('\\.meteor\\') === -1;
      });
    }
    if ('file' === options.type) {
      return t1.map(function(v) {
        return file_fix(v);
      });
    }
    if ('fileline' === options.type) {
      return t1.map(function(v) {
        return file_fix(v)+':'+v.lineNumber+':'+v.columnNumber;
      });
    }
    if ('source' === options.type) {
      return t1.map(function(v) {
        return v.source.trim().replace(/\\\\/g, '\\').replace(/\\/g, '/');
      });
    }
    return [];
  }
  /**
   * <h4>`file` public function</h4>
   * Get fullfilepath <br>
   * `file(offset = 0)`
   * <p>
   * @public
   * @param {number} [offset=0] - positive stack offset
   * @return {string} fullfilepath
   */
  static file(offset = 0) {
    const t = StackTrace.getSync().slice(1+offset, 2+offset);
    let src = this._select(t, { type:'file' });
    return src[0];
  }
  /**
   * <h4>`fileline` public function</h4>
   * Get fullfilepath:line:col <br>
   * `fileline(offset = 0)`
   * <p>
   * @public
   * @param {number} [offset=0] - positive stack offset
   * @return {string} fullfilepath:line:col
   */
  static fileline(offset = 0) {
    const t = StackTrace.getSync().slice(1+offset, 2+offset);
    let src = this._select(t, { type:'fileline' });
    return src[0];
  }
  /**
   * <h4>Get file, fileline, sourceArray</h4>
   * <p>
   * @public
   * @param {number} [offset=0] - positive stack offset
   * @return {Object} `{ file, fileline, sourceArray }`
   */
  static group(offset = 0) {
    const t = StackTrace.getSync().slice(1+offset, 2+offset);
    let fileline = this._select(t, { type:'fileline' });
    fileline=fileline[0];
    let file = this._select(t, { type:'file' });
    file=file[0];
    let sourceArray = this._select(t, { type:'source' });
    return { file, fileline, sourceArray };
  }
  /**
   * <h4>Get stack without first entry</h4>
   * <p>
   * @public
   * @param {number} [offset=0] - positive stack offset
   * @return {StackTrace[]}
   */
  static raw(offset = 0) {
    return StackTrace.getSync().slice(1+offset);
  }
  /**
   * <h4>Get List of Source Lines</h4>
   * <p>
   * @public
   * @param {number} [offset=0] - positive stack offset
   * @return {string} - CR delimited
   */
  static source(offset = 0) {
    const t = StackTrace.getSync().slice(1+offset);
    let src = this._select(t, { type:'source' });
    return src.join("\r");
  }
  /**
   * <h4>Get source array</h4>
   * <p>
   * @public
   * @param {number} [offset=0] - positive stack offset
   * @return {string[]}
   */
  static sourceArray(offset = 0) {
    const t = StackTrace.getSync().slice(1+offset);
    return this._select(t, { type:'source' });
  }
}

/**
 * <h4>Control Global</h4>
 * Meta control point for developer services<br>
 * Offers debug, logging, and flags
 * <p>
 * <h5>Dependencies</h5>
 * - `debug` - meteor/robodo:meteor-debug
 * - StackTrace
 * <p>
 * <h5>Implements</h5>
 * <p>
 * <h5>Implements.console</h5>
 * - `log(msg, object)`            - console.log with fileline
 * - `logStack(msg, object)`       - console.log with fileline, stack
 * <p>
 * <h5>Implements.debug</h5>
 * - `bug(msg, object)`            - debug.bug with fileline
 * - `bugStack(msg, object)`       - debug.bug with fileline, stack
 * <p>
 * <h5>Implements.trace</h5>
 * - `trace(msg, object)`          - TraceX.trace.info with fileline
 * - `traceStack(msg, object)`     - TraceX.trace.info with fileline, stack
 * - `info(msg, object)`           - TraceX.trace.info with fileline
 * - `infoStack(msg, object)`      - TraceX.trace.info with fileline, stack
 * - `debug(msg, object)`          - TraceX.trace.debug with fileline
 * - `debugStack(msg, object)`     - TraceX.trace.debug with fileline, stack
 * - `warning(msg, object)`        - TraceX.trace.warning with fileline
 * - `warningStack(msg, object)`   - TraceX.trace.warning with fileline, stack
 * - `exception(msg, object)`      - TraceX.trace.exception with fileline
 * - `exceptionStack(msg, object)` - TraceX.trace.exception with fileline, stack
 * - `error(msg, object)`          - TraceX.trace.error with fileline
 * - `errorStack(msg, object)`     - TraceX.trace.error with fileline, stack
 * <p>
 * <h5>Implements.util</h5>
 * - `logFileLoaded()`             - debug.imports and TraceX.info with file
 * <p>
 * @public
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/both/ctrl/ctrl.js
 */
class Ctrl { // eslint-disable-line no-global-assign
  //noinspection LocalVariableNamingConventionJS
  /**
   * <h4>Specialty output for StackTraces</h4>
   * <p>
   * @public
   */
  static StackTrace = StackTrace_;
  /**
   * <h4>Ensure TraceX initialization</h4>
   * <p>
   * @protected
   */
  static _ensureTraceX() {
    if (!TraceX.trace) {
      TraceX.init(TraceX_init);
    }
  }
  //noinspection OverlyComplexFunctionJS
  /**
   * <h4>Handle Output for TraceX</h4>
   * <p>
   * @protected
   * @param {string} pfx  - message prefix
   * @param {string} fileline  - fullpath:line:col
   * @param {string} kind - TraceX level
   * @param {string} file - fullpath
   * @param {Object} more - add to meta
   * @param {string} [msg] - user message
   * @param {Object} [object] - user meta
   */
  static _traceKind(pfx, fileline, kind, file, more, msg, object) {
    let meta = { file:file, type: 'trace', subtype: kind };
    meta = Object.extend({}, meta, more);
    if (object) {
      meta.object = object;
    }
    this._ensureTraceX();
    if (msg) {
      TraceX.trace[kind](pfx+fileline+' '+msg, meta);
    }else{
      TraceX.trace[kind](pfx+fileline, meta);
    }
  }
  /**
   * <h4>For webstorm hack</h4>
   * no-op
   * <p>
   * @public
   */
  static unused(v) {
    return v;
  }
  /**
   * <h4>Allow output depending on key</h4>
   * <p>
   * @public
   * @param {string} key
   * @return {boolean}
   */
  static doOutput(key) {
    const yes="TraceX::Meteor.trace".split(' ');
    const no="TraceX::Meteor.trace".split(' ');
    if (-1 < yes.indexOf(key)) {
      return -1 >= no.indexOf(key);
    }
    return false;
  }
  /**
   * <h4>Log using `console.log`</h4>
   * Logs with fileline
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static log(msg, object) {
    const fileline=this.StackTrace.fileline(1);
    const pfx = 'Ctrl.log';
    //noinspection IfStatementWithTooManyBranchesJS
    if (msg && object) {
      console.log(pfx, fileline, msg, object);
    }else if (msg) {
      console.log(pfx, fileline, msg);
    }else if (object) {
      console.log(pfx, fileline, object);
    }else{
      console.log(pfx, fileline);
    }
  }
  /**
   * <h4>Log using `console.log`</h4>
   * Logs with fileline, stack
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static logStack(msg, object) {
    const fileline=this.StackTrace.fileline(1);
    const source=this.StackTrace.source(1);
    const pfx = 'Ctrl.logStack';
    //noinspection IfStatementWithTooManyBranchesJS
    if (msg && object) {
      console.log(pfx, fileline, msg, object, source);
    }else if (msg) {
      console.log(pfx, fileline, msg, source);
    }else if (object) {
      console.log(pfx, fileline, object, source);
    }else{
      console.log(pfx, fileline, source);
    }
  }
  /**
   * <h4>Log using `debug.bug`</h4>
   * Logs with fileline
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static bug(msg, object) {
    const fileline=this.StackTrace.fileline(1);
    const pfx = 'Ctrl.debug ';
    //noinspection IfStatementWithTooManyBranchesJS
    if (msg && object) {
      debug_bug(pfx+fileline+' '+msg+"\r"+JSON.stringify(object));
    }else if (msg) {
      debug_bug(pfx+fileline+' '+msg);
    }else if (object) {
      debug_bug(pfx+fileline+"\r"+JSON.stringify(object));
    }else{
      debug_bug(pfx+fileline);
    }
  }
  /**
   * <h4>Log using `debug.bug`</h4>
   * Logs with fileline, stack
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static bugStack(msg, object) {
    const fileline=this.StackTrace.fileline(1);
    const source=this.StackTrace.source(1);
    const pfx = 'Ctrl.debugStack ';
    //noinspection IfStatementWithTooManyBranchesJS
    if (msg && object) {
      debug_bug(pfx+fileline+' '+msg+"\r"+JSON.stringify(object)+"\r"+source);
    }else if (msg) {
      debug_bug(pfx+fileline+' '+msg+"\r"+source);
    }else if (object) {
      debug_bug(pfx+fileline+"\r"+JSON.stringify(object)+"\r"+source);
    }else{
      debug_bug(pfx+fileline+"\r"+source);
    }
  }
  /**
   * <h4>Log using `TraceX.trace.info`</h4>
   * Logs with fileline
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static trace(msg, object) {
    const kind='info';
    const pfx = 'Ctrl.trace ';
    const g=this.StackTrace.group(1);
    this._traceKind(pfx, g.fileline, kind, g.file, {}, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.info`</h4>
   * Logs with fileline, stack
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static traceStack(msg, object) {
    const kind='info';
    const pfx = 'Ctrl.traceStack ';
    const g=this.StackTrace.group(1);
    this._traceKind(pfx, g.fileline, kind, g.file, { stack: g.sourceArray }, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.debug`</h4>
   * Logs with fileline
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static debug(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='debug';
    const pfx = 'Ctrl.'+kind+' ';
    this._traceKind(pfx, g.fileline, kind, g.file, {}, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.debug`</h4>
   * Logs with fileline, stack
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static debugStack(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='debug';
    const pfx = 'Ctrl.'+kind+'Stack ';
    this._traceKind(pfx, g.fileline, kind, g.file, { stack: g.sourceArray }, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.info`</h4>
   * Logs with fileline
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static info(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='info';
    const pfx = 'Ctrl.'+kind+' ';
    this._traceKind(pfx, g.fileline, kind, g.file, {}, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.info`</h4>
   * Logs with fileline, stack
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static infoStack(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='info';
    const pfx = 'Ctrl.'+kind+'Stack ';
    this._traceKind(pfx, g.fileline, kind, g.file, { stack: g.sourceArray }, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.warning`</h4>
   * Logs with fileline
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static warn(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='warning';
    const pfx = 'Ctrl.'+kind+' ';
    this._traceKind(pfx, g.fileline, kind, g.file, {}, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.warning`</h4>
   * Logs with fileline, stack
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static warnStack(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='warning';
    const pfx = 'Ctrl.'+kind+'Stack ';
    this._traceKind(pfx, g.fileline, kind, g.file, { stack: g.sourceArray }, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.warning`</h4>
   * Logs with fileline
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static warning(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='warning';
    const pfx = 'Ctrl.'+kind+' ';
    this._traceKind(pfx, g.fileline, kind, g.file, {}, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.warning`</h4>
   * Logs with fileline, stack
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static warningStack(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='warning';
    const pfx = 'Ctrl.'+kind+'Stack ';
    this._traceKind(pfx, g.fileline, kind, g.file, { stack: g.sourceArray }, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.exception`</h4>
   * Logs with fileline
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static exception(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='exception';
    const pfx = 'Ctrl.'+kind+' ';
    this._traceKind(pfx, g.fileline, kind, g.file, {}, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.exception`</h4>
   * Logs with fileline, stack
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static exceptionStack(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='exception';
    const pfx = 'Ctrl.'+kind+'Stack ';
    this._traceKind(pfx, g.fileline, kind, g.file, { stack: g.sourceArray }, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.error`</h4>
   * Logs with fileline
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static error(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='error';
    const pfx = 'Ctrl.'+kind+' ';
    this._traceKind(pfx, g.fileline, kind, g.file, {}, msg, object);
  }
  /**
   * <h4>Log using `TraceX.trace.error`</h4>
   * Logs with fileline, stack
   * <p>
   * @public
   * @param {string} [msg] - message to log
   * @param {Object} [object] - object to log
   */
  static errorStack(msg, object) {
    const g=this.StackTrace.group(1);
    const kind='error';
    const pfx = 'Ctrl.'+kind+'Stack ';
    this._traceKind(pfx, g.fileline, kind, g.file, { stack: g.sourceArray }, msg, object);
  }
  /**
   * <h4>Log using debug.imports and `TraceX.trace.log.db`</h4>
   * Put at the top of each file
   * <p>
   * @public
   */
  static logFileLoaded() {
    const file=this.StackTrace.file(1);
    const msg = 'fileLoaded '+file;
    debug_imports(msg);
    this._ensureTraceX();
    //process.env
    TraceX.trace.log("db", msg, { type:'fileLoad', file:file });
    //console.log('TraceX',TraceX);
  }
}
export default Ctrl;
Ctrl.unused(both_ctrl_ctrl_js);
Ctrl.unused(debug_server);

///////// FINAL
