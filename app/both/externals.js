/**
 * <h4>External Definitions for EsDoc </h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cboth%5Cexternals%2Ejs">C:\_\socialnitro-app\both\externals.js</a><br>
 * <h5>Meteor External Definitions</h5>
 * - {@link Meteor}                      - `meteor:meteor-base@1.0.4`
 * - {@link DDP}                         - `meteor:ddp@1.2.5`
 * - {@link DDPRateLimiter}              - `meteor:ddp-rate-limiter@1.0.6`
 * - {@link Mongo}                       - `meteor:mongo@1.1.14`
 * - {@link Random}                      - `meteor:random@1.0.10`
 * - {@link WriteResult}                 - `meteor:mongo`
 * - {@link _}                           - `meteor:underscore@1.0.10`
 *
 * <h5>MDG External Definitions</h5>
 * - {@link ValidatedMethod}             - `mdg:validated-method`
 *
 * <h5>Other Meteor External Definitions </h5>
 * - {@link Factory}                     - `dburles:factory`
 * - {@link SimpleSchema}                - `aldeed:simple-schema`
 * - {@link PublicationCollector}        - `johanbrook:publication-collector`
 * - {@link debug}                       - `robodo:meteor-debug`
 * - {@link StackTrace}                  - `peerlibrary:stacktrace`
 * - {@link TraceX}                      - `arsnebula:tracex`
 * - {@link TAPi18n}                     - `tap:i18n`
 *
 * <h5>Testing Meteor External Definitions</h5>
 * - {@link chai}                        - `practicalmeteor:chai`
 * - {@link assert}                      - `practicalmeteor:chai`
 *
 * <h5>NPM External Definitions</h5>
 * - {@link faker}                       - `Marak/Faker`
 *
 * <p>
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/both/externals.js
 */
const externals_js = '/both/externals.js'; // eslint-disable-line no-unused-vars

////// METEOR

/**
 * @typedef {Object} Meteor
 * @external {Meteor} http://docs.meteor.com/
 */
/**
 * @typedef {function} check
 * @external {check} https://atmospherejs.com/meteor/check
 */
/**
 * @typedef {Object} DDP
 * @external {DDP} https://docs.meteor.com/api/connections.html#DDP-connect
 */
/**
 * @typedef {Object} DDPRateLimiter
 * @external {DDPRateLimiter} https://docs.meteor.com/api/methods.html#ddpratelimiter
 */
/**
 * @typedef {Object} Mongo
 * @external {Mongo} https://docs.mongodb.com/
 */
/**
 * @typedef {Object} Random
 * @external {Random} https://docs.meteor.com/packages/random.html
 */
/**
 * @typedef {Object} FlowRouter
 * @external {FlowRouter} https://atmospherejs.com/kadira/flow-router
 */
/**
 * @typedef {Object} Factory
 * @external {Factory} https://atmospherejs.com/dburles/factory
 */
/**
 * @typedef {function} SimpleSchema
 * @external {SimpleSchema} https://atmospherejs.com/aldeed/simple-schema
 */
/**
 * @typedef {Object} PublicationCollector
 * @external {PublicationCollector} https://atmospherejs.com/johanbrook/publication-collector
 */
/**
 * @typedef {Object} Blaze
 * @external {Blaze} https://atmospherejs.com/gadicc/blaze-react-component
 */
/**
 * @typedef {function} jQuery
 * @external {jQuery} http://api.jquery.com/
 */

/**
 * @typedef {function} createContainer
 * @external {createContainer} https://atmospherejs.com/meteor/react-meteor-data
 */

/**
 * @typedef {Object} WriteResult
 * @external {WriteResult} https://docs.mongodb.com/manual/reference/method/WriteResult/
 */
/**
 * @typedef {function} debug
 * @external {debug} https://atmospherejs.com/robodo/meteor-debug
 */
/**
 * @external {StackTrace} https://github.com/stacktracejs/stacktrace.js
 */
/**
 * @external {TraceX} https://atmospherejs.com/arsnebula/tracex
 */
/**
 * @typedef {Object} TAPi18n
 * @external {TAPi18n} https://atmospherejs.com/tap/i18n
 */
/**
 * @typedef {function} ValidatedMethod
 * @external {ValidatedMethod} https://atmospherejs.com/mdg/validated-method
 */
/**
 * @typedef {function} resetDatabase
 * @external {resetDatabase} https://atmospherejs.com/xolvio/cleaner
 */
/**
 * @typedef {Object} _
 * @external {_} http://underscorejs.org/
 */
/**
 * @typedef {Object} sAlert
 * @external {sAlert} https://atmospherejs.com/juliancwirko/s-alert
 */

////// TESTING

/**
 * @typedef {Object} chai
 * @external {chai} http://chaijs.com/api/
 */
/**
 * @typedef {function} assert
 * @external {assert} http://chaijs.com/api/assert/
 */
/**
 * @typedef {function} expect
 * @external {expect} http://chaijs.com/api/bdd/
 */
/**
 * @typedef {function} unexpected
 * @external {unexpected} http://unexpected.js.org/
 */
/**
 * @typedef {function} demand
 * @external {demand} http://unexpected.js.org/
 */
/**
 * @typedef {Object} JsDom
 * @external {JsDom} https://github.com/tmpvar/jsdom
 */
/**
 * @typedef {Object} mochajsdom
 * @external {mochajsdom} https://www.npmjs.com/package/mocha-jsdom
 */
/**
 * @typedef {Object} Reactor
 * @external {Reactor} https://facebook.github.io/react/docs/test-utils.html
 */
/**
 * @typedef {Object} sinon
 * @external {sinon} http://sinonjs.org/docs/
 */

////// TESTING SUITES

/**
 * @typedef {function} describe
 * @external {describe} https://mochajs.org/#usage
 */
/**
 * @typedef {function} context
 * @external {context} https://mochajs.org/#usage
 */
/**
 * @typedef {function} it
 * @external {it} https://mochajs.org/#usage
 */
/**
 * @typedef {function} specify
 * @external {specify} https://mochajs.org/#usage
 */
/**
 * @typedef {function} before
 * @external {before} https://mochajs.org/#usage
 */
/**
 * @typedef {function} after
 * @external {after} https://mochajs.org/#usage
 */
/**
 * @typedef {function} beforeEach
 * @external {beforeEach} https://mochajs.org/#usage
 */
/**
 * @typedef {function} afterEach
 * @external {afterEach} https://mochajs.org/#usage
 */
/**
 * @typedef {function} suite
 * @external {suite} https://mochajs.org/#usage
 */
/**
 * @typedef {function} test
 * @external {test} https://mochajs.org/#usage
 */
/**
 * @typedef {function} suiteSetup
 * @external {suiteSetup} https://mochajs.org/#usage
 */
/**
 * @typedef {function} suiteTeardown
 * @external {suiteTeardown} https://mochajs.org/#usage
 */
/**
 * @typedef {function} setup
 * @external {setup} https://mochajs.org/#usage
 */
/**
 * @typedef {function} teardown
 * @external {teardown} https://mochajs.org/#usage
 */
/**
 * @typedef {function} browser
 * @external {browser} http://webdriver.io/api.html
 */

////// NPM

/**
 * @typedef {Object} React
 * @external {React} https://facebook.github.io/react/docs/hello-world.html
 */
/**
 * @typedef {function} mount
 * @external {mount} https://github.com/kadirahq/react-mounter
 */

/**
 * @typedef {Object} faker
 * @external {faker} https://github.com/Marak/Faker.js#readme
 */

//////

/**
 * @typedef {Object} Reload
 * @external {Reload} https://atmospherejs.com/aldeed/reload-extras
 */

/**
 * @typedef {Object} Audio
 * @external {Audio} https://www.w3schools.com/jsref/dom_obj_audio.asp
 */
/**
 * @typedef {Object} localStorage
 * @external {localStorage} https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
 */

///

// * @typedef {Object} WriteResult
// * @property {number} nInserted
// * @property {number} nMatched
// * @property {number} nModified
// * @property {number} nUpserted
// * @property {string} _id
// * @property {Object} writeError
// * @property {Object} writeConcernError
