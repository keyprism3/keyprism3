## README

`C:\_\keyprism3\app\both`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cboth) _Client and Server_

Files in the `lib` directory would be loaded first, before the ones in the `both` directory

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5CDesktop.ini) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cboth%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FVx%31Gi%31qGAqAAAgga) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/FB%37D%31%35%32D-%39%33%32E-%37F%36%34-EEBA-DC%34A%35%32AC%32%36%31F) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/both/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-45) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/both/main.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/both_main_js/index.html) - plato
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cboth%5Cexternals.js) `externals.js` - Commenting up external symbols for use by EsDoc
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cboth%5Cmain.js) `main.js` - Import `ctrl` and `tracex`

### Folder Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-orange-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cboth%5Cctrl%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%37%26webthought_id%3D%35%37%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FIjKmi%31qGAqAAAggs) `ctrl` - Developer High Level Controller
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cboth%5Ctracex%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%38%26webthought_id%3D%35%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FS.wWi%31qGAqAAAghD) `tracex` - `TraceX` Debug Utility Setup

### Anticipates

- `<other high-level component folders>`
