/**
 * <h4>Setup TraceX</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cboth%5Ctracex%5Ctracex%2Ejs">C:\_\socialnitro-app\both\tracex\tracex.js</a><br>
 * Attach to Meteor object
 * <p>
 * <h5>`Meteor` new members</h5>
 * - `.trace` `TraceX.trace`
 * - `.tracex` `Mongo.Collection.tracex`
 * <p>
 * <h5>Meteor Dependencies</h5>
 * - {@link TraceX} - meteor/arsnebula:tracex
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl} - Meta control point for developer services
 * <p>
 * @protected
 * @version 0.0.1
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/both/tracex/tracex.js
 * @todo nada
 */
const both_tracex_tracex_js= "/both/tracex/tracex.js";

import { TraceX } from 'meteor/arsnebula:tracex';
import Ctrl from '../ctrl/ctrl';

Ctrl.logFileLoaded();
Ctrl.unused(both_tracex_tracex_js);

/**
 * <h4>Log if `Meteor.trace` (`TraceX.trace`) is undefined in client and server</h4>
 * Using `Ctrl.doOutput`
 * <p>
 * @protected
 */
const key_trace='TraceX::Meteor.trace';
/**
 * <h4>Log if `Meteor.tracex` (`TraceX collection`) is undefined in client and server</h4>
 * Using `Ctrl.doOutput`
 * <p>
 * @protected
 */
const key_tracex='TraceX::Meteor.tracex';
/**
 * <h4>Attach `TraceX.trace` to `Meteor.trace`</h4>
 */
const Meteor_trace = TraceX.trace;
// Z_DOT Attach TraceX.trace to Meteor ::tracex.js z.97154073004872301.2017.02.16.10.17.25.179|tag
if ('undefined' === typeof Meteor.trace) {
  Meteor.trace = Meteor_trace;
  if (Ctrl.doOutput(key_trace)) {
    if (Meteor.isClient) {
      TraceX.trace.info('Meteor.trace is undefined in client');
    }else{
      TraceX.trace.info('Meteor.trace is undefined in server');
    }
  }
}else{
  if (Ctrl.doOutput(key_trace)) {
    if (Meteor.isClient) {
      TraceX.trace.info('Meteor.trace IS DEFINED in client');
    }else{
      TraceX.trace.info('Meteor.trace IS DEFINED in server');
    }
  }
}
/**
 * Attach Mongo.Collection.tracex to Meteor
 */
const Meteor_tracex = Mongo.Collection.get('tracex');
Ctrl.unused(Meteor_tracex);
// Z_DOT Attach Mongo.Collection.tracex to Meteor ::tracex.js z.72723273004872301.2017.02.16.10.20.32.727|tag
if (Meteor.isClient) {
  if (Ctrl.doOutput(key_tracex)) {
    TraceX.trace.info('TraceX initialized Client');
  }
  Meteor.tracex = Meteor_tracex;
  if (Ctrl.doOutput(key_tracex)) {
    TraceX.trace.info('Client Meteor.tracex:' + (typeof Meteor.tracex));
  }
}
if (Meteor.isServer) {
  if (Ctrl.doOutput(key_tracex)) {
    TraceX.trace.info('TraceX initialized Server');
  }
  Meteor.tracex = Meteor_tracex;
  Meteor.tracex.remove({});
  if (Ctrl.doOutput(key_tracex)) {
    TraceX.trace.info('Server Meteor.tracex:' + (typeof Meteor.tracex));
  }
  //console.log('C:/txt.txt');
  //no inspection JSUnresolvedVariable
  //console.log(__filename);
}

