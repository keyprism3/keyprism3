## README

`C:\_\keyprism3\app\both\tracex`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cboth%5Ctracex) _`TraceX` Debug Utility Setup_

Simple logging of debug and trace informatio

`arsnebula:tracex` on [Atmosphere](https://atmospherejs.com/arsnebula/tracex)

    TraceX.trace.log(level, message, meta);
    //
    TraceX.trace.debug(message, meta);
    TraceX.trace.info(message, meta);
    TraceX.trace.warning(message, meta);
    TraceX.trace.exception(message, meta);
    TraceX.trace.error(message, meta);
    //
    var myTrace = new TraceX.TraceLogger({"extra": "data"});
    myTrace.debug("This is some debug information.");
    //=> This is some debug information {"extra": "data"}

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5Cboth%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%35%26webthought_id%3D%34%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FVx%31Gi%31qGAqAAAgga) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cboth%5Ctracex%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FS.wWi%31qGAqAAAghD) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%30%33%39D%35%37AF-%34E%39%34-%39C%35D-%31%32A%34-D%33C%39%32%39C%32CDA%35) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/both/tracex/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-58) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/app/both/tracex/tracex.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/both_tracex_tracex_js/index.html) - plato
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5Cboth%5Ctracex%5Ctracex.js) `tracex.js` - Setup Tracex

### Folder Built-ins

_none_

### Anticipates

_none_
