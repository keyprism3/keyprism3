/**
 * <h4>Client and Server Entry</h4>
 * <a href="aip://open/C%3A%5C%5F%5Csocialnitro%2Dapp%5Cboth%5Cmain%2Ejs">C:\_\socialnitro-app\both\main.js</a><br>
 * <p>
 * <h5>Project Dependencies</h5>
 * - {@link Ctrl}   - `'/both/ctrl/ctrl'`
 * - {@link TraceX} - `meteor/arsnebula:tracex`
 * @protected
 * @see https://bitbucket.org/socialteria/socialnitro-app/src/master/both/main.js
 */
const both_main_js= "/both/main.js";

import Ctrl from './ctrl/ctrl';

import './tracex/tracex';

Ctrl.logFileLoaded();
Ctrl.unused(both_main_js);

/// FINAL
