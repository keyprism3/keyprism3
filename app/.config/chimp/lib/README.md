## README

`C:\_\keyprism3\app\.config\chimp\lib`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5C.config%5Cchimp%5Clib) _Chimp Configuration Helpers_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5Capp%5C.config%5Cchimp%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%35%26webthought_id%3D%35%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FQyRKi%31qGAqAAAgfO) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5C.config%5Cchimp%5Clib%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fk%38eqi%31qGAqAAAgfi) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/F%31%34%39%32%31%33%37-%34FEE-%30%33%32%35-%39%38%36B-%37EE%32CACB%31%36E%36) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/app/.config/chimp/lib/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-56) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### File Built-ins

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5C.config%5Cchimp%5Clib%5Cbabel-register.js) `babel-register.js` - `'plugins': ['transform-runtime'],'presets': ['es2015', 'stage-2']`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5C.config%5Cchimp%5Clib%5Cboolean-helper.js) `boolean-helper.js` - `isFalsey,isTruthy`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5C.config%5Cchimp%5Clib%5Cci.js) `ci.js` - `isCI()=>process.env.CI`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5Capp%5C.config%5Cchimp%5Clib%5Cenvironment-variable-parsers.js) `environment-variable-parsers.js` - `parseString, parseNumber, parseInteger, parseBoolean...`

### Folder Built-ins

_none_

### Anticipates

_none_
